import { Controller, Get, Logger, Res } from '@nestjs/common'
import { MessagePattern } from '@nestjs/microservices'
import { Response } from 'express'
import { ObjectId } from 'mongodb'
import { IMessagePattern } from '.'
import { downloadFileFromGridFS } from './repositories/storage.repository'

@Controller('storage')
export class StorageController {
    private readonly logger = new Logger(StorageController.name)

    @MessagePattern<IMessagePattern>({
        storage: 'get_file_stream'
    })
    getFile(
        data: {
            file_id: ObjectId,
            options?: {
                start: number,
                end: number,
            }
        }
    ) {
        const stream =
            downloadFileFromGridFS(new ObjectId('5fbc0825a4050a874838325d'))
        // res.pipe(stream)
        return stream
    }
}
