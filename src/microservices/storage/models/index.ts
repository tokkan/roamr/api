// IMAGE
// https://www.iana.org/assignments/media-types/media-types.xhtml#image
// VIDEO
// https://www.iana.org/assignments/media-types/media-types.xhtml#video

export type ContentTypes =
    'image/jpeg' |
    // 'image/jpg' |
    'image/apng' |
    'image/png' |
    'image/bmp' |
    'image/gif' |
    'image/tiff' |
    'image/webp' |
    'video/mpeg' |
    'video/ogg' |
    'video/mp2t' |
    'video/webm' |
    'video/3gpp' |
    'video/3gpp2' |
    'video/mp4' |
    'video/mpv'