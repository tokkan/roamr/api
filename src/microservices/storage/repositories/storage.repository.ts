import { ObjectId } from 'mongodb'
import { Buckets, MongoDatabaseHandle as db } from './../../../database/mongodb'
import fs from 'fs'
import { Readable } from 'stream'
import { Logger } from '@nestjs/common'
import { ContentTypes } from '../models'

const logger = new Logger('StorageRepository')

export const downloadFileFromGridFS = (
    file_id: ObjectId,
    bucketName: Buckets = 'social_fs',
    options?: {
        start: number
        end: number
    }
) => {
    try {
        const bucket = db.bucket(bucketName)
        const stream = bucket.openDownloadStream(
            file_id,
            options,
        )
        return stream
    } catch (error) {
        console.debug(error)
        logger.error(error)
        return null
    }
}

export const uploadFileToGridFS = (
    filename: string,
    buffer: Buffer,
    metadata: {
        [key: string]: string
        contentType: ContentTypes
    },
    bucketName: Buckets = 'social_fs',
) => {
    try {
        const _id = new ObjectId()
        const bucket = db.bucket(bucketName)
        const stream = bucket.openUploadStreamWithId(
            _id,
            `${filename}-${_id}`,
            {
                metadata,
            }
        )
        const readable = new Readable()
        readable.push(buffer)
        readable.push(null)
        readable.pipe(stream)
        return _id
    } catch (error) {
        console.debug(error)
        logger.error(error)
        return null
    }
}