export type MessagePatternTypes =
    'get_file_stream'

// export type EventPatternTypes =
//     'create_post'

export interface IMessagePattern {
    storage: MessagePatternTypes
}