import { CommandHandler, ICommandHandler, EventPublisher, EventBus } from '@nestjs/cqrs'
import { Logger } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import * as QRCode from 'qrcode'
import * as jimp from 'jimp'
import { UserEventHandle } from '../events/users.aggregate'
import { createUserFire, addSocialAccountToUser } from './../repositories/user-firebase.repository'
import { createUserMongo } from '../repositories/user.repository'
import { UsersService } from '../services/users.service'
import { RoleTypes, INotificationCredential, IUserEntityBase, IUserMongoEntity, IUserFireEntity } from '../models'
import { ISocialAccount } from '../models/social-account'

export class CreateUserCommand {
    constructor(
        public readonly user_fire_id: string,
        public readonly user_mongo_id: string,
        public readonly username: string,
        public readonly password: string,
        public readonly email: string,
        public readonly account: ISocialAccount,
        public readonly first_name: string,
        public readonly last_name: string,
        public readonly avatar: string,
        public readonly avatar_url: string,
        public readonly private_account: boolean,
        public readonly use_real_name: boolean,
        public readonly roles: RoleTypes[] = [],
        public readonly notification_credentials: INotificationCredential[] = [],
    ) { }
}

export class AddCustomerIdToUserCommand {
    constructor(
        public readonly mongo_user_id: ObjectId,
        public readonly fire_user_id: string,
        public readonly customer_id: string,
    ) { }
}

export class AddSocialAccountToUserCommand {
    constructor(
        public readonly user_id: string,
        public readonly access_token: string
    ) { }
}

@CommandHandler(CreateUserCommand)
export class CreateUserCommandHandler implements ICommandHandler<CreateUserCommand> {
    private readonly logger = new Logger(CreateUserCommandHandler.name)
    private readonly eventHandle: UserEventHandle

    constructor(
        private publisher: EventPublisher,
        private eventBus: EventBus,
    ) {
        // const blueprint = publisher.mergeClassContext(BattleEventHandle)
        // this.battleHandle = new blueprint()
        // this.battleHandle.autoCommit = true
        this.eventHandle =
            publisher.mergeObjectContext(
                new UserEventHandle(new ObjectId().toHexString()))
        this.eventHandle.autoCommit = true
    }

    async execute(command: CreateUserCommand): Promise<any> {
        this.logger.debug('Executing createUserHandler')
        try {
            const user_fire_id = command.user_fire_id
            const user_mongo_id = command.user_mongo_id
            const username = command.username
            const password = command.password
            const email = command.email
            const account = command.account
            const first_name = command.first_name
            const last_name = command.last_name
            const avatar = command.avatar
            const avatar_url = command.avatar_url
            const private_account = command.private_account
            const use_real_name = command.use_real_name
            const roles = command.roles
            const notification_credentials = command.notification_credentials

            if (roles.length <= 0) {
                roles.push(RoleTypes.PLEB)
            }

            const baseURL = `roamr/user/${user_mongo_id}`
            const userURL = await QRCode.toDataURL(baseURL, {
                errorCorrectionLevel: 'M',
            })

            const base_user: IUserEntityBase = {
                customer_id: '',
                social_accounts: [
                    account
                ],
                username: username,
                password: password,
                email: email,
                qr_code: baseURL,
                is_admin: false,
                is_account_verified: false,
                is_verified: false,
                first_name,
                last_name,
                full_name: `${first_name} ${last_name}`,
                avatar,
                avatar_url,
                private_account,
                use_real_name,
                roles: roles,
                role_access: [],
                notification_credentials,
            }

            const mongo_user: IUserMongoEntity = {
                ...base_user,
                fire_id: user_fire_id,
                _id: new ObjectId(user_mongo_id),
            }

            const fire_user: IUserFireEntity = {
                ...base_user,
                fire_id: user_fire_id,
                _id: user_mongo_id,
            }

            await createUserMongo(mongo_user)
            await createUserFire(fire_user)

        } catch (error) {
            this.logger.error(error)
        }
    }
}

@CommandHandler(AddCustomerIdToUserCommand)
export class AddCustomerIdToUserCommandHandler<T extends AddCustomerIdToUserCommand> implements ICommandHandler<T> {
    constructor(
        private readonly usersService: UsersService,
    ) { }

    async execute(command: T) {
        // save/update customer.id to database
        let fire_res =
            await this.usersService
                .updateFireUserByValuePair(
                    command.fire_user_id,
                    'customer_id',
                    command.customer_id)

        let mongo_res =
            await this.usersService
                .updateMongoUserByValuePair(
                    command.mongo_user_id,
                    'customer_id',
                    command.customer_id)

        return {
            mongo_res,
            fire_res,
        }
    }
}

@CommandHandler(AddSocialAccountToUserCommand)
export class AddSocialAccountToUserCommandHandler<T extends AddSocialAccountToUserCommand>
    implements ICommandHandler<T> {
    private readonly logger =
        new Logger(AddSocialAccountToUserCommandHandler.name)
    private readonly eventHandle: UserEventHandle

    constructor(
        private publisher: EventPublisher,
        private eventBus: EventBus,
    ) {
        // const blueprint = publisher.mergeClassContext(BattleEventHandle)
        // this.battleHandle = new blueprint()
        // this.battleHandle.autoCommit = true
        this.eventHandle =
            publisher.mergeObjectContext(
                new UserEventHandle(new ObjectId().toHexString()))
        this.eventHandle.autoCommit = true
    }

    async execute(command: T): Promise<any> {
        try {
            this.logger.debug('AddSocialAccountToUser command')

            const user_id = command.user_id
            const access_token = command.access_token

            this.logger.debug(command)

            await addSocialAccountToUser(
                user_id,
                access_token
            )

        } catch (error) {
            this.logger.error(error)
        }
    }
}