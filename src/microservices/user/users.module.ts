import { Module } from '@nestjs/common'
import { CqrsModule } from '@nestjs/cqrs'
import { UsersService } from './services/users.service'
import { UserSagas } from './sagas/users.saga'
import { UserEventHandle } from './events/users.aggregate'
import {
  CreateUserCommandHandler, AddSocialAccountToUserCommandHandler
} from './commands/users.commands'
import { GetUserByIdQuery, GetUserByIdQueryHandler, GetUsersByIdsQueryHandler } from './queries/user.queries'

const queryHandlers = [
  GetUserByIdQueryHandler,
  GetUsersByIdsQueryHandler,
]

const commandHandlers = [
  CreateUserCommandHandler,
  AddSocialAccountToUserCommandHandler,
]

const eventHandlers = [
]

@Module({
  imports: [
    CqrsModule,
  ],
  providers: [
    UsersService,
    ...queryHandlers,
    ...commandHandlers,
    ...eventHandlers,
    UserEventHandle,
    UserSagas,
  ],
  exports: [UsersService],
})
export class UsersModule { }
