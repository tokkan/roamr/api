export enum SocialProviders {
    FACEBOOK = 'Facebook',
    INSTAGRAM = 'Instagram',
    TWITTER = 'Twitter',
    TWITCH = 'Twitch',
    MICROSOFT = 'Microsoft',
    GOOGLE = 'Google',
}

export interface ISocialAccount {
    id: string
    username: string
    email: string
    access_token: string
    provider: SocialProviders
}