import { Logger } from "@nestjs/common"
import { AggregateRoot } from "@nestjs/cqrs"
import { ObjectId } from "mongodb"
import * as QRCode from 'qrcode'
import * as jimp from 'jimp'
import { RoleTypes } from "../models"
import { ISocialAccount } from "../models/social-account"

export class CreateUserEvent {
    constructor(
        public readonly user_fire_id: string,
        public readonly user_mongo_id: string,
        public readonly username: string,
        public readonly password: string,
        public readonly email: string,
        public readonly account: ISocialAccount,
        public readonly first_name: string,
        public readonly last_name: string,
        public readonly avatar: string,
        public readonly avatar_url: string,
        public readonly private_account: boolean,
        public readonly use_real_name: boolean,
        public readonly roles: RoleTypes[] = [],
    ) { }
}

export class AddSocialAccountToUserEvent {
    constructor(
        public readonly user_id: string,
        public readonly access_token: string,
    ) { }
}

export class UserEventHandle extends AggregateRoot {
    private readonly logger = new Logger(UserEventHandle.name)

    constructor(private readonly id: string) {
        super()
    }

    async createUser(
        user_fire_id: string,
        user_mongo_id: string,
        username: string,
        password: string,
        email: string,
        account: ISocialAccount,
        first_name: string,
        last_name: string,
        avatar: string,
        avatar_url: string,
        private_account: boolean,
        use_real_name: boolean,
        roles: RoleTypes[] = [],
    ) {

        if (roles.length <= 0) {
            roles.push(RoleTypes.PLEB)
        }

        this.apply(new CreateUserEvent(
            user_fire_id,
            user_mongo_id,
            username,
            password,
            email,
            account,
            first_name,
            last_name,
            avatar,
            avatar_url,
            private_account,
            use_real_name,
            roles,
        ))
        return { user_mongo_id, user_fire_id }
    }

    async addSocialAccountToUser(
        user_id: string,
        access_token: string,
    ) {
        this.apply(new AddSocialAccountToUserEvent(user_id, access_token))
        return { user_id, access_token }
    }
}