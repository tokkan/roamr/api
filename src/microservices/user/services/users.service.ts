import { Injectable, Logger } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { CommandBus, EventPublisher } from '@nestjs/cqrs'
import {
    getUserByUsername, checkIfUsernameExists, getUserByProp, getPasswordById, addSocialAccountToUser, getUserByDocument, updateFireUserByValuePairs
} from './../repositories/user-firebase.repository'
// import { toDataURL } from 'qrcode'
// import * as QRCode from 'qrcode'
// import * as jimp from 'jimp'
import { UserEventHandle } from '../events/users.aggregate'
import { createHmac } from 'crypto'
import { updateUserElementInArray, updateMongoUserByValuePairs, getMongoUserByProp, getMongoUsersByIds } from '../repositories/user.repository'
import { objectToValuePairs } from 'src/common/utility/pipe-utility'
import { IUserFireEntity, IUserMongoEntity, INotificationCredential, IUserEntityBaseArrayMap } from '../models'
import { ISocialAccount, SocialProviders } from '../models/social-account'
import { IMongoEntity, IFirestoreEntity, IMongoStringEntity } from 'src/database/models'
// import { checkIfNameExists } from '../players/players.repository'
// import { CurrencyService } from '../currency/currency.service'

// http://thecodebarbarian.com/creating-qr-codes-with-node-js.html

interface ICreateUserParams {
    username: string
    password: string
    // player_name: string
    // family_name: string
}

interface ICreateUserParamsBase {
    username: string
    password: string
    email: string
    access_token: string
    first_name: string
    last_name: string
    avatar: string
    avatar_url: string
    private_account: boolean
    use_real_name: boolean
}

interface ICreateMongoUserParams extends ICreateUserParamsBase, IMongoEntity, IFirestoreEntity {
}

interface ICreateFireUserParams extends ICreateUserParamsBase, IFirestoreEntity, IMongoStringEntity {
}

interface INameCheck<T extends string | boolean> {
    username: T
    // player_name: T
    // family_name: T
}

@Injectable()
export class UsersService {
    // private readonly users: User[]
    private readonly logger = new Logger(UsersService.name)
    private readonly users: IUserFireEntity[]
    private readonly eventHandle: UserEventHandle

    constructor(
        private readonly commandBus: CommandBus,
        // private readonly eventBus: EventBus,
        private readonly publisher: EventPublisher,
        // private readonly currencyService: CurrencyService,
    ) {
        this.eventHandle =
            publisher.mergeObjectContext(
                new UserEventHandle(new ObjectId().toHexString()))
        this.eventHandle.autoCommit = true
    }

    async createUserFromFirebase(payload: ICreateFireUserParams) {
        const body_values = Object.values(payload)

        if (body_values.some((s) => s === undefined)) {
            return {
                id: '',
                access_token: '',
                validation_passed: false,
            }
        }
        const {
            _id,
            fire_id,
            username,
            password,
            email,
            access_token,
            first_name,
            last_name,
            avatar,
            avatar_url,
            private_account,
            use_real_name,
        } = payload

        const hash =
            createHmac('sha512', process.env.ACCOUNT_KEY)
                .update(access_token)
                .digest('hex')

        let account: ISocialAccount = {
            id: '',
            access_token: hash,
            email: '',
            provider: SocialProviders.FACEBOOK,
            username: '',
        }

        let user_id =
            await this.eventHandle
                .createUser(
                    fire_id,
                    _id,
                    username,
                    password,
                    email,
                    account,
                    first_name,
                    last_name,
                    avatar,
                    avatar_url,
                    private_account,
                    use_real_name)

        // let res = await addSocialAccountToUser(id, access_token)

        return {
            id: user_id,
            access_token: access_token,
            validation_passed: true,
        }
    }

    async addSocialAccountToUser(user_id: string, access_token: string) {
        return await this.eventHandle
            .addSocialAccountToUser(user_id, access_token)
    }

    async findMongoUserById(user_id: ObjectId) {
        let res = await getMongoUserByProp('_id', user_id)
        return res
    }

    async findMongoUsersByIds(user_ids: ObjectId[]) {
        let res = await getMongoUsersByIds(user_ids)
        return res
    }

    async findOne(prop: keyof IUserFireEntity, input: string): Promise<IUserFireEntity | null> {
        const user = await getUserByProp(prop, input)

        if (user !== null) {
            return user
        } else {
            return null
        }
    }

    async findOneByDocument(input: string): Promise<IUserFireEntity | null> {
        const user = await getUserByDocument(input)

        if (user !== null) {
            return user
        } else {
            return null
        }
    }

    async findOneAdminByDocument(input: string): Promise<IUserFireEntity | null> {
        const user = await getUserByDocument(input)

        if (user !== null &&
            user.is_admin) {
            return user
        } else {
            return null
        }
    }

    async findOneByDocumentAndBusiness(input: string):
        Promise<IUserFireEntity | null> {
        const user = await getUserByDocument(input)

        if (user !== null &&
            user.is_admin) {
            return user
        } else {
            return null
        }
    }

    async findByUserName(username: string): Promise<IUserFireEntity | undefined> {
        const user = await getUserByUsername(username)

        if (user !== null) {
            return user
        } else {
            return null
        }
    }

    async checkIdAvailability(
        id: string
    ) {
    }

    async checkNameAvailability(
        name: string,
        // prop: 'username' | 'player_name' | 'family_name' = 'username'
        prop: 'username' = 'username'
    ) {
        if (prop === 'username') {
            return await checkIfUsernameExists(name)
        } else {
            // return await checkIfNameExists(name, prop)
        }
    }

    private nameCheck = (input: string, minLength: number = 3, maxLength: number = 15) => {
        const spaceCheck = new RegExp(/\s/)
        if (input.length >= 3 &&
            input.length <= 15 &&
            !spaceCheck.test(input)) {
            return true
        } else {
            return false
        }
    }

    private checkIfNamesAreValid(username: string) {

        const u = this.nameCheck(username)

        const res: INameCheck<boolean> = {
            username: u,
        }

        return res
    }

    async checkIfNamesAreAvailable(username: string) {
        const nres = this.checkIfNamesAreValid(username)
        const entries: boolean[] = Object.values(nres)

        if (entries.some(s => s === false)) {
            return nres
        }

        const usernameExists =
            await this.checkNameAvailability(username, 'username')

        const res: INameCheck<boolean> = {
            username: !usernameExists,
        }

        return res
    }

    async getPassword(user_id: ObjectId) {
        const pw = await getPasswordById(user_id)

        if (pw !== null) {
            return pw
        } else {
            return null
        }
    }

    async updateMongoUserByValuePair<
        T extends IUserMongoEntity,
        K extends keyof IUserMongoEntity,
        >(
            user_id: ObjectId,
            prop: K,
            value: T[K],
    ) {
        let res = await updateMongoUserByValuePairs(user_id, [{
            prop,
            value,
        }])
        return res
    }

    // async updateNotificationCredentials<
    //     T extends Partial<INotificationCredential>,
    //     K extends keyof Partial<INotificationCredential>
    // >(
    //     user_id: ObjectId,
    //     pairs: {
    //         prop: K,
    //         value: T[K],
    //     }[],
    //     array_filters: {
    //         prop: K,
    //         value: T[K],
    //     }[],
    // ) {

    // }

    async updateNotificationCredentials(
        user_id: ObjectId,
        entity: INotificationCredential,
        array_filters: Partial<INotificationCredential>,
    ) {
        let res =
            await updateUserElementInArray(
                user_id,
                'notification_credentials',
                entity,
                array_filters)

        this.logger.debug(this.updateNotificationCredentials.name)
        console.log(res)

        return res
        // let pairs = objectToValuePairs(entity)
        // let filters = objectToValuePairs(array_filters)
        // let res =
        //     await updateElementInArray(user_id, [
        //         {
        //             array_prop: 'notification_credentials',
        //             prop: 
        //         }
        //     ])
    }

    async updateUserElementInArray<
        // K extends keyof Pick<IUserEntityBase, 'notification_credentials' | 'roles' | 'role_access' | 'social_accounts'>,
        // T extends IUserEntityBaseArrayMap,
        // P extends keyof T,
        // V extends T[P],
        // U extends keyof T[P],
        P extends keyof IUserEntityBaseArrayMap,
        U extends keyof IUserEntityBaseArrayMap[P],
        V extends IUserEntityBaseArrayMap[P],
        U2 extends keyof IUserEntityBaseArrayMap[P],
        V2 extends IUserEntityBaseArrayMap[P],
        >(
            user_id: ObjectId,
            pairs: {
                // array_prop: K,
                array_prop: P,
                prop: U,
                value: V[U]
            }[],
            array_filters: {
                prop: U2,
                value: V2[U2]
            }[],
        // array_filters: {
        //     prop: P,
        //     value: U[P]
        // }[],
    ) {
        // // let res = await updateMongoUserByValuePairs(user_id, [{
        // //     prop,
        // //     value,
        // // }])
        // // return res

        // let res =
        //     await updateElementInArray(user_id, pairs, [
        //         {
        //             prop: 'device_id',
        //             value: '',
        //         }
        //     ])
        // return res
    }

    async updateFireUserByValuePair<
        T extends IUserFireEntity,
        K extends keyof IUserFireEntity,
        >(
            user_id: string,
            prop: K,
            value: T[K],
    ) {
        let res = await updateFireUserByValuePairs(user_id, [{
            prop,
            value,
        }])
        return res
    }
}