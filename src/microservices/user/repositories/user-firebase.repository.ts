// import { MongoMachine as db, DatabaseModels } from '../../database'
import { Firebase as db, DatabaseModels } from '../../../database/firebase/database'
import { ObjectId } from 'mongodb'
import { Logger } from '@nestjs/common'
import { createHmac } from 'crypto'
import { firestore } from 'firebase-admin'
import { IUserFireEntity } from '../models'
import { SocialProviders, ISocialAccount } from '../models/social-account'

const logger = new Logger('UserRepository')

async function parse<T>(data: any) {
    return await JSON.parse(JSON.stringify(data))
}

/**
 * 
 * @param user User
 * @param account_id id
 */
export async function createUserFire(user: IUserFireEntity) {
    const created = await _createUser(user)

    if (!created) {
        return null
    }
    return created
}

/**
 * Creates a new user
 * @param user User 
 */
async function _createUser(user: IUserFireEntity) {
    try {
        let c = db.collection(DatabaseModels.USERS)
        let response = await c.doc(user.fire_id).set(user)
        // let response = await c.insertOne(user)

        return response

    } catch (error) {
        logger.error(error)
    }
}

export async function updateUser(
    user_id: ObjectId,
    prop: keyof IUserFireEntity,
    input: any
) {
    try {
        let c = db.collection(DatabaseModels.USERS)
        let response = await c.where('_id', '==', user_id.toHexString()).get()
        response.docs[0].ref.update({
            [prop]: input
        })
        // const response = await c.updateOne({
        //     _id: user_id
        // }, {
        //     $push: {
        //         [prop]: input
        //     }
        // })

        return response

    } catch (error) {
        logger.error(error)
    }
}

export async function updateFireUserByValuePairs<
    T extends IUserFireEntity,
    K extends keyof IUserFireEntity,
>(
    id: string,
    valuePairs: { prop: K, value: T[K] }[]) {
    try {
        let c = db.collection(DatabaseModels.SE_PINS)
        let query = await c.doc(id).get()
        let entity = await parse<IUserFireEntity>(query.data())

        valuePairs.forEach((f) => {
            entity[f.prop] = f.value
        })

        let response = await c.doc(id).update(entity)
        return response
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function addSocialAccountToUser(
    user_id: string,
    social_access_token: string,
    provider: SocialProviders = SocialProviders.FACEBOOK
) {
    try {

        logger.debug(addSocialAccountToUser.name)
        // logger.debug(user_id)
        // logger.debug(social_access_token)
        // logger.debug(provider)

        const hash =
            createHmac('sha512', process.env.ACCOUNT_KEY)
                .update(social_access_token)
                .digest('hex')

        let account: ISocialAccount = {
            id: '',
            access_token: hash,
            email: '',
            provider,
            username: '',
        }

        // let c = db.collection(DatabaseModels.USERS)
        // let response =
        //     c.where('_id', '==', user_id)
        let c = db.collection(DatabaseModels.USERS)
        logger.debug('pineapple')
        // const response = await c.where("_id", "==", user_id).get()
        const response = c.doc(user_id)

        let res = await response.update({
            'social_accounts': firestore.FieldValue.arrayUnion(account)
        })

        return res
        // return null
    } catch (error) {
        logger.debug('error here')
        logger.error(error)
        throw error
    }
}

// export async function addCurrencyAccountToUser(
//     user_id: ObjectId,
//     account: string
// ) {
//     return await updateUser(
//         user_id,
//         'currency_accounts',
//         { address: account, name: 'Main' }
//     )
// }

// export async function addPlayerToUser(user_id: ObjectId, player_id: ObjectId) {
//     try {
//         // const store = await db.connect()
//         // const response =
//         //     await store
//         //         ?.collection(DatabaseModels.USERS)
//         //         .updateOne({
//         //             _id: user_id
//         //         }, {
//         //             // $addToSet: {
//         //             //     players: { $each: [player_id] }
//         //             // }
//         //             $push: {
//         //                 players: player_id
//         //             }
//         //         })

//         let c = await db.collection(DatabaseModels.USERS)
//         const response = await c.updateOne({
//             _id: user_id
//         }, {
//             // $addToSet: {
//             //     players: { $each: [player_id] }
//             // }
//             $push: {
//                 players: player_id
//             }
//         })

//         return response

//     } catch (error) {
//         logger.error(error)
//     }
// }

export async function checkIfUsernameExists(username: string): Promise<boolean | null> {
    try {
        let c = db.collection(DatabaseModels.USERS)
        let response = await c.where("username", "==", username).get()

        if (response) {
            return true
        } else {
            return false
        }
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function getUserByProp(prop: keyof IUserFireEntity, input: any)
    : Promise<IUserFireEntity> {
    try {
        let c = db.collection(DatabaseModels.USERS)
        const response = await c.where(prop, "==", input).get()

        if (!response.empty &&
            response.docs[0].data().hasOwnProperty(prop)) {
            return parse(response.docs[0].data())
        } else {
            return null
        }
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function getUserByDocument(input: any)
    : Promise<IUserFireEntity> {
    try {
        let c = db.collection(DatabaseModels.USERS)
        const response = await c.doc(input).get()

        if (response.exists) {
            return parse(response.data())
        } else {
            return null
        }
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function getUserByUsername(username: string): Promise<IUserFireEntity> {
    try {
        let c = db.collection(DatabaseModels.USERS)
        let response = await c.where("username", "==", username).get()
        return await JSON.parse(JSON.stringify(response.docs[0].data))
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function getPasswordById(id: ObjectId) {
    try {

        let c = db.collection(DatabaseModels.USERS)
        let response = await c.where("_id", "==", id).get()

        if (response.empty || response.docs[0].data === undefined) {
            return null
        }

        return response.docs[0].get("password")
        // }
    } catch (error) {
        return null
    }
}