import { Injectable, Logger } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { IInstantMessageChatTypes, IChatUser, IInstantMessage } from '../models'
import { createInstantMessageChat, doesInstantMessageChatExist, getChatMessages, getChatsForUser } from '../repositories/message-repository'

@Injectable()
export class MessagesService {
    private readonly logger = new Logger(MessagesService.name)

    async getChatsForUser(
        _id: ObjectId,
    ) {
        let res = await getChatsForUser(_id)
        return res
    }

    async getMessagesForChat(
        chat_id: ObjectId,
    ) {
        let res = await getChatMessages(chat_id)
        return res
    }

    async createInstantMessageChat(
        type: IInstantMessageChatTypes,
        participants: IChatUser[],
    ) {
        const exists =
            await doesInstantMessageChatExist(participants.map(m => m._id))

        if (exists === null) {
            return null
        }

        if (!exists) {
            let res =
                await createInstantMessageChat(
                    type,
                    participants,
                )
            return res
        }
        return null
    }
}
