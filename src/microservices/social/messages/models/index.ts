import { ObjectId } from "mongodb";
import { IMongoEntity } from "src/database/models"

export type IInstantMessageChatTypes =
    'single' |
    'group'

export interface IInstantMessageChatEntity extends IMongoEntity {
    type: IInstantMessageChatTypes
    participants: IChatUser[]
    messages: IInstantMessage[]
}

export interface IInstantMessage {
    // _id: string | number
    _id: ObjectId
    text: string
    createdAt: Date | number
    // user: IMessageUser
    user: ObjectId
    image?: string
    video?: string
    audio?: string
    system?: boolean
    sent?: boolean
    received?: boolean
    pending?: boolean
    quickReplies?: IQuickReplies
}

export interface IChatUser {
    _id: ObjectId
    name?: string
    avatar?: string
}

export interface IReply {
    title: string
    value: string
    messageId?: any
}

export interface IQuickReplies {
    type: 'radio' | 'checkbox'
    values: IReply[]
    keepIt?: boolean
}