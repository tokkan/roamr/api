import { Logger } from "@nestjs/common"
import { ObjectId } from "mongodb"
import { MongoDatabaseHandle as db, DatabaseModels } from "src/database/mongodb"
import { IChatUser, IInstantMessage, IInstantMessageChatTypes } from "../models"

const logger = new Logger('MessageRepository')

export const getChatsForUser = async (
    _id: ObjectId,
) => {
    try {
        let c = await db.collectionSocial(DatabaseModels.INSTANT_MESSAGES)
        let res =
            await c.find({
                'participants._id': _id,
            }).toArray()
        return res
    } catch (error) {
        console.debug(error)
        logger.error(error)
        return null
    }
}

export const getChatMessages = async (
    chat_id: ObjectId,
) => {
    try {
        let c = await db.collectionSocial(DatabaseModels.INSTANT_MESSAGES)
        let res =
            await c.findOne({
                _id: chat_id,
            })

            if (res) {
                return res.messages
            }
    } catch (error) {
        console.debug(error)
        logger.error(error)
        return null
    }
}

export const createInstantMessageChat = async (
    type: IInstantMessageChatTypes,
    participants: IChatUser[],
) => {
    try {
        let c = await db.collectionSocial(DatabaseModels.INSTANT_MESSAGES)
        let res = await c.insertOne({
            type,
            participants,
            messages: [],
        })
        return res
    } catch (error) {
        logger.debug(error)
        return null
    }
}

export const doesInstantMessageChatExist = async (
    participants: ObjectId[]
) => {
    try {
        let c = await db.collectionSocial(DatabaseModels.INSTANT_MESSAGES)
        let res = await c.findOne({
            type: 'single',
            participants: {
                $all: participants
            }
        })

        return res ? true : false
    } catch (error) {
        return null
    }
}