import { Body, Controller, InternalServerErrorException, Logger, Post, Res } from '@nestjs/common'
import { MessagePattern } from '@nestjs/microservices'
import { Response } from 'express'
import { ObjectId } from 'mongodb'
import { IChatUser, IInstantMessage, IInstantMessageChatTypes } from '../models'
import { MessagesService } from '../services/messages.service'

@Controller('messages')
export class MessagesController {
    private readonly logger = new Logger(MessagesController.name)

    constructor(
        private readonly messagesService: MessagesService,
    ) { }

    @MessagePattern({
        messages: 'get_chats_for_user'
    })
    async getChatsForUser(data: {
        _id: string
    }) {
        const id = new ObjectId(data._id)

        this.logger.debug(id)

        let response =
            await this.messagesService
                .getChatsForUser(id)
        return response
    }
    
    @MessagePattern({
        messages: 'get_chat_messages'
    })
    async getChatMessages(data: {
        _id: string
    }) {
        const id = new ObjectId(data._id)

        let response =
            await this.messagesService
                .getMessagesForChat(id)
        return response
    }

    @MessagePattern({
        messages: 'create_chat'
    })
    async createInstantMessageChat(
        data: {
            type: IInstantMessageChatTypes,
            participants: IChatUser[],
        },
    ) {
        const {
            type,
            participants,
        } = data

        this.logger.debug(this.createInstantMessageChat.name)
        this.logger.debug(data)

        switch (type) {
            case 'single':
                let response = await this.messagesService
                    .createInstantMessageChat(type, participants)
                if (response.result.ok > 0) {
                    return 200
                } else {
                    return 500
                }
            case 'group':
            default:
                break
        }
    }
}
