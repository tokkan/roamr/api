import { Module } from '@nestjs/common'
import { ClientsModule, Transport } from '@nestjs/microservices';
import { PostService } from './post/services/post.service';
import { FollowshipService } from './followship/followship.service';
import { MessagesModule } from './messages/messages.module';
import { PostController } from './post/controllers/post.controller';
import { PostModule } from './post/post.module';

@Module({
  imports: [
      ClientsModule.register([
          {
              name: 'GATEWAY_SERVICE',
              transport: Transport.TCP,
          }
      ]),
      MessagesModule,
      PostModule,
  ],
  providers: [FollowshipService],
  // controllers: [PostController],
})
export class SocialModule { }
