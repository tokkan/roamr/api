import { ObjectId } from "mongodb"
import { IMongoEntity } from "src/database/models"

export interface ICommentThreadEntity extends IMongoEntity {
    entity_id: ObjectId
    entity_type: 'post' | 'event'
    comments: ICommentSubEntity[]
}

/**
 * reply_id would be if a comment is a reply 
 * to another comment otherwise null
 * 
 * likes are ids/references to the user who liked the comment
 */
export interface ICommentSubEntity extends IMongoEntity {
    reply_id: ObjectId
    comment: string
    replies: ICommentSubEntity[]
    likes: ObjectId[]
}