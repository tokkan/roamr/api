import { ObjectId } from "mongodb"
import { IMongoEntity } from "src/database/models"

// export type ReactionTypes =
//     'like' |
//     'thumbs_up' |
//     'sad_face' |
//     'lol'

export type ReactionTypes =
    'like' 

export interface IReactionEntity extends IMongoEntity {
    // id: ObjectId
    type: 'post' | 'event' | 'comment'
    reactions: {
        user_id: ObjectId
        reaction: ReactionTypes  
    }[]
}