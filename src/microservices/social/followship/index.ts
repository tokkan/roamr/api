import {
    BulkWriteUpdateOneOperation,
    BulkWriteInsertOneOperation,
    BulkWriteDeleteOneOperation
} from "mongodb"
import { IFollowingEntity } from "./models"
export interface IJobAction<T, P> {
    type: T
    operation: P
}

export type JobActions =
    IFollowAction |
    IChangeAction |
    IUnfollowAction

export interface IFollowAction extends IJobAction<
    'follow',
    BulkWriteUpdateOneOperation<IFollowingEntity>
    > { }

export interface IChangeAction extends IJobAction<
    'change',
    BulkWriteUpdateOneOperation<IFollowingEntity>
    > { }

export interface IUnfollowAction extends IJobAction<
    'unfollow',
    BulkWriteUpdateOneOperation<IFollowingEntity>
    > { }

export interface IFollowJob {
    job: JobActions
}

export interface IFollowNotificationSettingsPayload {
    notify_posts?: boolean
    notify_events?: boolean
    notify_stories?: boolean
}

export interface IFollowMuteSettingsPayload {
    mute_posts?: boolean
    mute_events?: boolean
    mute_stories?: boolean
}