import { Injectable, Logger } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'
import { ObjectId, BulkWriteUpdateOneOperation, UpdateQuery, UpdateOneOptions, BulkWriteUpdateOperation, FilterQuery } from 'mongodb'
import { from, of, zip } from 'rxjs'
import { groupBy, mergeMap, toArray } from 'rxjs/operators'
import { IFollowJob } from '.'
import { FollowerTypes, IFollowingEntity, IFollowMuteSettings, IFollowNotificationSettings } from './models'
import { getFollowStats, getFollowingState, bulkFollowJobs, getFollowingOfUser } from './repositories/followship.repository'

export interface IFollowNotificationValuePair<T extends IFollowNotificationSettings, K extends keyof IFollowNotificationSettings> {
    prop: K
    value: T[K]
}

@Injectable()
export class FollowshipService {
    private readonly logger = new Logger(FollowshipService.name)
    private bulkFollowJobs: IFollowJob[] = []

    async getFollowStats(
        user_id: ObjectId,
    ) {
        let entity = await getFollowStats(user_id)
        let followers = 0
        let following = 0

        if (entity) {
            followers = entity.followers !== undefined ?
                entity.followers.length : 0
            following = entity.following !== undefined ?
                entity.following.length : 0
        }

        const res = {
            followers,
            following,
        }
        return res
    }

    async getFollowingState(
        source_user_id: ObjectId,
        target_id: ObjectId,
        type: FollowerTypes,
    ) {
        let entity =
            await getFollowingState(source_user_id, target_id, type)
        let isFollowingTarget = false
        // let interestType: InterestTypes | null = null
        let notification_settings: IFollowNotificationSettings = {
            notify_posts: false,
            notify_events: false,
            notify_stories: false,
        }
        let mute_settings: IFollowMuteSettings = {
            mute_posts: false,
            mute_events: false,
            mute_stories: false,
        }

        this.logger.debug(getFollowingState.name)
        console.log(source_user_id)
        console.log(target_id)
        console.log(entity)
        // console.log(entity.following[0].notification_settings)

        if (entity !== null) {
            isFollowingTarget = true
            notification_settings = entity.notification_settings
            mute_settings = entity.mute_settings
        }

        return {
            isFollowingTarget,
            notification_settings,
            mute_settings,
        }
    }

    async getFollowStatsAndUserRelated(
        source_user_id: ObjectId,
        target_id: ObjectId,
        type: FollowerTypes,
    ) {
        let target_stats = await this.getFollowStats(target_id)
        let user_related = await this.getFollowingState(
            source_user_id,
            target_id,
            type
        )

        this.logger.debug(this.getFollowStatsAndUserRelated.name)
        console.log(source_user_id)
        console.log(target_id)
        // console.log(user_related)
        this.logger.debug('return values')
        console.log(target_stats)
        console.log(user_related)

        return {
            ...target_stats,
            ...user_related,
        }
    }

    async getFollowers(
        user_id: ObjectId,
    ) {
    }

    async getFollowing(
        user_id: ObjectId,
    ) {
        let res = getFollowingOfUser(user_id)
        return res
    }

    async getSuggestedToFollow(
        user_id: ObjectId,
    ) {
    }

    async queFollowTarget(
        source_user_id: ObjectId,
        target_id: ObjectId,
        type: FollowerTypes,
    ) {
        let operation: BulkWriteUpdateOneOperation<IFollowingEntity> = {
            updateOne: {
                filter: {
                    user_id: source_user_id,
                },
                update: {
                    $addToSet: {
                        following: {
                            target_id: target_id,
                            follower_type: type,
                            notification_settings: {
                                notify_posts: false,
                                notify_events: false,
                                notify_stories: false,
                            },
                            mute_settings: {
                                mute_posts: false,
                                mute_events: false,
                                mute_stories: false,
                            },
                        }
                    }
                },
                upsert: true,
            }
        }

        let operation2: BulkWriteUpdateOneOperation<IFollowingEntity> = {
            updateOne: {
                filter: {
                    user_id: target_id,
                },
                update: {
                    $addToSet: {
                        followers: {
                            target_id: source_user_id,
                            follower_type: type,
                            notification_settings: {
                                notify_posts: false,
                                notify_events: false,
                                notify_stories: false,
                            },
                            mute_settings: {
                                mute_posts: false,
                                mute_events: false,
                                mute_stories: false,
                            },
                        }
                    }
                },
                upsert: true,
            }
        }
        const jobs: IFollowJob[] = [operation, operation2]
            .map(m => {
                return {
                    job: {
                        type: 'follow',
                        operation: m
                    }
                }
            })
        this.bulkFollowJobs.push(...jobs)
    }

    async queUnfollowTarget(
        source_user_id: ObjectId,
        target_id: ObjectId,
        type: 'user' | 'business' | 'zone' | 'municipality',
    ) {
        let operation: BulkWriteUpdateOneOperation<IFollowingEntity> = {
            updateOne: {
                filter: {
                    user_id: source_user_id,
                },
                update: {
                    $pull: {
                        following: {
                            target_id: target_id,
                            follower_type: type,
                        }
                    }
                },
                upsert: true,
            }
        }

        let operation2: BulkWriteUpdateOneOperation<IFollowingEntity> = {
            updateOne: {
                filter: {
                    user_id: target_id,
                },
                update: {
                    $pull: {
                        followers: {
                            target_id: source_user_id,
                            follower_type: type,
                        }
                    }
                },
                upsert: true,
            }
        }
        const jobs: IFollowJob[] = [operation, operation2]
            .map(m => {
                return {
                    job: {
                        type: 'unfollow',
                        operation: m
                    }
                }
            })
        this.bulkFollowJobs.push(...jobs)
    }

    async queChangeFollowshipOptions(
        source_user_id: ObjectId,
        target_id: ObjectId,
        target_type: 'user' | 'business' | 'zone' | 'municipality',
        // notification_settings: IFollowNotificationSettingsPayload,
        // mute_settings: IFollowMuteSettingsPayload,
        settings: object
    ) {

        this.logger.debug(this.queChangeFollowshipOptions.name)

        let update_op: {
            filter: FilterQuery<IFollowingEntity>
            update: UpdateQuery<IFollowingEntity>
            arrayFilters: object[]
        } = Object.entries(settings).reduce((prev, curr) =>
            ({
                filter: {
                    user_id: source_user_id,
                    // "following.target_id": target_id,
                    // "following.follower_type": target_type,
                },
                update: {
                    $set: {
                        // @ts-ignore
                        ...prev.update.$set,
                        // [`following.$[item].notification_settings.${curr[0]}`]: curr[1]
                        [curr[0]]: curr[1]
                    },
                },
                arrayFilters: [{
                    $and: [
                        { [`item.target_id`]: new ObjectId(target_id) },
                        { [`item.follower_type`]: target_type },
                    ]
                }]
            }), {
            filter: {},
            update: {},
            arrayFilters: [],
        })

        let operation: BulkWriteUpdateOneOperation<IFollowingEntity> = {
            updateOne: update_op,
        }
        this.bulkFollowJobs.push({
            job: {
                type: 'change',
                operation,
            }
        })
    }

    @Cron("0 */5 * * * *")
    async runFollowJobs() {
        if (this.bulkFollowJobs.length > 0) {
            this.logger.debug('Executing bulk jobs for followship')
            let jobs = this.bulkFollowJobs.splice(0, 10000)

            from(jobs).pipe(
                groupBy(g => g.job.type),
                mergeMap(group => zip(of(group.key),
                    group.pipe(toArray())))
            ).subscribe(async (s) => {
                switch (s[0]) {
                    case 'follow':
                        let addJobs: IFollowJob[] = []
                        addJobs.push(...s[1])
                        await bulkFollowJobs(addJobs.map(m => m.job.operation))
                        break
                    case 'change':
                        let changeJobs: IFollowJob[] = []
                        changeJobs.push(...s[1])
                        await bulkFollowJobs(changeJobs.map(m => m.job.operation))
                        break
                    case 'unfollow':
                        let deleteJobs: IFollowJob[] = []
                        deleteJobs.push(...s[1])
                        await bulkFollowJobs(deleteJobs.map(m => m.job.operation))
                        break
                    default:
                        break
                }
            })
        } else {
            this.logger.debug(`No followship jobs in queue`)
        }
    }
}
