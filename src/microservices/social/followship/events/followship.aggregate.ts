import { Logger } from "@nestjs/common"
import { AggregateRoot } from "@nestjs/cqrs"
import { ObjectId } from "mongodb"
import { FollowerTypes } from "../models"

export class FollowTargetEvent {
    constructor(
        public readonly source_id: ObjectId,
        public readonly target_id: ObjectId,
        public readonly target_type: FollowerTypes,
    ) { }
}

export class FollowshipEventHandle extends AggregateRoot {
    private readonly logger = new Logger(FollowshipEventHandle.name)

    constructor(private readonly id: string) {
        super()
    }

    async followTarget(
        source_id: ObjectId,
        target_id: ObjectId,
        target_type: FollowerTypes,
    ) {
        this.apply(new FollowTargetEvent(
            source_id,
            target_id,
            target_type
        ))
        return {
            source_id,
            target_id,
            target_type,
        }
    }
}