import { ObjectId } from "mongodb"
import { IMongoEntity } from "src/database/models"

export type FollowerTypes =
    'user' |
    'business' |
    'zone' |
    'municipality'

export type InterestTypes =
    'muted' |
    'following' |
    'post notifications'

export interface IFollowNotificationSettings {
    notify_posts: boolean
    notify_events: boolean
    notify_stories: boolean
}

export interface IFollowMuteSettings {
    mute_posts: boolean
    mute_events: boolean
    mute_stories: boolean
}

export interface IFollower {
    target_id: ObjectId
    follower_type: FollowerTypes
    notification_settings: IFollowNotificationSettings
    mute_settings: IFollowMuteSettings
}

export interface IFollowing {
    target_id: ObjectId
    follower_type: FollowerTypes
    notification_settings: IFollowNotificationSettings
    mute_settings: IFollowMuteSettings
}

export interface IFollowingEntity extends IMongoEntity {
    user_id: ObjectId
    followers: IFollower[]
    following: IFollowing[]
}