import { CommandHandler, ICommandHandler, EventPublisher, EventBus } from '@nestjs/cqrs'
import { Logger } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { FollowshipEventHandle } from '../events/followship.aggregate'
import { FollowshipService } from '../followship.service'
import { FollowerTypes } from '../models'

export class FollowTargetCommand {
    constructor(
        public readonly source_id: ObjectId,
        public readonly target_id: ObjectId,
        public readonly target_type: FollowerTypes,
    ) { }
}

@CommandHandler(FollowTargetCommand)
export class FollowTargetCommandHandler implements ICommandHandler<FollowTargetCommand> {
    private readonly logger = new Logger(FollowTargetCommandHandler.name)
    private readonly eventHandle: FollowshipEventHandle

    constructor(
        private publisher: EventPublisher,
        private eventBus: EventBus,
        private followshipService: FollowshipService,
    ) {
        this.eventHandle =
            this.publisher.mergeObjectContext(
                new FollowshipEventHandle(new ObjectId().toHexString()))
        this.eventHandle.autoCommit = true
    }

    async execute(command: FollowTargetCommand): Promise<any> {
        this.logger.debug('Executing followshipEventHandler')
        try {
            const {
                source_id,
                target_id,
                target_type,
            } = command
            this.followshipService.queFollowTarget(
                source_id,
                target_id,
                target_type,
            )
        } catch (error) {
            this.logger.error(error)
        }
    }
}