import { IQueryHandler, QueryHandler } from "@nestjs/cqrs"
import { ObjectId } from "mongodb"
import { FollowshipService } from "../followship.service"
import { IFollowing } from "../models"

export class GetFollowingByUserIdQuery {
    constructor(
        public readonly user_id: ObjectId,
    ) { }
}

@QueryHandler(GetFollowingByUserIdQuery)
export class GetFollowingByUserIdQueryHandler implements IQueryHandler<GetFollowingByUserIdQuery> {
    constructor(
        private readonly followshipService: FollowshipService,
    ) { }

    async execute(query: GetFollowingByUserIdQuery): Promise<IFollowing[]> {
        let res =
            await this.followshipService
                .getFollowing(query.user_id)
        return res
    }
}

// export class GetUsersByIdsQuery {
//     constructor(
//         public readonly user_ids: ObjectId[],
//     ) { }
// }

// @QueryHandler(GetUsersByIdsQuery)
// export class GetUsersByIdsQueryHandler implements IQueryHandler<GetUsersByIdsQuery> {
//     constructor(
//         private readonly usersService: UsersService,
//     ) { }

//     async execute(query: GetUsersByIdsQuery): Promise<IUserMongoEntity[]> {
//         let res =
//             await this.usersService
//                 .findMongoUsersByIds(query.user_ids)
//         return res
//     }
// }