import { Injectable, Logger } from "@nestjs/common"
import { Saga, ICommand, ofType } from "@nestjs/cqrs"
import { Observable } from "rxjs"
import { map, mergeMap } from 'rxjs/operators'
import { NotifyFollowTargetCommand } from "src/services/remote-notification/commands/remote-notifications.commands"
import { FollowTargetCommand } from "../commands/followship.commands"

@Injectable()
export class FollowshipSagas {
    private readonly logger = new Logger(FollowshipSagas.name)

    @Saga()
    followTarget = (events$: Observable<any>): Observable<ICommand> => {
        return events$
            .pipe(
                ofType(FollowTargetCommand),
                map(event => {
                    this.logger.debug('Saga follow target')
                    const {
                        source_id,
                        target_id,
                        target_type,
                    } = event

                    const commands: ICommand[] = [
                        new FollowTargetCommand(
                            source_id,
                            target_id,
                            target_type,
                        ),
                        new NotifyFollowTargetCommand(
                            source_id,
                            target_id,
                            target_type,
                        )
                    ]

                    return commands
                }),
                mergeMap(f => f),
            )
    }
}