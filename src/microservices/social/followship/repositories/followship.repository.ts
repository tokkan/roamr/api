import { Logger } from "@nestjs/common"
import { ObjectId, BulkWriteInsertOneOperation, BulkWriteUpdateOneOperation } from "mongodb"
import { MongoDatabaseHandle as db, DatabaseModels } from "src/database/mongodb"
import { FollowerTypes, IFollowing, IFollowingEntity } from "../models"

const logger = new Logger('FollowshipRepository')

export async function getFollowStats(
    user_id: ObjectId,
) {
    try {
        let c = await db.collectionSocial(DatabaseModels.FOLLOWSHIP)
        let res = await c.findOne({
            user_id,
        })
        return res
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function getFollowingState(
    source_id: ObjectId,
    target_id: ObjectId,
    target_type: FollowerTypes,
) {
    try {
        let c = await db.collectionSocial(DatabaseModels.FOLLOWSHIP)

        let res = await c.findOne({
            user_id: source_id,
            following: {
                $elemMatch: {
                    target_id: target_id
                    // target_id: new ObjectId('')
                }
            }
        }, {
            projection: {
                following: 1,
            }
        })

        if (res && res.following.length > 0) {
            return res.following[0]
        } else {
            return null
        }
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function getFollowingOfUser(
    user_id: ObjectId,
) {
    try {
        let f = await db.collectionSocial(DatabaseModels.FOLLOWSHIP)
        let following: IFollowing[] =
            await f.find<IFollowing>({
                user_id: user_id
            }).project({
                following: 1,
            }).toArray()

        return following
    } catch (error) {
        logger.log(error)
        return null
    }
}

export async function bulkFollowJobs(
    bulkJobs: BulkWriteUpdateOneOperation<IFollowingEntity>[],
) {
    try {
        let c = await db.collectionSocial(DatabaseModels.FOLLOWSHIP)
        let res = await c.bulkWrite(bulkJobs)
        logger.debug(res)
        return res
    } catch (error) {
        logger.error(error)
        return null
    }
}