import { Logger } from "@nestjs/common"
import { ObjectId, BulkWriteInsertOneOperation } from "mongodb"
import { MongoDatabaseHandle as db, DatabaseModels } from 'src/database/mongodb'
import { IFollowing } from "../../followship/models"
import { IPostEntity } from "../models"

const logger = new Logger('PostsRepository')

export async function getPostById(post_id: ObjectId) {
    try {
        let c = await db.collectionSocial(DatabaseModels.POSTS)
        let res = await c.findOne({
            _id: post_id
        })

        if (res) {
            return res
        } else {
            return null
        }
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function getPostsForUserId(user_id: ObjectId) {
    try {
        let c = await db.collectionSocial(DatabaseModels.POSTS)
        let u = await db.collectionSocial(DatabaseModels.USERS)
        let user = u.findOne({
            _id: user_id,
        })

        if (user) {
            let posts =
                await c.find({
                    poster_id: user_id
                }).toArray()

            if (posts) {
                return posts
            }
        }
        return []
    } catch (error) {
        logger.error(error)
        return []
    }
}

// TODO: Get user's posts or not?
// TODO: Somehow group or aggregate data to get good posts, perhaps start with some sorting by latest and continue evolving the algorithm with time
// TODO: See if RxJS could be used
export async function getPostsForUserTimeline(
    user_id: ObjectId,
    limit: number,
    skip: number,
) {
    try {
        let c = await db.collectionSocial(DatabaseModels.POSTS)
        let u = await db.collectionSocial(DatabaseModels.USERS)
        let f = await db.collectionSocial(DatabaseModels.FOLLOWSHIP)
        let user = await u.findOne({
            _id: user_id,
        })

        logger.debug('get posts repository')
        // logger.debug(user)
        // logger.debug(user_id)

        if (user) {
            let posts =
                await c.find({
                    // poster_id: user_id
                }).toArray()

            // logger.debug(posts)

            if (posts) {
                return posts
            }

            let following: IFollowing[] =
                await f.find<IFollowing>({
                    user_id: user_id
                }).project({
                    following: 1,
                }).toArray()

            if (following.length <= 0) {
                return []
            }

            let query:
                Pick<IPostEntity, 'poster_id' | 'posted_by_type'>[] =
                following.map(m => {
                    return {
                        poster_id: m.target_id,
                        posted_by_type: m.follower_type
                    }
                })

            logger.debug(getPostsForUserTimeline.name)
            console.log(following)
            console.log(query)

            // let posts =
            //     await c.find(
            //         {
            //             $or: query,
            //             // poster_id: { $in: follower_ids.map(m => m.target_id) }
            //         })
            //         .sort('')
            //         .limit(limit)
            //         .skip(skip)
            //         .toArray()

            // return posts
        }

        return []
    } catch (error) {
        console.log('getPostsFromUserTimeLine Error')
        console.log(error)
        logger.error(error)
        return []
    }
}

export async function createPostFromEntity(entity: IPostEntity) {
    try {
        let c = await db.collectionSocial(DatabaseModels.POSTS)

        console.log(entity)

        let response = await c.insertOne(entity)

        logger.debug('After post insert')
        logger.debug(response)

        return response

    } catch (error) {
        console.log(error)
        logger.error(error)
        return null
    }
}

export async function bulkAddPosts(
    bulkJobs: BulkWriteInsertOneOperation<IPostEntity>[]
) {
    try {
        let c = await db.collectionSocial(DatabaseModels.POSTS)
        let res = await c.bulkWrite(bulkJobs)
        return res
    } catch (error) {
        logger.error(error)
        return null
    }
}