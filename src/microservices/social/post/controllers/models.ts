export interface IImageAssetPayload {
    // base64: string
    // width: number
    // height: number
    // size: number
    // mime: string
    // // mime: 'image/jpg' | 'image/png' | 'video/mp4'
    data: string,
    modificationDate: string,
    size: number,
    mime: string,
    height: number,
    width: number,
    path: string,
}