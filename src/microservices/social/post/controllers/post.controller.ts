import { Controller, Logger, UseInterceptors } from '@nestjs/common'
import { EventPattern, MessagePattern } from '@nestjs/microservices'
import { FilesInterceptor } from '@nestjs/platform-express/multer/interceptors/files.interceptor'
import { ObjectId } from 'mongodb'
import { FormDataInterceptor } from 'src/common/interceptors/form-data-interceptor'
import { ContentTypes } from 'src/microservices/storage/models'
import { EventPatternTypes, IMessagePatterns, MessagePatternTypes } from '.'
import { PosterTypes, ITaggedInPost } from '../models'
import { PostService } from '../services/post.service'
import { IImageAssetPayload } from './models'

@Controller('post')
export class PostController {
    private readonly logger = new Logger(PostController.name)

    constructor(
        private readonly postService: PostService,
    ) { }

    @MessagePattern<IMessagePatterns>({
        messages: 'get_posts_for_user_timeline'
    })
    async getPostsForUserTimeline(
        data: {
            user_id: string
            limit: number
            skip: number
        }
    ) {
        const _id = new ObjectId(data.user_id)
        const {
            limit,
            skip,
        } = data
        let res =
            await this.postService.getPostsForUserTimeline(
                _id,
                limit,
                skip,
            )
        return res
    }

    // @UseInterceptors(FilesInterceptor('assets'))
    // @UseInterceptors(FormDataInterceptor)
    @EventPattern<EventPatternTypes>('create_post')
    async createPost(
        data: {
            payload: {
                poster_id: ObjectId
                poster_user_id: ObjectId
                posted_by_type: PosterTypes
                caption: string
                assets: IImageAssetPayload[]
                is_comments_enabled: boolean
                tagged_in_post: ITaggedInPost[]
                coordinates: number[]
                is_location_enabled: boolean
            },
            assets: any[],
            type: string,
            metadata: {
                [key: string]: string
                contentType: ContentTypes
            },
        }
    ) {
        this.logger.debug(this.createPost.name)
        // let post = JSON.parse(JSON.stringify(data.post))
        // this.logger.debug(post)
        // this.logger.debug(data)

        // let keys = Object.keys(data)

        // console.log(keys)
        // // console.log(request[keys[0]])
        // // request = JSON.stringify(request)

        // // keys.forEach(f => {
        // //     // // request.body[f] = JSON.parse(request.body[f])
        // //     // // if (typeof request[f] === 'string') {
        // //     data[f] = JSON.parse(data[f])
        // //     // // }
        // //     // // request[f] = JSON.parse(JSON.stringify(request[f]))
        // // })

        // this.logger.debug(data.payload)
        // this.logger.debug(data.payload.caption)
        const {
            type,
            assets,
            payload,
            metadata,
        } = data
        let res =
            await this.postService.queueCreatePost(
                payload,
                type,
                assets,
                metadata,
            )
        return res
    }
}
