export interface IMessagePatterns {
    messages: MessagePatternTypes
}

export type MessagePatternTypes =
    'get_posts_for_user_timeline' 

export type EventPatternTypes =
    'create_post'

