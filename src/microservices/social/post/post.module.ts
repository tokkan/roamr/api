import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { PostController } from './controllers/post.controller';
import { PostService } from './services/post.service';

@Module({
    imports: [
        CqrsModule,
    ],
    controllers: [
        PostController,
    ],
    providers: [
        PostService,
    ],
    exports: [
        PostService,
    ]
})
export class PostModule {}
