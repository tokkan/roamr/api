import { ObjectId } from "mongodb";

export interface IUserProjection {
    _id: ObjectId
    username: string
    avatar: string
}