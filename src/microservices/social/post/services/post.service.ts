import { Injectable, Logger } from '@nestjs/common'
// import { StorageService } from '../../storage/storage.service'
import { ObjectId, BulkWriteInsertOneOperation } from 'mongodb'
import { IPostEntity, PosterTypes, ITaggedInPost } from './../models'
import { IPostJob } from './..'
import { Cron } from '@nestjs/schedule'
import { from, zip, of } from 'rxjs'
import { groupBy, mergeMap, toArray } from 'rxjs/operators'
import {
    bulkAddPosts, createPostFromEntity, getPostsForUserTimeline
} from './../repositories/post.repository'
import { QueryBus } from '@nestjs/cqrs'
import { GetUsersByIdsQuery } from 'src/microservices/user/queries/user.queries'
import { IUserProjection } from '.'
import { uploadFileToGridFS } from 'src/microservices/storage/repositories/storage.repository'
import { ContentTypes } from 'src/microservices/storage/models'

export type FeedItemTypes =
    'post' |
    'event'

export interface IFeedItem<T extends FeedItemTypes, I> {
    type: T
    item: I
}

export interface IPostProjection {
    _id: ObjectId
    poster_id: ObjectId
    poster_user_id: ObjectId
    posted_by_type: PosterTypes
    caption: string
    // images: string[]
    images: ObjectId[]
    is_comments_enabled: boolean
    tagged_in_post: ITaggedInPost[]
    like_count: number
    comment_count: number
    user_id: ObjectId
    username: string
    avatar: string
}

export interface IPostFeedItem extends IFeedItem<
    'post',
    IPostProjection
    > { }

export interface IEventFeedItem extends IFeedItem<
    'event',
    {
        _id: string
        name: string
    }> { }

export type FeedItems =
    IPostFeedItem |
    IEventFeedItem

@Injectable()
export class PostService {
    private readonly logger = new Logger(PostService.name)
    private postJobs: IPostJob[] = []

    constructor(
        // private readonly storageService: StorageService,
        private readonly queryBus: QueryBus,
    ) { }

    private async getUsers(user_ids: ObjectId[]) {
        let users =
            await this.queryBus
                .execute<
                    GetUsersByIdsQuery,
                    IUserProjection[]
                >(new GetUsersByIdsQuery(user_ids))

        return users
    }

    async getPostsForUserTimeline(
        user_id: ObjectId,
        limit: number,
        skip: number
    ) {
        this.logger.debug(getPostsForUserTimeline.name)
        const feedItems: FeedItems[] = []
        let posts = await getPostsForUserTimeline(user_id, limit, skip)

        const user_ids = posts.map(m => m.poster_user_id)
        const users = await this.getUsers(user_ids)

        posts.forEach(f => {
            const user = users.find(q =>
                q._id.toHexString() === f.poster_user_id.toHexString())

            if (user === undefined ||
                user === null) {
                console.debug('failed')
                return
            }

            feedItems.push({
                type: 'post',
                item: {
                    ...f,
                    user_id: user._id,
                    username: user.username,
                    avatar: user.avatar,
                }
            })
        })

        return feedItems
    }

    async getPostComments(
        post_id: ObjectId,
    ) {
    }

    async queueCreatePost(
        post: {
            poster_id: ObjectId,
            poster_user_id: ObjectId,
            posted_by_type: PosterTypes,
            caption: string,
            is_comments_enabled: boolean,
            tagged_in_post: ITaggedInPost[],
            coordinates: number[],
            is_location_enabled: boolean,
        },
        type: string,
        assets: any[],
        metadata: {
            [key: string]: string
            contentType: ContentTypes
        }
    ) {
        this.logger.debug(this.queueCreatePost.name)
        // this.logger.debug(assets[0])
        // this.logger.debug(post)
        const images: ObjectId[] = []
        assets.forEach(f => {
            console.log('assets foreach')
            // const b = Buffer.from(f.data)
            const b = Buffer.from(`${f.data}`)
            let id = uploadFileToGridFS(`post`, b, metadata)
            console.log(id)
            console.log('after buffer')
            images.push(id)
        })

        let entity: IPostEntity = {
            // fire_id: '',
            _id: new ObjectId(),
            poster_id: new ObjectId(post.poster_id),
            poster_user_id: new ObjectId(post.poster_user_id),
            posted_by_type: post.posted_by_type,
            caption: post.caption,
            // images: assetIds.map(m => m.toHexString()),
            // images: assets,
            images,
            is_comments_enabled: post.is_comments_enabled,
            tagged_in_post: post.tagged_in_post,
            coordinates: post.coordinates,
            is_location_enabled: post.is_location_enabled,
            comment_count: 0,
            like_count: 0,
        }
        let operation: BulkWriteInsertOneOperation<IPostEntity> = {
            insertOne: {
                document: entity
            }
        }
        this.postJobs.push({
            job: {
                type: 'add',
                operation,
            }
        })
    }

    async createPost(
        post: {
            poster_id: ObjectId,
            poster_user_id: ObjectId,
            posted_by_type: PosterTypes,
            caption: string,
            is_comments_enabled: boolean,
            tagged_in_post: ITaggedInPost[],
            coordinates: number[],
            is_location_enabled: boolean,
        },
        type: string,
        assets: any[]
    ) {
        this.logger.debug(this.createPost.name)
        // const assetIds: ObjectId[] = []

        // for (let i = 0; i < assets.length; i++) {
        //     const asset_data = assets[i].data
        //     let assetType: 'jpeg' | 'video' | undefined = undefined

        //     if (assets[i].mime.startsWith('image')) {
        //         assetType = 'jpeg'
        //     } else {
        //         assetType = 'video'
        //     }

        //     if (assetType === undefined) {
        //         this.logger.debug('Incorrect mime type')
        //         return
        //     }

        //     let file_id =
        //         await this.storageService.uploadAsset(
        //             assetPath,
        //             asset_data,
        //             assetType
        //         )

        //     assetIds.push(file_id)
        // }

        let entity: IPostEntity = {
            // fire_id: '',
            _id: new ObjectId(),
            poster_id: post.poster_id,
            poster_user_id: post.poster_user_id,
            posted_by_type: post.posted_by_type,
            caption: post.caption,
            // images: assetIds.map(m => m.toHexString()),
            images: assets,
            is_comments_enabled: post.is_comments_enabled,
            tagged_in_post: post.tagged_in_post,
            coordinates: post.coordinates,
            is_location_enabled: post.is_location_enabled,
            comment_count: 0,
            like_count: 0,
        }

        let response = await createPostFromEntity(entity)
        if (response.result.ok === 1) {
            return true
        } else {
            return false
        }
    }

    @Cron("0 */2 * * * *")
    private async bulkWriteJob() {
        if (this.postJobs.length > 0) {
            this.logger.debug('Executing bulk jobs for posts')
            let jobs = this.postJobs.splice(0, 10000)

            from(jobs).pipe(
                groupBy(g => g.job.type),
                mergeMap(group => zip(of(group.key),
                    group.pipe(toArray())))
            ).subscribe(async (s) => {
                switch (s[0]) {
                    case 'add':
                        let ops = s[1]
                            .filter(f => f.job.type === 'add')
                            .map<BulkWriteInsertOneOperation<IPostEntity>>(m => {
                                return m.job.operation as BulkWriteInsertOneOperation<IPostEntity>
                            })
                        await bulkAddPosts(ops)
                        break
                    case 'change':
                        // let changeJobs: IPostJob[] = []
                        // addJobs.push(...s[1])
                        // await bulkAddPosts(changeJobs.map(m => m.job.operation))
                        break
                    case 'remove reaction':
                        // let deleteJobs: IPostJob[] = []
                        // deleteJobs.push(...s[1])
                        // await bulkAddPosts(deleteJobs.map(m => m.job.operation))
                        break
                    default:
                        break
                }
            })
        } else {
            this.logger.debug(`No social data jobs in queue`)
        }
    }
}
