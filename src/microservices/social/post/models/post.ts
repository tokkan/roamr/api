import { ObjectId } from "mongodb"
import { IMongoEntity } from "src/database/models"

export type PosterTypes =
    'user' |
    'business' |
    'zone' |
    'municipality'

export interface ITaggedInPost {
    _id: ObjectId
    type: 'user' | 'business' | 'zone' | 'municipality'
}

export interface IPostEntity extends IMongoEntity {
    poster_id: ObjectId
    poster_user_id: ObjectId
    posted_by_type: PosterTypes
    caption: string
    // images: string[]
    images: ObjectId[]
    is_comments_enabled: boolean
    tagged_in_post: ITaggedInPost[]
    like_count: number
    comment_count: number
    coordinates: number[]
    is_location_enabled: boolean
}

export type ReactionTypes = 
    'like'

export interface IReaction {
    type: ReactionTypes
    id: ObjectId
}

export interface IPostActivityEntity extends IMongoEntity {
    likes: ObjectId[]
}