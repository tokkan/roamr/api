import { ObjectId } from "mongodb"
import { PosterTypes, ITaggedInPost } from "."

export interface ICreatePostPayload {
    post: {
        poster_id: ObjectId,
        poster_user_id: ObjectId,
        posted_by_type: PosterTypes,
        caption: string,
        is_comments_enabled: boolean,
        tagged_in_post: ITaggedInPost[],
        coordinates: number[],
        is_location_enabled: boolean,
    },
    assetPath: string,
    assets: any[]
}