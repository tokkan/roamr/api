import { CommandHandler, ICommandHandler, EventPublisher, EventBus } from '@nestjs/cqrs'
import { Logger } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { PostEventHandle } from '../events/create-post.aggregate'
import { PostService } from '../services/post.service'
import { ICreatePostPayload } from '../models/payloads'

export class CreatePostCommand {
    constructor(
        public readonly entity: ICreatePostPayload,
    ) { }
}

@CommandHandler(CreatePostCommand)
export class CreatePostCommandHandler implements ICommandHandler<CreatePostCommand> {
    private readonly logger = new Logger(CreatePostCommandHandler.name)
    private readonly eventHandle: PostEventHandle

    constructor(
        private publisher: EventPublisher,
        private eventBus: EventBus,
        private postService: PostService,
    ) {
        this.eventHandle =
            publisher.mergeObjectContext(
                new PostEventHandle(new ObjectId().toHexString()))
        this.eventHandle.autoCommit = true
    }

    async execute(command: CreatePostCommand): Promise<any> {
        this.logger.debug('Executing PostEventHandler')
        try {
            const {
                entity,
            } = command
            await this.postService
                .createPost(
                    entity.post,
                    entity.assetPath,
                    entity.assets,
                )
        } catch (error) {
            this.logger.error(error)
        }
    }
}