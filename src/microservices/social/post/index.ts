import { BulkWriteUpdateOneOperation, BulkWriteInsertOneOperation, BulkWriteDeleteOneOperation } from "mongodb"
import { IPostEntity } from './models'

export interface IJobAction<T, P> {
    type: T
    operation: P
}

export type JobActions =
    IAddAction |
    IChangeAction |
    IDeleteAction

export interface IAddAction extends IJobAction<
    'add',
    BulkWriteInsertOneOperation<IPostEntity>
    > { }

export interface IChangeAction extends IJobAction<
    'change',
    BulkWriteUpdateOneOperation<IPostEntity>
    > { }

export interface IDeleteAction extends IJobAction<
    'remove reaction',
    BulkWriteDeleteOneOperation<IPostEntity>
    > { }

export interface IPostJob {
    job: JobActions
}