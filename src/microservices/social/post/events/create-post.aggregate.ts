import { Logger } from "@nestjs/common"
import { AggregateRoot } from "@nestjs/cqrs"
import { ObjectId } from "mongodb"
import { FollowerTypes } from "src/microservices/social/models/following/following"
import { ICreatePostPayload } from "../models/payloads"

export class CreatePostEvent {
    constructor(
        public readonly entity: ICreatePostPayload,
    ) { }
}

export class PostEventHandle extends AggregateRoot {
    private readonly logger = new Logger(PostEventHandle.name)

    constructor(private readonly id: string) {
        super()
    }

    async createPost(
        entity: ICreatePostPayload,
    ) {
        this.apply(new CreatePostEvent(
            entity,
        ))
        return {
            entity,
        }
    }
}