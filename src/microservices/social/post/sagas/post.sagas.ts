import { Injectable, Logger } from "@nestjs/common"
import { Saga, ICommand, ofType } from "@nestjs/cqrs"
import { Observable } from "rxjs"
import { map, delay, mergeMap } from 'rxjs/operators'
import { CreatePostCommand } from "../commands/create-post.command"
import { CreatePostEvent } from "../events/create-post.aggregate"

@Injectable()
export class PostSagas {
    private readonly logger = new Logger(PostSagas.name)

    @Saga()
    postCreated = (events$: Observable<any>): Observable<ICommand> => {
        return events$
            .pipe(
                ofType(CreatePostEvent),
                // delay(1000),
                map(event => {
                    const {
                        entity,
                    } = event

                    const commands: ICommand[] = [
                        new CreatePostCommand(
                            entity
                        ),
                    ]

                    return commands
                }),
                mergeMap(f => f),
            )
    }
}