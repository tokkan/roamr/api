import { Logger } from "@nestjs/common"
import { AggregateRoot } from "@nestjs/cqrs"
import { ObjectId } from "mongodb"
import { IPinEntity } from "../models"

export class CreatePinEvent {
    constructor(
        public readonly entity: IPinEntity
    ) { }
}

export class ActivatePinEvent {
    constructor(
        public readonly id: ObjectId,
    ) { }
}

export class PinEventHandle extends AggregateRoot {
    private readonly logger = new Logger(PinEventHandle.name)

    constructor(private readonly id: string) {
        super()
    }

    async createPin(
        entity: IPinEntity
    ) {
        this.apply(new CreatePinEvent(entity))
        return entity._id
    }

    async activatePin(
        id: ObjectId
    ) {
        this.apply(new ActivatePinEvent(id))
        return id
    }
}