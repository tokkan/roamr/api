import { ImageSourceTypes, LanguageTypes } from "src/models/common/common";

export interface IPinRating {
    is_sponsored: boolean
    likes: number
    clicks: number
    following: number
}

// export interface ISuspension {
//     is_suspended: boolean
//     is_banned: boolean
//     timeout: number
// }

// export enum LanguageTypes {
//     SE = 'se',
//     EN = 'en',
//     DK = 'dk',
//     NL = 'nl',
// }

// export enum PinTypes {
//     camping = 'Camping',
//     cafe = 'Cafe',
//     restaurant = 'Restaurant',
//     trail = 'Trail',
//     swimming = 'Swimming',
//     fishing = 'Fishing',
//     bnb = 'bnb',
//     rest_area = 'Rest area',
//     store = 'Store',
//     heritage = 'Heritage',
//     nature_reserve = 'Nature reserve',
//     poi = 'poi',
//     EVENT = 'Event',
//     AREA = 'Area',
//     BUSINESS = 'Business',
// }

// export enum TrailTypes {
//     HIKING = 'Hiking',
//     BIKING = 'Biking',
// }

// export enum StoreTypes {
//     PHYSICAL_ITEMS = 'Physical items',
//     CONSUMABLES = 'Consumables',
//     MEDICINE = 'Medicine',
// }

// export type AllPinTypes =
//     PinTypes |
//     TrailTypes |
//     StoreTypes

// export interface IPinImageInfo {
//     title: string
//     description: string
//     lang: LanguageTypes
// }

// export interface IPinImageWithInfo {
//     source: string
//     source_type: ImageSourceTypes
//     alt: string
//     info: IPinImageInfo[]
// }

export interface IPinImage {
    source: string
    source_type: ImageSourceTypes
    alt: string
}

export interface IPinInfo {
    title: string
    sub_title: string
    description: string[]
    lang: LanguageTypes
    image: IPinImage
    index: number
}