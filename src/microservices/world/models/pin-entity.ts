import { ObjectId } from "mongodb"
import {
    IPinRating,
} from "."
import { PinTypes, SubPinTypes } from "./pin-types"
import { IPinInfo } from "./pin"
import { IFirestoreEntity, IMongoEntity } from "src/database/models"
import {
    GeoTypes, IGeoBaseEntity, IGeoProperties, IPointAction
} from "../common"

export interface IConnectedGeometry {
    _id: ObjectId
    geo_type: Extract<GeoTypes, 'trail' | 'area'>
    index: number
}

export interface IPinGeoProperties extends IGeoProperties {
    _id: string
    pin_type: PinTypes
    sub_pin_types: PinTypes[]
}

export interface IPinEntityBase extends IGeoBaseEntity {
    properties: IPinGeoProperties
    geometry: IPointAction
    geo_ids: IConnectedGeometry[],
    // zone_ids: ObjectId[]
    pin_type: PinTypes
    // sub_pin_types: (PinTypes & SubPinTypes)[]
    sub_pin_types: PinTypes[]
    info: IPinInfo[]
    rating: IPinRating
    // images:
}

// export interface IPinFirestoreEntity extends IFirestoreEntity, IMongoStringEntity, IPinEntityBase {
//     municipality_fire_id: string
//     municipality_mongo_id: string
// }

export interface IPinEntity extends IMongoEntity, IPinEntityBase {
    // municipality_fire_id: string
    municipality_mongo_id: ObjectId
}