import { ObjectId } from "mongodb"
import { IMongoEntity } from "src/database/models"
import { GeoTypes, IGeoProperties, IPolyAction } from "../../common"

export interface IMunicipalityCodeBasic {
    county_code: string
    municipality_code: string
    name: string
}

export interface IMunicipalityCodeEntity extends
    IMunicipalityCodeBasic {
}

// export interface IMunicipalityEntity extends IMongoStringEntity {
//     category_code: number
//     category: string
//     county_code: number
//     county_name: string
//     municipality_code: number
//     municipality_name: string
//     full_county_code: string
//     full_municipality_code: string
//     location: firestore.GeoPoint[]
// }

export interface IMunicipalityMongoProperties {
    _id: ObjectId
    fire_id: string
    geo_type: GeoTypes
    category_code: number
    category: string
    county_code: number
    county_name: string
    municipality_code: number
    municipality_name: string
    full_county_code: string
    full_municipality_code: string
}

export interface IMunicipalityMongoEntity extends IMongoEntity {
    type: string
    properties: IMunicipalityMongoProperties & IGeoProperties
    geometry: IPolyAction
}

// interface IMunicipalityProperties {
//     category_code: number
//     category: string
//     county_code: number
//     county_name: string
//     municipality_code: number
//     municipality_name: string
//     full_county_code: string
//     full_municipality_code: string
// }