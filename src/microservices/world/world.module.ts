import { Module } from '@nestjs/common'
import { BullModule } from '@nestjs/bull'
import { CqrsModule } from '@nestjs/cqrs'

const queryHandlers = [
]

const commandHandlers = [
]

const eventHandlers = [
]

export enum RedisQueues {
    PINS = 'pins',
    TRAILS = 'trails',
    AREAS = 'areas',
    ZONES = 'zones',
    MUNICIPALITIES = 'municipalities',
}

export enum RedisJobNames {
    PIN = 'pin',
    PIN_ADD = 'pin_add',
}

@Module({
    imports: [
        CqrsModule,
        BullModule.registerQueue(
            {
                name: RedisQueues.PINS,
                redis: {
                    host: process.env.REDIS_HOST,
                    port: +process.env.PORT,
                }
            },
            {
                name: RedisQueues.TRAILS,
                redis: {
                    host: process.env.REDIS_HOST,
                    port: +process.env.PORT,
                }
            },
        )
    ]
})
export class WorldModule { }
