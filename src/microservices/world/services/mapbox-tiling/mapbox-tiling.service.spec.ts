import { Test, TestingModule } from '@nestjs/testing';
import { MapboxTilingService } from './mapbox-tiling.service';

describe('MapboxTilingService', () => {
  let service: MapboxTilingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MapboxTilingService],
    }).compile();

    service = module.get<MapboxTilingService>(MapboxTilingService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
