import { Injectable, Logger, OnModuleInit, UnprocessableEntityException } from '@nestjs/common'
import mbxStyles, { StylesService } from '@mapbox/mapbox-sdk/services/styles'
import mbxTilesets, { TilesetsService } from '@mapbox/mapbox-sdk/services/tilesets'
import { SdkConfig } from '@mapbox/mapbox-sdk/lib/classes/mapi-client'
import { Cron, CronExpression } from '@nestjs/schedule'
// import fs from 'fs/promises'
import { default as fsPromises } from 'fs'
const fs = fsPromises.promises
import { EOL } from 'os'
import { StringDecoder } from 'string_decoder'
import { from, zip, of } from 'rxjs'
import { groupBy, mergeMap, toArray } from 'rxjs/operators'
import gjv from 'geojson-validation'
import { ObjectId } from 'mongodb'
import { QueryBus } from '@nestjs/cqrs'
import axios from 'axios'
import { TilesetSourceIdTypes, IGeoProperties, RoamrTilesets, IGeoJSONFeature } from '../../common'
import { readWholeFile, editTilesetSourceFile, deleteTilesetSourcesFromFile, getTilesetSourceFilePath, writeTilesetSourceToFile } from '../../repositories/mapbox-tiling.file-handle'
// import {
//     IsPinCollectionEmptyQuery
// } from 'src/services/world/pin/queries/pin-queries'

interface IRecipeOptions {
    version: number,
    layers: {
        [key: string]: {
            source: string,
            minzoom: number,
            maxzoom: number,
        }
    }
}

@Injectable()
export class MapboxTilingService {
    // export class MapboxTilingService implements OnModuleInit {
    private readonly logger = new Logger(MapboxTilingService.name)
    private readonly decoder = new StringDecoder('utf8')
    private readonly stylesService: StylesService
    private readonly tilesetsService: TilesetsService
    private readonly mapbox_access_token: string
    private readonly mapbox_root_endpoint = `https://api.mapbox.com/`
    private readonly tilesetSourceURL = 'mapbox://tileset-source/glenolof/'
    // private tilesetSourceJobs: ICreateTilesetSourceJob[] = []
    // private tilesetSourceJobs: JobActions[] = []

    constructor(
        private readonly queryBus: QueryBus,
    ) {
        let config: SdkConfig = {
            accessToken: process.env.MAPBOX_SECRET_TOKEN,
            origin: this.mapbox_root_endpoint,
        }
        this.stylesService = mbxStyles(config)
        this.tilesetsService = mbxTilesets(config)
        this.mapbox_access_token = process.env.MAPBOX_SECRET_TOKEN
    }

    // async onModuleInit() {
    //     await this.createGeodataFolders()
    // }

    // private groupBy(list: any[], keyGetter: any) {
    //     const map = new Map()
    //     list.forEach((item) => {
    //         const key = keyGetter(item)
    //         const collection = map.get(key)
    //         if (!collection) {
    //             map.set(key, [item])
    //         } else {
    //             collection.push(item)
    //         }
    //     })
    //     return map
    // }

    // private async createGeodataFolders() {
    //     await createGeoDataFolder()
    //     await createGeoDataBackupFolder()
    // }

    /**
 * Tileset processing
 * Free: 0-5
 * 10m: 6-10
 * 1m: 11-13
 * 30cm: 14-16
 * 1cm: 17-22
 * @param tileset_type 
 */
    getRecipeOptions(
        tileset_type: TilesetSourceIdTypes
    ): IRecipeOptions {
        // set appropiate max zoom for the different tileset types
        switch (tileset_type) {
            case 'roamr-pins':
                // let options_pins = {
                //     version: 1,
                //     layers: {
                //         roamr_pins: {
                //             source: `${this.tilesetSourceURL}roamr-pins`,
                //             minzoom: 0,
                //             maxzoom: 5,
                //         }
                //     }
                // }
                let options_pins = {
                    version: 1,
                    layers: {
                        roamr_pins: {
                            source: `${this.tilesetSourceURL}roamr-pins-1`,
                            minzoom: 0,
                            maxzoom: 5,
                        }
                    }
                }
                return options_pins
            case 'roamr-trails':
                let options_trails = {
                    version: 1,
                    layers: {
                        roamr_trails: {
                            source: `${this.tilesetSourceURL}roamr-trails`,
                            minzoom: 0,
                            maxzoom: 5,
                            features: {
                                simplification: 6,
                            }
                        }
                    }
                }
                return options_trails
            case 'roamr-areas':
                let options_areas = {
                    version: 1,
                    layers: {
                        roamr_areas: {
                            source: `${this.tilesetSourceURL}roamr-areas`,
                            minzoom: 0,
                            maxzoom: 5,
                            features: {
                                simplification: 10,
                            }
                        }
                    }
                }
                return options_areas
            case 'roamr-zones':
                let options_zones = {
                    version: 1,
                    layers: {
                        roamr_zones: {
                            source: `${this.tilesetSourceURL}roamr-zones`,
                            minzoom: 0,
                            maxzoom: 5,
                            features: {
                                simplification: 10,
                            }
                        }
                    }
                }
                return options_zones
            case 'roamr-municipalities':
                let options_municipalities = {
                    version: 1,
                    layers: {
                        roamr_municipalities: {
                            source:
                                `${this.tilesetSourceURL}roamr-municipalities`,
                            minzoom: 0,
                            maxzoom: 5,
                            features: {
                                simplification: {
                                    distance: 1,
                                    outward_only: true,
                                }
                            },
                            tiles: {
                                union: [{
                                    simplification: {
                                        distance: 4,
                                        outward_only: true,
                                    }
                                }]
                            }
                        }
                    }
                }
                return options_municipalities
        }
    }

    //     /**
    // * Tileset processing
    // * Free: 0-5
    // * 10m: 6-10
    // * 1m: 11-13
    // * 30cm: 14-16
    // * 1cm: 17-22
    // * @param tileset_type 
    // */
    //     private getRecipeOptions(
    //         tileset_type: TilesetSourceIdTypes
    //     ): IRecipeOptions {
    //         // set appropiate max zoom for the different tileset types
    //         switch (tileset_type) {
    //             case 'roamr-pins':
    //                 let options_pins = {
    //                     version: 1,
    //                     recipe: {
    //                         layers: {
    //                             roamr_pins: {
    //                                 source: `${this.tilesetSourceURL}roamr-pins`,
    //                                 minzoom: 0,
    //                                 maxzoom: 5,
    //                             }
    //                         }
    //                     }
    //                 }
    //                 return options_pins
    //             case 'roamr-trails':
    //                 let options_trails = {
    //                     version: 1,
    //                     recipe: {
    //                         layers: {
    //                             roamr_trails: {
    //                                 source: `${this.tilesetSourceURL}roamr-trails`,
    //                                 minzoom: 0,
    //                                 maxzoom: 5,
    //                                 features: {
    //                                     simplification: 6,
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //                 return options_trails
    //             case 'roamr-areas':
    //                 let options_areas = {
    //                     version: 1,
    //                     recipe: {
    //                         layers: {
    //                             roamr_areas: {
    //                                 source: `${this.tilesetSourceURL}roamr-areas`,
    //                                 minzoom: 0,
    //                                 maxzoom: 5,
    //                                 features: {
    //                                     simplification: 10,
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //                 return options_areas
    //             case 'roamr-zones':
    //                 let options_zones = {
    //                     version: 1,
    //                     recipe: {
    //                         layers: {
    //                             roamr_zones: {
    //                                 source: `${this.tilesetSourceURL}roamr-zones`,
    //                                 minzoom: 0,
    //                                 maxzoom: 5,
    //                                 features: {
    //                                     simplification: 10,
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //                 return options_zones
    //             case 'roamr-municipalities':
    //                 let options_municipalities = {
    //                     version: 1,
    //                     recipe: {
    //                         layers: {
    //                             roamr_municipalities: {
    //                                 source:
    //                                     `${this.tilesetSourceURL}roamr-municipalities`,
    //                                 minzoom: 0,
    //                                 maxzoom: 5,
    //                                 features: {
    //                                     simplification: {
    //                                         distance: 1,
    //                                         outward_only: true,
    //                                     }
    //                                 },
    //                                 tiles: {
    //                                     union: [{
    //                                         simplification: {
    //                                             distance: 4,
    //                                             outward_only: true,
    //                                         }
    //                                     }]
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //                 return options_municipalities
    //         }
    //     }

    async getTilesetSourcesFromFile(
        tilesetSourceId: TilesetSourceIdTypes,
        readBackup: boolean = false,
    ) {
        let res = await readWholeFile(tilesetSourceId, readBackup)
        return res
    }


    async editTilesetSourcesFromFile<T extends IGeoProperties, K extends keyof IGeoProperties>(
        tilesetSourceId: TilesetSourceIdTypes,
        value_pairs: {
            id: string,
            pairs: {
                prop: K
                value: T[K]
            }[]
        }[],
    ) {
        // TODO: que edit sources without reading file then use this function to write to file instead

        let res =
            await editTilesetSourceFile(
                tilesetSourceId,
                value_pairs
            )
        return res

        // let res =
        //     await getTilesetSourceChanges(tilesetSourceId, value_pairs)
        // // await editTilesetSourceFile(tilesetSourceId, value_pairs)

        // res.forEach(async (f) => {
        //     await this.queTilesetSourceJob({
        //         tilesetSourceId,
        //         type: 'change', 
        //         feature: f,
        //     })
        // })
        // return res
    }

    async deleteTilesetSourcesFromFile(
        tilesetSourceId: TilesetSourceIdTypes,
        geo_ids: ObjectId[],
    ) {
        let res =
            await deleteTilesetSourcesFromFile(
                tilesetSourceId,
                geo_ids,
            )
        return res
    }

    /**
     * Limits
     * Tileset sources can be composed of up to 10 source files.
     * The maximum combined total size of all files 
     * that compose a tileset source is 50 GB.
     * If the total size of all the files that 
     * compose a tileset source is greater than 50 GB,
     * MTS will return a response that 
     * contains an error property with more details.
     * /tilesets/v1/sources/{username}/{id}
     * {id} The id for the tileset source to be created
     * https://stackoverflow.com/questions/16848972/how-to-emit-pipe-array-values-as-a-readable-stream-in-node-js
     * @param data 
     */
    async writeTilesetSourceToFile(
        tilesetSourceId: TilesetSourceIdTypes,
        // data: any | any[]
        data: IGeoJSONFeature | IGeoJSONFeature[]
    ) {
        try {
            this.logger.debug(this.writeTilesetSourceToFile.name)
            const path = getTilesetSourceFilePath(tilesetSourceId)
            const buffers = []

            if (Array.isArray(data)) {
                data.forEach(f => {
                    let buffer = Buffer.from(JSON.stringify(f))
                    buffers.push(buffer)
                })
            } else {
                let buffer = Buffer.from(JSON.stringify(data))
                buffers.push(buffer)
            }

            let file = await writeTilesetSourceToFile(path, buffers)

            // // @ts-ignore
            // let r1 = await this.tilesetsService.createTilesetSource({
            //     id: tilesetSourceId,
            //     // id: `glenolof.${tilesetSourceId}`,
            //     file: path,
            // }).send()

            // await this.createPublishTilesetJob(tilesetSourceId)

            // return r1

            // let res = await this.createTilesetSource(tilesetSourceId)
        } catch (error) {
            console.debug('error in createTilesetSource')
            console.debug(error)
            this.logger.error(error)
            return null
        }
    }

    async createTilesetSource(
        tilesetSourceId: TilesetSourceIdTypes,
    ) {
        const path = getTilesetSourceFilePath(tilesetSourceId)

        try {
            // @ts-ignore
            let r1 = await this.tilesetsService.createTilesetSource({
                // id: tilesetSourceId,
                id: `${tilesetSourceId}-1`,
                // id: `glenolof.${tilesetSourceId}`,
                file: path,
            }).send()

            // await this.createPublishTilesetJob(tilesetSourceId)
            return r1
        } catch (error) {
            console.log('Error in createTilesetSource')
            console.log(error.request._options.params)
            console.log(error)
            // console.log(error.request.error)
            // console.log(error.message)
            this.logger.error(error)
            return null
        }
    }

    async replaceTilesetSource(
        tilesetSourceId: TilesetSourceIdTypes,
    ) {
        this.logger.debug(this.replaceTilesetSource.name)

        const path = getTilesetSourceFilePath(tilesetSourceId)
        let url = `https://api.mapbox.com/tilesets/v1/sources/glenolof/${tilesetSourceId}?access_token=${this.mapbox_access_token}`
        try {
            let response =
                axios.put(url, {
                    file: path,
                }, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    }
                })

            // await this.createPublishTilesetJob(tilesetSourceId)
            return response
        } catch (error) {
            console.log('error in replaceTilesetSource')
            console.log(error)
            this.logger.error(error)
            return null
        }
    }

    async getTilesetSource(id: string) {
        try {
            let response =
                await this.tilesetsService
                    // @ts-ignore
                    .getTilesetSource({
                        id
                    })
                    .send()

            if (response) {
                return response.body
            }

            return null
        } catch (error) {
            this.logger.error(error)
            return null
        }
    }

    async listTilesetSources(): Promise<any[]> {
        try {
            let response =
                await this.tilesetsService
                    // @ts-ignore
                    .listTilesetSources({
                        ownerId: 'glenolof',
                    })
                    .send()

            if (response) {
                return response.body
            }
            return []
        } catch (error) {
            this.logger.error(error)
            return []
        }
    }

    /**
     * https://docs.mapbox.com/api/maps/#delete-a-tileset-source
     */
    async deleteTilesetSource(id: string) {

        this.logger.debug(this.deleteTilesetSource.name)

        try {
            this.logger.debug('getTilesetSource')
            const sources = await this.listTilesetSources()

            if (sources.every(s => !s.id.includes(id))) {
                this.logger.debug('tileset source not existing, exiting delete tileset source')
                return null
            }
        } catch (error) {
            console.log('Error in getting tileset source')
            console.log(error)
            this.logger.error(error)
            return null
        }

        try {

            this.logger.debug('Attempt to delete tileset source')

            let response =
                await this.tilesetsService
                    // @ts-ignore
                    .deleteTilesetSource({
                        // id
                        id: `${id}-1`
                    })
                    .send()

            if (response) {
                this.logger.debug('Succeeded deleting tileset source')
                console.log(response.body)
                return response.body
            }

            this.logger.debug('No response from delete tileset source')

            return null
        } catch (error) {
            console.log('Error in delete tileset source')
            console.log(error)
            this.logger.error(error)
            return null
        }
    }

    async checkIfTilesetExistsOrCreate(
        tilesetSourceId: TilesetSourceIdTypes
    ) {
        this.logger.debug('Checking if tileset exists')
        let tilesets = await this.listTilesets()

        if (tilesets
            .map((m: any) => m.id)
            .every((s: any) => `glenolof.${tilesetSourceId}` !== s)) {
            await this.createTileset(tilesetSourceId, tilesetSourceId)
            await this.updateRecipeByTileset(tilesetSourceId)
        }
    }

    async listTilesets() {
        try {
            let response =
                await this.tilesetsService
                    .listTilesets({
                        ownerId: 'glenolof'
                    })
                    .send()

            if (response) {
                return response.body
            }

            return []
        } catch (error) {
            this.logger.error(error)
            return []
        }
    }

    /**
     * If a tileset is already created, 
     * MTS will return HTTP 400 (Bad request)
     * @param tilesetId 
     * @param tilesetSourceName 
     */
    async createTileset(
        tilesetId: RoamrTilesets,
        tilesetSourceName: TilesetSourceIdTypes,
    ) {
        this.logger.debug(this.createTileset.name)
        let recipe = this.getRecipeOptions(tilesetSourceName)
        console.log('recipe options:')
        console.log(recipe)

        try {
            // @ts-ignore
            let res = await this.tilesetsService.createTileset({
                tilesetId: `glenolof.${tilesetId}`,
                recipe,
                name: tilesetId,
            }).send()

            return res
        } catch (error) {
            console.log('error in createTileset')
            console.log(error)
            this.logger.error(error)
            return null
        }
    }

    // TODO: Use updateTilesetRecipe instead and remove the tileset source
    async deleteTileset(
        tilesetId: RoamrTilesets,
    ) {
        try {
            // @ts-ignore
            let res = await this.tilesetsService.deleteTileset({
                tilesetId: `glenolof.${tilesetId}`,
            })
                .send()

            return res
        } catch (error) {
            this.logger.error(error)
            return null
        }
    }

    async publishTileset(tilesetId: RoamrTilesets) {
        // await this.deleteTilesetSource(tilesetId)
        // await this.createTilesetSource(tilesetId)
        await this.checkIfTilesetExistsOrCreate(tilesetId)

        try {
            // @ts-ignore
            let response = await this.tilesetsService.publishTileset({
                tilesetId: `glenolof.${tilesetId}`
            }).send()

            this.logger.debug('tileset has been published')

            return response
        } catch (error) {
            console.debug('error in publish')
            console.debug(error)
            this.logger.error(error)
            return null
        }
    }

    /**
     * Use this function to update tileset recipe 
     * with e.g. new tilesetSources
     * @param tilesetId 
     */
    async updateTilesetRecipe(
        tilesetId: RoamrTilesets
    ) {
        // TODO: First get old recipe and append, or
        // modify it from the ground up
    }

    // private async checkMongoDBCollection(
    //     tilesetSourceId: TilesetSourceIdTypes
    // ): Promise<boolean> {
    //     switch (tilesetSourceId) {
    //         case 'roamr-pins':
    //             // return await this.queryBus
    //             //     .execute(new IsPinCollectionEmptyQuery())
    //             return await isPinCollectionEmpty()
    //         default:
    //             break
    //     }
    // }

    async getRecipe(tilesetId: RoamrTilesets) {
        try {
            console.log(this.getRecipe.name)
            console.log(tilesetId)

            let res =
                // @ts-ignore
                await this.tilesetsService.getRecipe({
                    tilesetId: `glenolof.${tilesetId}`
                }).send()

            if (res) {
                console.log('res body')
                console.log(res.body.recipe.layers.roamr_municipalities)
                return res.body
            } else {
                return null
            }
        } catch (error) {
            this.logger.error(error)
            return null
        }
    }

    async updateRecipe(
        tilesetId: RoamrTilesets,
        options: IRecipeOptions,
    ) {
        try {
            let { valid } = await this.validateRecipe(options)
            console.log(this.updateRecipe.name)

            if (valid === null ||
                valid === undefined ||
                !valid) {
                console.log('Recipe is not valid')
                return null
            }

            let res =
                // @ts-ignore
                await this.tilesetsService.updateRecipe({
                    tilesetId: `glenolof.${tilesetId}`,
                    recipe: options
                }).send()

            console.log('after updateRecipe')

            return res
        } catch (error) {
            console.log(error)
            this.logger.error(error)
            return null
        }
    }

    async updateRecipeByTileset(
        tilesetId: RoamrTilesets,
    ) {
        try {
            const options = this.getRecipeOptions(tilesetId)
            let { valid } = await this.validateRecipe(options)
            console.log('validate')

            if (valid === null ||
                valid === undefined ||
                !valid) {
                console.log('Recipe is not valid')
                return null
            }

            let res = await this.updateRecipe(tilesetId, options)
            // let res =
            //     // @ts-ignore
            //     await this.tilesetsService.updateRecipe({
            //         tilesetId: `glenolof.${tilesetId}`,
            //         recipe: options
            //     }).send()

            console.log('after updateRecipe2')

            return res.statusCode === 204
        } catch (error) {
            this.logger.error(error)
            return null
        }
    }

    async validateRecipe(
        options: IRecipeOptions,
    ) {
        try {
            console.log('validateRecipe')
            let res =
                // @ts-ignore
                await this.tilesetsService.validateRecipe({
                    recipe: options
                }).send()

            console.log('validateRecipeafter')

            if (res) {
                return res.body
            } else {
                return null
            }
        } catch (error) {
            this.logger.error(error)
            return null
        }
    }

    async validateRecipeByTileset(tilesetSourceId: TilesetSourceIdTypes) {
        const options = this.getRecipeOptions(tilesetSourceId)
        let res = await this.validateRecipe(options)
        return res
    }
}
