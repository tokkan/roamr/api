import { InjectQueue } from '@nestjs/bull'
import { Injectable, Logger, NotImplementedException } from '@nestjs/common'
import { Job, Queue } from 'bull'
import { ObjectId } from 'mongodb'
import { buildQRCode } from 'src/common/utility/qr-code'
import { DatabaseModels, Firebase } from 'src/database/firebase/database'
import { LanguageTypes, CountryTypes } from 'src/models/common/common'
import { IGeoJSONFeature, convertCoordinatesToGeoPoint, TilesetSourceIdTypes, JobActions, IAddAction } from '../../common'
import { IPinEntity, PinTypes, IPinImage, IPinEntityBase } from '../../models'
import {
    isPinCollectionEmpty, getPinByProp, getAllPins, getAllPinsForUser, createPinMongo, batchCreatePinsMongo, updatePinByValuePairs, deletePinsById, updatePinByProp
} from '../../repositories/pin.repository'
import { RedisJobNames, RedisQueues } from '../../world.module'

@Injectable()
export class PinService {
    private readonly logger = new Logger(PinService.name)

    constructor(
        // private readonly tilingService: TilingService,
        // private readonly qrCodeService: QrCodeService,
        @InjectQueue(RedisQueues.PINS) private pinQueue: Queue<JobActions>
    ) { }

    async parse<T>(data: any): Promise<T | null> {
        try {
            return await JSON.parse(JSON.stringify(data))
        } catch (error) {
            this.logger.error(error)
            return null
        }
    }

    async isCollectionEmpty() {
        let isEmpty = await isPinCollectionEmpty()
        return isEmpty
    }

    // async getPinById(entity_id: string) {
    //     return await getPinById(entity_id)
    // }

    async getPinById(id: ObjectId) {
        let res = await getPinByProp('_id', id)
        return res
    }

    async getPinByBusinessId(id: ObjectId) {
        let res = await getPinByProp('business_id', id)
        return res
    }

    async getAllPins(limit: number, offset: number) {
        let res = await getAllPins(limit, offset)
        return res
    }

    async getAllPinsForUser(user_id: ObjectId, limit: number, offset: number) {
        let res = await getAllPinsForUser(user_id, limit, offset)
        return res
    }

    // async createPin(entity: IPinFirestoreEntity) {
    //     entity.is_active = false
    //     let response = await createPin(entity)
    //     return response
    // }

    async createPin(entity: IPinEntity) {
        let id = new ObjectId()
        let qr_code = buildQRCode(id.toHexString(), 'pin')
        entity._id = id
        entity.qr_code = qr_code
        entity.is_active = false
        let response = await createPinMongo(entity)
        return response
    }

    // async createPinFromBusiness(payload: ICreatePinFromBusinessPayload) {
    async createPinFromBusiness(payload: {
        pin_mongo_id: string,
        // pin_fire_id: string,
        municipality_mongo_id: string,
        // municipality_fire_id: string,
        longitude: number,
        latitude: number,
        pin_type: PinTypes,
        sub_pin_types: PinTypes[],
        title: string,
        sub_title: string,
        description: string[],
        image?: IPinImage,
        language: LanguageTypes,
        country: CountryTypes,
        business_id?: ObjectId,
    }) {
        const {
            base_entity,
            // fire_entity,
            mongo_entity
        } = await this.buildPinFromProps(payload)

        let res = await createPinMongo(mongo_entity)
        return res
    }

    async buildPinFromProps(payload: {
        pin_mongo_id: string,
        // pin_fire_id: string,
        municipality_mongo_id: string,
        // municipality_fire_id: string,
        longitude: number,
        latitude: number,
        pin_type: PinTypes,
        sub_pin_types: PinTypes[],
        title: string,
        sub_title: string,
        description: string[],
        image?: IPinImage,
        language: LanguageTypes,
        country: CountryTypes,
        business_id?: ObjectId,
        // qr_code?: string,
    }) {
        let business_id = null
        let is_business = false
        let qr_code = buildQRCode(payload.pin_mongo_id, 'pin')
        let image: IPinImage = {
            source: '',
            source_type: 'url',
            alt: '',
        }

        if (payload.business_id !== undefined &&
            payload.business_id !== null &&
            payload.business_id.toHexString().length > 0) {
            business_id = new ObjectId(payload.business_id)
            is_business = true
        }

        if (payload.image !== undefined) {
            image = payload.image
        }

        const base_entity: IPinEntityBase = {
            geo_ids: [],
            rating: {
                clicks: 0,
                following: 0,
                is_sponsored: false,
                likes: 0,
            },
            is_active: false,
            pin_type: payload.pin_type,
            sub_pin_types: payload.sub_pin_types,
            geo_type: 'pin',
            business_id,
            is_business,
            // qr_code: payload.qr_code,
            qr_code,
            info: [{
                title: payload.title,
                sub_title: payload.sub_title,
                description: payload.description,
                image,
                index: 0,
                lang: payload.language,
            }],
            geometry: {
                type: 'Point',
                coordinates: [payload.longitude, payload.latitude],
            },
            properties: {
                title: payload.title,
                sub_title: payload.sub_title,
                // description: payload.description,
                geo_type: "pin",
                country: payload.country,
                is_active: false,
                _id: payload.pin_mongo_id,
                // fire_id: payload.pin_fire_id,
                business_id,
                pin_type: payload.pin_type,
                sub_pin_types: payload.sub_pin_types,
            }
        }

        // const fire_entity: IPinFirestoreEntity = {
        //     ...base_entity,
        //     _id: payload.pin_mongo_id,
        //     fire_id: payload.pin_fire_id,
        //     municipality_fire_id: payload.municipality_fire_id,
        //     municipality_mongo_id: payload.municipality_mongo_id,
        // }

        const mongo_entity: IPinEntity = {
            ...base_entity,
            _id: new ObjectId(payload.pin_mongo_id),
            // fire_id: payload.pin_fire_id,
            // municipality_fire_id: payload.municipality_fire_id,
            municipality_mongo_id: new ObjectId(payload.municipality_mongo_id),
        }

        return {
            base_entity,
            // fire_entity,
            mongo_entity,
        }
    }

    async batchCreateMongoPins(features: IPinEntity[]) {
        let res = await batchCreatePinsMongo(features)
        return res
    }

    // async batchCreateFirebasePins(features: IPinFirestoreEntity[]) {
    //     let res = await batchCreatePinsFirestore(features)
    //     return res
    // }

    // updatePinEntityByProp<T extends IPinFirestoreEntity, K extends keyof T>(
    //     entity: T,
    //     prop: K,
    //     value: T[K]
    // ) {
    //     entity[prop] = value
    //     return entity
    // }

    // async updatePinByProp<T extends IPinFirestoreEntity, K extends keyof T>(
    //     entity: T,
    //     prop: K,
    //     value: T[K]
    // ) {
    //     // return await
    // }

    /**
 * The props variable uses a reduce function to create a object with
 * the param prop as a property and value as value, like:
 * {
 *  [prop1]: value1,
 *  [prop2]: value2,
 *  ...and so on with all props in the pairs array
 * }
 * @param id 
 * @param pairs 
 */
    async updatePinByValuePairs<T extends IPinEntity, K extends keyof T>(
        id: ObjectId,
        pairs: {
            prop: K,
            value: T[K]
        }[],
    ) {
        let res = await updatePinByValuePairs(id, pairs)
        return res
    }

    async deletePin() {
        throw new NotImplementedException()
    }

    async deletePins(ids: ObjectId[]) {
        let res = await deletePinsById(ids)
        return res
    }

    // async activatePinFirestore(id: string) {
    //     // validation to check if all business requirements are met
    //     // if validation passed, set active state to true
    //     let document = await this.getPinById(id)
    //     let entity = await this.parse<IPinFirestoreEntity>(document)

    //     if (entity !== null) {
    //         entity = this.updatePinEntityByProp(entity, 'is_active', true)
    //         let res = await updatePinByValuePairs(entity.fire_id, [{
    //             prop: 'is_active',
    //             value: true
    //         }])
    //         return res
    //     } else {
    //         this.logger.error(`Pin entity couldn't be found`)
    //         return null
    //     }
    // }

    async activatePinMongo(id: string) {
        return await updatePinByProp(new ObjectId(id), 'is_active', true);
    }

    async sanitizeData(
        features: any[],
        unprocessed_features: any[] = [],
        country: CountryTypes = 'sweden',
    ) {
        try {
            const geoFeatures: IGeoJSONFeature[] = []
            // const firestore_entities: IPinFirestoreEntity[] = []
            const mongo_entities: IPinEntity[] = []
            let c = Firebase.collection(DatabaseModels.SE_PINS)

            for (let i = 0; i < features.length; i++) {
                let ref = c.doc()
                let mongo_id = new ObjectId()

                if (unprocessed_features.length > 0) {
                    unprocessed_features[i]._id = mongo_id.toHexString()
                    unprocessed_features[i].properties._id =
                        mongo_id.toHexString()
                    unprocessed_features[i].fire_id = ref.id
                    unprocessed_features[i].properties.fire_id = ref.id
                    unprocessed_features[i].geo_type = 'pin'
                    unprocessed_features[i].properties.geo_type = 'pin'
                    unprocessed_features[i].country = country
                    unprocessed_features[i].properties.country = country
                }

                features[i]._id = mongo_id.toHexString()
                features[i].properties._id = mongo_id.toHexString()
                features[i].fire_id = ref.id
                features[i].properties.fire_id = ref.id
                features[i].geo_type = 'pin'
                features[i].properties.geo_type = 'pin'
                features[i].country = country
                features[i].properties.country = country
                const m = features[i]
                let geopoint = convertCoordinatesToGeoPoint(m.geometry)

                geoFeatures.push({
                    type: 'Feature',
                    geometry: features[i].geometry,
                    properties: features[i].properties,
                })

                // firestore_entities.push({
                //     fire_id: ref.id,
                //     ...m.properties,
                //     // _id: m.properties._id.toHexString(),
                //     _id: mongo_id.toHexString(),
                //     location: geopoint,
                //     geo_type: 'pin',
                // })

                mongo_entities.push({
                    // type: m.type,
                    // geometry: m.geometry,
                    // properties: m.properties,
                    ...m,
                    _id: mongo_id,
                    fire_id: ref.id,
                    geo_type: 'pin',
                })
            }

            return {
                geo_features: geoFeatures,
                unprocessed_features: unprocessed_features,
                // firestore_entities,
                mongo_entities
            }
        } catch (error) {
            console.log('error in sanitize')
            console.log(error)
            this.logger.error(error)
            return {
                sanitized_features: null,
                // firestore_entities: null,
                mongo_entities: null
            }
        }
    }

    async uploadParsedData({
        tilesetSourceId,
        // unprocessed,
        simplified,
    }: {
        tilesetSourceId: TilesetSourceIdTypes
        // unprocessed: any[]
        simplified: IGeoJSONFeature[]
    }
    ) {
        // let geoFeatures: IGeoJsonFeature[] = []
        let geoFeatures: IGeoJSONFeature[] = []
        // let unprocessedFeatures: IGeoJsonFeature[] = []
        // let f_entities: IPinFirestoreEntity[] = []
        let m_entities: IPinEntity[] = []

        const {
            geo_features,
            unprocessed_features,
            // firestore_entities,
            mongo_entities
        } = await this.sanitizeData(simplified, [])
        geoFeatures = geo_features
        // unprocessedFeatures = unprocessed_features
        // f_entities = firestore_entities
        m_entities = mongo_entities
        // let res1 =
        //     await this.batchCreateFirebasePins(f_entities)
        let res2 =
            await this.batchCreateMongoPins(m_entities)

        // await this.tilingService
        //     .updateRecipeByTileset(tilesetSourceId)

        // TODO: Add to CQRS to avoid circular dependencies?
        // geoFeatures.forEach(async (f) => {
        //     await this.tilingService.queTilesetSourceJob({
        //         tilesetSourceId,
        //         type: 'add',
        //         feature: f,
        //     })
        // })

        let jobs: {
            name?: string
            data: JobActions
            opts?: any
        }[] = geoFeatures.map(f => {
            let data: JobActions = {
                tilesetSourceId,
                type: 'add',
                feature: f,
            }
            return {
                name: RedisJobNames.PIN_ADD,
                data,
                // opts: {
                // }
            }
        })
        this.pinQueue.addBulk(jobs)
    }
}