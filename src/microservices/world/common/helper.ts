import { Logger, UnprocessableEntityException } from '@nestjs/common'
import {
    simplify, point, polygon, lineString, truncate, LineString, Polygon, Feature
} from '@turf/turf'
import gjv from 'geojson-validation'
import { CoordsActions, IGeoFile, IGeoJSONFeature, TilesetSourceIdTypes } from '.'
import { StringDecoder } from 'string_decoder'

const logger = new Logger(`Geo helper utility`)
const decoder = new StringDecoder('utf8')

export function simplifyGeoJSON(
    tilesetSourceId: TilesetSourceIdTypes,
    // data: Exclude<CoordsActions, IPointAction>,
    data: CoordsActions,
): CoordsActions {
    try {
        let options = {
            tolerance: 0.001,
            highQuality: false,
            mutate: false,
        }

        switch (data.type) {
            case 'Point':
                const point = truncatePoint(data.coordinates)
                return {
                    type: data.type,
                    coordinates: point,
                }

            case 'LineString':
                options.tolerance = 0.000001
                options.highQuality = true
                const linestring = lineString(data.coordinates)
                const linestring_truncated =
                    truncateGeoJSON(linestring)
                let s1 = simplify(linestring_truncated, options)
                return s1

            case 'Polygon':
                if (tilesetSourceId === 'roamr-municipalities') {
                    options.tolerance = 0.0001
                    options.highQuality = true
                } else if (tilesetSourceId === 'roamr-trails') {
                    options.tolerance = 0.00001
                    options.highQuality = true
                }
                else {
                    options.tolerance = 0.001
                }

                if (tilesetSourceId === 'roamr-municipalities') {
                    const poly = polygon(data.coordinates)
                    let s2 = simplify(poly, options)
                    return {
                        type: s2.geometry.type,
                        coordinates: s2.geometry.coordinates,
                    }
                } else {
                    const poly = polygon(data.coordinates)
                    const poly_truncated =
                        truncateGeoJSON(poly)
                    let s2 = simplify(poly_truncated, options)
                    return s2
                }
            default:
                return null
        }
    } catch (error) {
        logger.error(error)
        return null
    }
}

/**
 * Recommended precision unless it's survey grade GPS 
 * (where 10 decimal places should suffice)
 * is 6 decimal places, so that's the default 
 * @param data 
 * @param precision 
 */
export function truncatePoint(
    data: number[],
    precision: number = 6,
) {
    let options = {
        precision,
        coordinates: 3,
        mutate: false,
    }
    const geo_point = point(data)
    let truncated = truncate(geo_point, options)
    return truncated.geometry.coordinates
}

export function truncateGeoJSON(
    geojson: Feature<LineString> | Feature < Polygon >,
    precision: number = 6,
) {
    let options = {
        precision,
        coordinates: 3,
        mutate: false,
    }
        let truncated = truncate(geojson, options)

        if(truncated.geometry.type === 'LineString') {
    let res = truncated as Feature<LineString>
    // return res
    return turfToGeoJSON(res)
} else if (truncated.geometry.type === 'Polygon') {
    let res = truncated as Feature<Polygon>
    return turfToGeoJSON(res)
}
    }

export function turfToGeoJSON(
    geojson: Feature<LineString> | Feature < Polygon >,
): CoordsActions {
    switch(geojson.geometry.type) {
            case 'LineString':
    return {
        type: geojson.geometry.type,
        coordinates: geojson.geometry.coordinates
    }
            case 'Polygon':
    return {
        type: geojson.geometry.type,
        coordinates: geojson.geometry.coordinates
    }
            default:
    return null
}
    }

export function validateGeoJSON(
    tilesetSourceId: TilesetSourceIdTypes,
    data: IGeoJSONFeature,
) {
    switch (tilesetSourceId) {
        case 'roamr-pins':
            let isPoint = gjv.isPoint(data.geometry)
            return isPoint
        case 'roamr-trails':
            let isTrail = gjv.isLineString(data.geometry)
            return isTrail
        case 'roamr-areas':
        case 'roamr-zones':
        case 'roamr-municipalities':
            let isPolygon = gjv.isPolygon(data.geometry)
            return isPolygon
        default:
            return false
    }
}

export async function parseFileToGeoJSON(
    tilesetSourceId: TilesetSourceIdTypes,
    file: IGeoFile,
) {
    const json = decoder.write(file.buffer)
    let data = await JSON.parse(json)

    console.log(parseFileToGeoJSON.name)

    if (Array.isArray(data)) {
        data.forEach(f => {
            let valid = validateGeoJSON(tilesetSourceId, f)
            if (!valid) {
                throw new UnprocessableEntityException()
            }
        })

        let res = simplifyGeoJSONFeatures(tilesetSourceId, data)
        return {
            unprocessed: data,
            simplified: res,
        }
    } else {
        // let valid = validateGeoJSON(tilesetSourceId, data.features)
        let valid = validateGeoJSON(tilesetSourceId, data)
        if (!valid) {
            throw new UnprocessableEntityException()
        }

        let res = simplifyGeoJSONFeatures(tilesetSourceId, [data])
        return {
            unprocessed: data.features as IGeoJSONFeature[],
            simplified: res,
        }
    }
}

export function simplifyGeoJSONFeatures(
    tilesetSourceId: TilesetSourceIdTypes,
    geoFeatures: IGeoJSONFeature[]
) {
    let res: IGeoJSONFeature[] = []
    geoFeatures.forEach(f => {
        // if (f.geometry.type !== 'Point') {
        let simplified = simplifyGeoJSON(
            tilesetSourceId,
            f.geometry
        )
        res.push({
            // ...f,
            // ...simplified
            type: f.type,
            properties: f.properties,
            geometry: simplified
        })
        // }
    })
    return res
}