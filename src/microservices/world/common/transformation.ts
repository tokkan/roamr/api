import { firestore } from 'firebase-admin'
import { IGeoProperties } from '.'

export interface IGeoFile {
    fieldname: string
    originalname: string
    encoding: string
    mimetype: MimeType
    buffer: Buffer
    size: number
}

export interface IGeoJSONFeatureCollection {
    type: string
    features: IGeoJSONFeature[]
}

export interface IGeoJSONFeature {
    type: string
    properties: IGeoProperties
    geometry: CoordsActions
}

// export interface IGeoValuePair {
//     id: string
//     pairs: {
//         property: string
//         value: string
//     }[]
// }

export interface IGeoValuePair {
    id: string
    pairs: {
        prop: string
        value: any
    }[]
}

export interface IAction<T, P> {
    type: T
    coordinates: P
}

export type CoordsActions =
    IPointAction |
    IPathAction |
    IPolyAction

export interface IPointAction extends IAction<
    'Point',
    number[]
    > { }

export interface IPathAction extends IAction<
    'LineString',
    number[][]
    > { }

export interface IPolyAction extends IAction<
    'Polygon',
    number[][][]
    > { }

function switchPathCoordinates(coords: number[][]): number[][] {
    let res = coords.map(m => m.reverse())
    return res
}

function switchPolyCoordinates(coords: number[][][]): number[][][] {
    let array = coords[0]
    let res = array.map(m => m.reverse())
    return [res]
}

export function getCoordinates(
    action: CoordsActions
) {
    switch (action.type) {
        case 'Point':
            return action.coordinates

        case 'LineString':
            let path = switchPathCoordinates(action.coordinates)
            return path

        case 'Polygon':
            let poly = switchPolyCoordinates(action.coordinates)
            return poly
    }
}

export function convertCoordinatesToGeoPoint(action:
    IPointAction |
    IPathAction |
    IPolyAction
) {
    switch (action.type) {
        case 'Point':
            let point = action.coordinates
            return [new firestore.GeoPoint(point[1], point[0])]
        // return new firestore.GeoPoint(point[1], point[0])

        case 'LineString':
            let path = action.coordinates
            let geopath: firestore.GeoPoint[] =
                path.map(m =>
                    new firestore.GeoPoint(
                        m[1],
                        m[0],
                    ))
            return geopath

        case 'Polygon':
            let poly = action.coordinates
            let geopoly: firestore.GeoPoint[] =
                poly[0].map(m =>
                    new firestore.GeoPoint(
                        m[1],
                        m[0],
                    ))
            return geopoly
    }
}

export function convertPathCoordinatesToGeoPoint(coords: number[][]) {
    let flipped = coords.map(m => m.reverse())
    let res: firestore.GeoPoint[] =
        flipped.map(m => new firestore.GeoPoint(m[0], m[1]))

    // let res: { latitude: number, longitude: number }[] =
    // flipped.map(m => {
    //     return {
    //         latitude: m[0],
    //         longitude: m[1],
    //     }
    // })

    return res
}

export function convertPolyCoordinatesToGeoPoint(coords: number[][][]) {
    let flipped = coords[0].map(m => m.reverse())
    let res: firestore.GeoPoint[] = flipped.map(m =>
        new firestore.GeoPoint(m[0], m[1]))
    // latitude: m[0],
    // longitude: m[1],

    // let res: IPinCoordinates[] = flipped.map(m => {
    //     return {
    //         latitude: m[0],
    //         longitude: m[1],
    //     }
    // })

    return res
}