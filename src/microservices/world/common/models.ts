import { ObjectId } from "mongodb"
import { CountryTypes } from "src/models/common/common"
import { IGeoJSONFeature, IGeoValuePair } from "."

// export enum TilesetSourceIds {
//     ROAMR_PINS = 'roamr-pins',
//     ROAMR_trailS = 'roamr-trails',
//     ROAMR_AREAS = 'roamr-areas',
//     ROAMR_MUNICIPALITIES = 'roamr-municipalities',
// }

export type GeoTypes =
    'pin' |
    'trail' |
    'area' |
    'zone' |
    'municipality'

// export type CountryTypes =
//     'sweden' |
//     'norway' |
//     'denmark' |
//     'finland' |
//     'iceland' |
//     'netherlands' |
//     'germany' |
//     'usa'

export type RoamrTilesets =
    'roamr-pins' |
    'roamr-trails' |
    'roamr-areas' |
    'roamr-zones' |
    'roamr-municipalities'

export type TilesetSourceIdTypes =
    'roamr-pins' |
    'roamr-trails' |
    'roamr-areas' |
    'roamr-zones' |
    'roamr-municipalities'

export interface IGeoProperties {
    [key: string]: any
    title: string
    // description: string[]
    is_active: boolean
    geo_type: GeoTypes
    country: CountryTypes
}

export interface IGeoBaseEntity {
    business_id: ObjectId
    is_business: boolean
    is_active: boolean
    qr_code: string
    geo_type: GeoTypes
}

export type TilesetSourceJobTypes =
    'add' |
    'change'

export interface IJobAction<T, P> {
    tilesetSourceId: TilesetSourceIdTypes
    type: T
    feature: P
}

export type JobActions =
    IAddAction |
    IChangeAction |
    IDeleteAction

export interface IAddAction extends IJobAction<
    'add',
    IGeoJSONFeature
    > { }

export interface IChangeAction extends IJobAction<
    'change',
    IGeoValuePair
    > { }

export interface IDeleteAction extends IJobAction<
    'delete',
    ObjectId
    > { }

export interface ICreateTilesetSourceJob {
    // tilesetSourceId: TilesetSourceIdTypes
    // // feature: IGeoJSONFeature
    // // type: TilesetSourceJobTypes
    // feature: JobActions
    job: JobActions
}