import { Logger } from "@nestjs/common"
import gjv from 'geojson-validation'
import { default as fsPromises } from 'fs'
import fs from 'fs'
const fsp = fsPromises.promises
import readline from 'readline'
import { EOL } from "os"
import { ObjectId } from "mongodb"
import {
    IGeoJSONFeature, TilesetSourceIdTypes, IGeoProperties
} from "../common"

const logger = new Logger('MapboxTilingFileHandle')

async function parse<T extends IGeoJSONFeature>(data: string): Promise<T> {
    return await JSON.parse(data)
}

export async function createGeoDataFolder() {
    try {
        logger.log('Checking if geodata folder for tilesetSources exists')
        await fsp.mkdir('geodata')
        logger.log('Created geodata folder')
    } catch (error) {
        logger.log('Geodata folder already exists')
    }
}

export async function createGeoDataBackupFolder() {
    try {
        logger.log('Checking if geodata/backup folder for tilesetSources exists')
        await fsp.mkdir('geodata/backup')
        logger.log('Created geodata/backup folder')
    } catch (error) {
        logger.log('Geodata/backup folder already exists')
    }
}

export async function backupTilesetSourcesFiles() {
    let pin_file = getFullFilePath('roamr-pins')
    let pin_file_backup = getFullFilePath('roamr-pins', true)
    let trails_file = getFullFilePath('roamr-trails')
    let trails_file_backup = getFullFilePath('roamr-trails', true)
    let areas_file = getFullFilePath('roamr-areas')
    let areas_file_backup = getFullFilePath('roamr-areas', true)
    let zones_file = getFullFilePath('roamr-zones')
    let zones_file_backup = getFullFilePath('roamr-zones', true)
    let municipalities_file = getFullFilePath('roamr-municipalities')
    let municipalities_file_backup = getFullFilePath('roamr-municipalities', true)

    await copyFile(pin_file, pin_file_backup)
    await copyFile(trails_file, trails_file_backup)
    await copyFile(areas_file, areas_file_backup)
    await copyFile(zones_file, zones_file_backup)
    await copyFile(municipalities_file, municipalities_file_backup)
}

export function getTilesetSourceFilePath(tilesetSourceId: TilesetSourceIdTypes) {
    switch (tilesetSourceId) {
        case 'roamr-pins':
            return 'geodata/tileset-source-pins.geojson.ld'

        case 'roamr-trails':
            return 'geodata/tileset-source-trails.geojson.ld'

        case 'roamr-areas':
            return 'geodata/tileset-source-areas.geojson.ld'

        case 'roamr-zones':
            return 'geodata/tileset-source-zones.geojson.ld'

        case 'roamr-municipalities':
            return 'geodata/tileset-source-municipalities.geojson.ld'

        default:
            break
    }
}

export function getFilePath(
    tilesetSourceId: TilesetSourceIdTypes,
    readBackup: boolean = false,
) {
    let path = 'geodata/'
    let file_name = ''

    if (readBackup) {
        path = 'geodata/backup/'
    }

    switch (tilesetSourceId) {
        case 'roamr-pins':
            file_name = 'tileset-source-pins'
            break
        case 'roamr-trails':
            file_name = 'tileset-source-trails'
            break
        case 'roamr-areas':
            file_name = 'tileset-source-areas'
            break
        case 'roamr-zones':
            file_name = 'tileset-source-zones'
            break
        case 'roamr-municipalities':
            file_name = 'tileset-source-municipalities'
            break
        default:
            file_name = ''
            break
    }

    if (file_name.length <= 0) {
        return {
            file_name: null,
            ext: null,
        }
    } else {
        return {
            file_name: `${path}${file_name}`,
            ext: '.geojson.ld',
        }
    }
}

function getFullFilePath(
    tilesetSourceId: TilesetSourceIdTypes,
    readBackup: boolean = false,
) {
    const { file_name, ext } = getFilePath(tilesetSourceId, readBackup)
    return `${file_name}${ext}`
}

async function doesFileExist(path: string) {
    try {
        let res = await fsp.stat(path)
    } catch (error) {
        if (error === 'ENOENT') {

        }
    }
}

async function copyFile(
    src: string,
    dest: string,
) {
    try {
        await fsp.copyFile(src, dest)
    } catch (error) {
        logger.error(error)
    }
}

export async function copyTempFile(
    tilesetSourceId: TilesetSourceIdTypes,
) {
    const { file_name, ext } = getFilePath(tilesetSourceId)
    const temp = `${file_name}-1${ext}`
    const original = `${file_name}${ext}`

    try {
        console.log('copy file')

        await fsp.copyFile(
            temp,
            original,
        )

        console.log('file copied')
    } catch (error) {
        console.log('copy error')
        console.log(error)
        logger.error(error)
    }
}

export async function readFileToBuffer(
    tilesetSourceId: TilesetSourceIdTypes,
    readBackup: boolean = false,
) {
    try {
        let file_name = getFullFilePath(tilesetSourceId, readBackup)
        let file = await fsp.readFile(file_name)
        return file
    } catch (error) {
        logger.error(error)
        return null
    }
}

function openReadStream(
    tilesetSourceId: TilesetSourceIdTypes,
    readBackup: boolean = false,
) {
    try {
        let file_name = getFullFilePath(tilesetSourceId, readBackup)
        let stream = fs.createReadStream(file_name)
        let read_interface = readline.createInterface({
            input: stream,
            output: process.stdout,
            terminal: false,
            crlfDelay: Infinity,
        })

        return read_interface
    } catch (error) {
        logger.error(error)
        return null
    }
}

function openReadWriteStream(
    tilesetSourceId: TilesetSourceIdTypes,
    readBackup: boolean = false,
) {
    try {
        const { file_name, ext } = getFilePath(tilesetSourceId, readBackup)
        let read_stream = fs.createReadStream(`${file_name}${ext}`, {
            // autoClose: true,
            // flags: 'a+',
            // flags: 'w+',
            // mode: 0o777,
        })
        let write_stream = fs.createWriteStream(`${file_name}-1${ext}`, {
            // autoClose: true,
            // flags: 'w+',
            // flags: 'a+',
            // mode: fs.constants.W_OK,
            // mode: fs.constants.O_RDWR,
            // mode: 0o777,
        })
        // let write_stream = fs.createWriteStream(file_name)
        let read_interface = readline.createInterface({
            input: read_stream,
            output: write_stream,
            terminal: false,
            crlfDelay: Infinity,
            removeHistoryDuplicates: true,
        })

        return {
            read_interface,
            read_stream,
            write_stream,
        }
    } catch (error) {
        logger.error(error)
        return {
            read_interface: null,
            read_stream: null,
            write_stream: null
        }
    }
}

export async function readWholeFile(
    tilesetSourceId: TilesetSourceIdTypes,
    readBackup: boolean = false,
) {
    let features: IGeoJSONFeature[] = []
    let rl = openReadStream(tilesetSourceId, readBackup)

    try {
        // rl.on('line', async (line) => {
        //     let data = await JSON.parse(line)
        //     features.push(data)
        // })

        for await (const line of rl) {
            // let data = await JSON.parse(line)
            let data = await parse(line)
            features.push(data)
        }

        return features
    } catch (error) {
        logger.error(error)
        return []
    }
}

export async function getFeatureInFileByPinIds(
    tilesetSourceId: TilesetSourceIdTypes,
    pin_ids: string[],
    readBackup: boolean = false,
) {
    let features: IGeoJSONFeature[] = []
    let rl = openReadStream(tilesetSourceId, readBackup)

    try {
        for await (const line of rl) {
            // let data = await JSON.parse(line)
            let data = await parse(line)

            console.log('reading line')

            if (pin_ids.some(s => s === data.properties._id)) {
                console.log('found id')
                features.push(data)
            }

            if (pin_ids.length === features.length) {
                return
            }
        }

        return features
    } catch (error) {
        logger.error(error)
        return []
    }
}

export async function writeTilesetSourceToFile(
    pathAndName: string,
    data: any[]
) {
    const path = pathAndName
    try {
        let file = await fsp.open(path, 'a+')
        for (let i = 0; i < data.length; i++) {
            const feature = data[i];
            await file.write(feature)
            // await file.write('\n')
            await file.write(EOL)
        }
        return file
    } catch (error) {
        console.log('Error in writeTilsetSourceToFile')
        console.log(error)
        this.logger.error(error)
        return null
    }
}

export async function editTilesetSourceFile<T extends IGeoProperties, K extends keyof IGeoProperties>(
    tilesetSourceId: TilesetSourceIdTypes,
    value_pairs: {
        id: string,
        pairs: {
            prop: K
            value: T[K]
        }[]
    }[],
) {
    logger.debug(editTilesetSourceFile.name)

    const {
        read_interface,
        read_stream,
        write_stream } = openReadWriteStream(tilesetSourceId)

    // const read_interface = openReadStream(tilesetSourceId)
    const totalChanges = value_pairs.reduce((sum, elt) =>
        sum += (elt.pairs.length ? elt.pairs.length : 1), 0)
    let numberOfChanges = 0
    // const changes: IGeoJSONFeature[] = []

    console.log('after opening stream')
    // console.log('total changes')
    // console.log(totalChanges)
    console.log('value_pairs:')
    console.log(value_pairs)
    console.log(value_pairs[0].pairs)

    try {
        for await (const line of read_interface) {
            console.log('for await read_interface')
            console.log('read interface line 0')
            let data = await parse(line)
            let newLine = line

            logger.log(editTilesetSourceFile.name)
            console.log(data)

            if (value_pairs.some(s => s.id === data.properties._id)) {

                console.log('id exists')

                let value_pair =
                    value_pairs.find(f => f.id === data.properties._id)

                if (value_pair !== undefined) {
                    console.log('value_pair exists')

                    for (let i = 0; i < value_pair.pairs.length; i++) {
                        const pair = value_pair.pairs[i]
                        let originalValue = data.properties[pair.prop]
                        const propertyToChange = `"${pair.prop}":`
                        // let valueToChange = pair.value
                        let quotes = ``

                        console.log('pairs')
                        console.log(pair)
                        console.log(originalValue)
                        console.log(propertyToChange)

                        if (typeof originalValue === 'number') {
                            // if (!isNaN(Number(originalValue
                            //     .toString()
                            //     .replace(',', '.')))) {
                            // originalValue =
                            //     data.properties[pair.property]
                            //     .toString()
                            //     .replace(',', '.')

                            quotes = ""
                        }

                        if (typeof originalValue === typeof 'boolean') {
                            quotes = ""
                        } else if (['true', 'false'].includes(originalValue)) {
                            quotes = ""
                        }

                        if (typeof originalValue === typeof 'string') {
                            quotes = `"`
                        }

                        let pairToChange = `${propertyToChange}${quotes}${originalValue}${quotes}`
                        let pairToChangeTo = `"${pair.prop}":${quotes}${pair.value}${quotes}`
                        let regexp =
                            // new RegExp(`"${pair.property}":"${pair.value}"`)
                            new RegExp(pairToChange)
                        let ln = line.match(regexp)

                        console.log('props')
                        console.log(pairToChange)
                        console.log(pairToChangeTo)

                        console.log('ln')
                        console.log(ln)

                        if (ln !== null) {
                            numberOfChanges++
                            let mod = ln[0].replace(pairToChange, pairToChangeTo)
                            newLine = newLine.replace(ln[0], mod)
                            // write_stream.write(`${newLine}${EOL}`)

                            // if (numberOfChanges >= totalChanges) {
                            //     read_interface.close()
                            // }
                        }
                    }

                    write_stream.write(`${newLine}${EOL}`)
                } else {
                    write_stream.write(`${line}${EOL}`)
                }
            } else {
                write_stream.write(`${line}${EOL}`)
            }
        }

        await copyTempFile(tilesetSourceId)
        return true
    } catch (error) {
        logger.error(error)
        return false
    }
}

export async function deleteTilesetSourcesFromFile(
    tilesetSourceId: TilesetSourceIdTypes,
    geo_ids: ObjectId[] = [],
) {
    logger.debug(deleteTilesetSourcesFromFile.name)

    const {
        read_interface,
        read_stream,
        write_stream } = openReadWriteStream(tilesetSourceId)

    let numberOfLines = 0
    let numberOfChanges = 0
    // const changes: IGeoJSONFeature[] = []

    console.log('after opening stream')

    try {
        if (geo_ids.length > 0) {
            for await (const line of read_interface) {
                console.log('for await read_interface')
                console.log('read interface line 0')
                let data = await parse(line)
                let newLine = line
                numberOfLines++

                logger.log(deleteTilesetSourcesFromFile.name)
                console.log(data)

                logger.debug(`Comparing if there's a match in file`)
                logger.debug(geo_ids.some(s =>
                    s.toHexString() === data.properties._id))

                console.log(geo_ids)
                console.log(data.properties._id)

                if (geo_ids.some(s =>
                    s.toHexString() === data.properties._id)) {

                    logger.debug('Found match, deleting line')

                    // If line should be removed we're just not writing it instead

                    // read_interface.write(null, { ctrl: true, name: 'u' })
                    // write_stream.write(null, { ctrl: true, name: 'u' })
                    numberOfChanges++
                } else {
                    write_stream.write(`${newLine}${EOL}`)
                }
            }

            await copyTempFile(tilesetSourceId)
        }

        return {
            numberOfLines,
            numberOfChanges,
        }
    } catch (error) {
        logger.error(error)
        return {
            numberOfLines: 0,
            numberOfChanges: 0,
        }
    }
}

export async function getTilesetSourceChanges<T extends IGeoProperties, K extends keyof IGeoProperties>(
    tilesetSourceId: TilesetSourceIdTypes,
    value_pairs: {
        id: string,
        pairs: {
            prop: K
            value: T[K]
        }[]
    }[],
) {
    try {
        console.log(getTilesetSourceChanges.name)

        const read_interface = openReadStream(tilesetSourceId)
        const totalChanges = value_pairs.reduce((sum, elt) =>
            sum += (elt.pairs.length ? elt.pairs.length : 1), 0)
        const changes: IGeoJSONFeature[] = []

        console.log('totalChanges')
        console.log(totalChanges)

        read_interface.on('line', async (line) => {
            console.log('read interface line 0')
            let data = await parse(line)

            logger.log(editTilesetSourceFile.name)
            console.log(data)

            if (value_pairs.some(s => s.id === data.properties._id)) {

                console.log('id exists')

                let value_pair =
                    value_pairs.find(f => f.id === data.properties._id)

                if (value_pair !== undefined) {
                    console.log('value_pair exists')

                    for (let i = 0; i < value_pair.pairs.length; i++) {
                        const pair = value_pair.pairs[i]
                        // data.properties[pair.property] =
                        //     pair.value

                        let pairToChange = `"${pair.prop}":"${data.properties[pair.prop]}"`
                        let pairToChangeTo = `"${pair.prop}":"${pair.value}"`
                        let regexp =
                            // new RegExp(`"${pair.property}":"${pair.value}"`)
                            new RegExp(pairToChange)
                        let ln = line.match(regexp)

                        console.log('ln')
                        console.log(ln)

                        if (ln !== null) {
                            let mod = ln[0].replace(pairToChange, pairToChangeTo)
                            let newLine = line.replace(ln[0], mod)
                            let change = await parse(newLine)
                            changes.push(change)
                        } else {
                            return []
                        }
                    }
                } else {
                    return []
                }
            } else {
                return []
            }

            if (changes.length >= totalChanges) {
                read_interface.close()
            }
        })

        return changes
    } catch (error) {
        logger.error(error)
        return []
    }
}