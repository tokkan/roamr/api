import { Logger } from "@nestjs/common"
import { ObjectId } from "mongodb"
import { DatabaseModels } from "src/database/mongodb"
import { MongoDatabaseHandle as db } from "src/database/mongodb/database"
import { IPinEntity } from "../models"

const logger = new Logger('PinMongoRepository')

export async function isPinCollectionEmpty() {
    try {
        // let c = await db.collectionWorld({
        //     db: 'roamr-world',
        //     collectionWorld: DatabaseModels.PINS
        // })
        let c = await db.collectionWorld(DatabaseModels.PINS)
        let count = await c.countDocuments()
        return count > 0 ? false : true
    } catch (error) {
        return true
    }
}

export async function getPinByProp<T extends IPinEntity, K extends keyof T>(
    prop: K,
    value: T[K]
) {
    try {
        let c = await db.collectionWorld(DatabaseModels.PINS)
        let response = await c.findOne({
            [prop]: value,
        })
        return response
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function getAllPins(
    limit: number,
    offset: number
) {
    try {
        let c = await db.collectionWorld(DatabaseModels.PINS)
        let response = await c.find({}, {
            limit,
            skip: offset
        }).toArray()

        if (response) {
            return response
        } else {
            return []
        }
    } catch (error) {
        console.log(error)
        logger.error(error)
        return []
    }
}

export async function getAllPinsForUser(
    user_id: ObjectId,
    limit: number,
    offset: number
) {
    try {
        let c = await db.collectionWorld(DatabaseModels.PINS)
        let response = await c.find({
            _id: user_id
        }, {
            limit,
            skip: offset
        }).toArray()

        if (response) {
            return response
        } else {
            return []
        }
    } catch (error) {
        logger.error(error)
        return []
    }
}

export async function createPinMongo(entity: IPinEntity) {
    try {
        let c = await db.collectionWorld(DatabaseModels.PINS)
        let response = await c.insertOne(entity)
        return response
    } catch (error) {
        console.log('error in createPinMongo')
        console.log(error)
        logger.error(error)
        return error
    }
}

export async function batchCreatePinsMongo(
    entities: IPinEntity[]
) {
    try {
        let c =
            await db.collectionWorld(DatabaseModels.PINS)
        let res = await c.insertMany(entities)
        return res
    } catch (error) {
        logger.error(error)
        return false
    }
}

export async function updatePinByProp<T extends IPinEntity, K extends keyof T>(
    id: ObjectId,
    prop: K,
    value: T[K]
) {
    try {
        let c = await db.collectionWorld(DatabaseModels.PINS)
        await c.updateOne({
            _id: id
        }, {
            $set: {
                [prop]: value
            }
        })
    } catch (error) {
        logger.error(error)
        return null
    }
}

/**
 * The props variable uses a reduce function to create a object with
 * the param prop as a property and value as value, like:
 * {
 *  [prop1]: value1,
 *  [prop2]: value2,
 *  ...and so on with all props in the pairs array
 * }
 * @param id 
 * @param pairs 
 */
export async function updatePinByValuePairs<T extends IPinEntity, K extends keyof T>(
    id: ObjectId,
    pairs: {
        prop: K,
        value: T[K]
    }[],
) {
    let props = pairs.reduce((prev, curr) =>
        ({
            ...prev,
            [curr.prop]: curr.value,
            [`properties.${curr.prop}`]: curr.value,
        }), {})

    try {
        let c = await db.collectionWorld(DatabaseModels.PINS)
        await c.updateOne({
            _id: id
        }, {
            $set: props
        })
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function deletePinById(pin_id: ObjectId) {
    try {
        let c = await db.collectionWorld(DatabaseModels.PINS)
        let res = await c.deleteOne({
            _id: pin_id,
        })

        if (res) {
            return res.deletedCount
        }
        return null
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function deletePinsById(pin_ids: ObjectId[]) {
    try {
        let c = await db.collectionWorld(DatabaseModels.PINS)
        let res = await c.deleteMany({
            _id: { $in: pin_ids }
        })

        if (res) {
            return res.deletedCount
        }
        return null
    } catch (error) {
        logger.error(error)
        return null
    }
}