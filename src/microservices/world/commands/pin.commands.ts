import { CommandHandler, ICommandHandler, EventPublisher, EventBus } from '@nestjs/cqrs'
import { Logger } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { simplifyGeoJSON, simplifyGeoJSONFeatures } from '../common/helper'
import { IGeoJSONFeature, IPointAction, JobActions } from '../common'
import { PinService } from '../services/pin/pin.service'
import { IPinGeoProperties, IPinImage, PinTypes } from '../models'
import { PinEventHandle } from '../events/pin.aggregate'
import { CountryTypes, LanguageTypes } from 'src/models/common/common'
import { InjectQueue } from '@nestjs/bull'
import { RedisJobNames, RedisQueues } from '../world.module'
import { Queue } from 'bull'

export class CreatePinMongoCommand {
    constructor(
        // public readonly entity: ICreatePinFromBusinessPayload,
        public readonly entity: {
            // pin_fire_id: string,
            pin_mongo_id: string,
            // municipality_fire_id: string,
            municipality_mongo_id: string,
            zone_id: string,
            longitude: number,
            latitude: number,
            pin_type: PinTypes,
            sub_pin_types: PinTypes[],
            title: string,
            sub_title: string,
            description: string[],
            language: LanguageTypes,
            image?: IPinImage,
            business_id: ObjectId,
            country: CountryTypes,
        }
    ) { }
}

export class ActivatePinMongoCommand {
    constructor(
        public readonly pin_id: string
    ) { }
}

@CommandHandler(CreatePinMongoCommand)
export class CreatePinMongoCommandHandler<T extends CreatePinMongoCommand> implements ICommandHandler<T> {
    private readonly logger = new Logger(CreatePinMongoCommandHandler.name)
    private readonly eventHandle: PinEventHandle

    constructor(
        private publisher: EventPublisher,
        private eventBus: EventBus,
        private readonly pinService: PinService,
        @InjectQueue(RedisQueues.PINS) 
        private readonly pinQueue: Queue<JobActions>
    ) {
        this.eventHandle =
            publisher.mergeObjectContext(
                new PinEventHandle(new ObjectId().toHexString()))
        this.eventHandle.autoCommit = true
    }

    async execute(command: T): Promise<any> {
        this.logger.debug('Executing PinEventHandler')
        // try {
        // const entity = {
        //     ...command.entity,
        //     _id: new ObjectId(command.entity._id),
        //     municipality_mongo_id: new ObjectId(command.entity.municipality_mongo_id),
        // }

        let properties: IPinGeoProperties = {
            title: command.entity.title,
            geo_type: 'pin',
            is_active: false,
            country: command.entity.country,
            _id: command.entity.pin_mongo_id,
            // fire_id: command.entity.pin_fire_id,
            pin_type: command.entity.pin_type,
            sub_pin_types: command.entity.sub_pin_types,
        }

        let raw_features: IGeoJSONFeature[] = [
            {
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: [
                        command.entity.longitude,
                        command.entity.latitude,
                    ]
                },
                // properties: {
                //     title: command.entity.title,
                //     geo_type: 'pin',
                //     is_active: false,
                //     country: command.entity.country,
                //     _id: command.entity.pin_mongo_id,
                //     fire_id: command.entity.pin_fire_id,
                // }
                properties: properties,
            }
        ]

        let features =
            simplifyGeoJSONFeatures('roamr-pins', raw_features)

        let res =
            await this.pinService
                .createPinFromBusiness({
                    ...command.entity,
                    longitude: (features[0].geometry as IPointAction)
                        .coordinates[0],
                    latitude: (features[0].geometry as IPointAction)
                        .coordinates[1],
                })

        // features.forEach(async (f) => {
        //     await this.tilingService
        //         .queTilesetSourceJob({
        //             tilesetSourceId: 'roamr-pins',
        //             type: 'add',
        //             feature: f,
        //         })
        // })
        
        let jobs: {
            name?: string
            data: JobActions
            opts?: any
        }[] = features.map(f => {
            let data: JobActions = {
                tilesetSourceId: 'roamr-pins',
                type: 'add',
                feature: f,
            }
            return {
                name: RedisJobNames.PIN_ADD,
                data,
                // opts: {
                // }
            }
        })
        this.pinQueue.addBulk(jobs)
    }
}

@CommandHandler(ActivatePinMongoCommand)
export class ActivatePinMongoCommandHandler<T extends ActivatePinMongoCommand> implements ICommandHandler<T> {
    private readonly logger = new Logger(ActivatePinMongoCommandHandler.name)
    private readonly eventHandle: PinEventHandle

    constructor(
        private publisher: EventPublisher,
        private eventBus: EventBus,
        private readonly pinService: PinService,
    ) {
        this.eventHandle =
            publisher.mergeObjectContext(
                new PinEventHandle(new ObjectId().toHexString()))
        this.eventHandle.autoCommit = true
    }

    async execute(command: T): Promise<any> {
        this.logger.debug('Executing PinEventHandler')
        this.pinService.activatePinMongo(command.pin_id)
    }
}