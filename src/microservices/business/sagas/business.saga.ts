import { Injectable, Logger } from "@nestjs/common"
import { Saga, ICommand, ofType } from "@nestjs/cqrs"
import { Observable } from "rxjs"
import { map, delay, mergeMap } from 'rxjs/operators'
import { CreateBusinessEvent, ActivateBusinessEvent } from "../events/business.aggregate"
import { CreateBusinessCommand, ActivateBusinessCommand } from "../commands/business.commands"
import { CreatePinMongoCommand } from "src/microservices/world/commands/pin.commands"

@Injectable()
export class BusinessSagas {
    private readonly logger = new Logger(BusinessSagas.name)

    @Saga()
    businessCreated = (events$: Observable<any>): Observable<ICommand> => {
        return events$
            .pipe(
                ofType(CreateBusinessEvent),
                // delay(1000),
                map(event => {
                    this.logger.debug('Saga Create business event func')
                    // this.logger.debug(events$)
                    // this.logger.debug(event)

                    const business = event.business_mongo
                    const pin = event.pin

                    // const payload = event.entity
                    // const pinEntity: IPinFirestoreEntity = {
                    //     _id: event.pin_mongo_id,
                    //     fire_id: event.pin_fire_id,
                    //     municipality_fire_id: event.municipality_fire_id,
                    //     municipality_mongo_id: event.municipality_mongo_id,
                    //     trail_ids: [],
                    //     area_ids: [],
                    //     zone_ids: [],
                    //     rating: {
                    //         clicks: 0,
                    //         following: 0,
                    //         is_sponsored: false,
                    //         likes: 0,
                    //     },
                    //     is_active: false,
                    //     pin_type: event.pin_type,
                    //     sub_pin_types: [],
                    //     geo_type: 'pin',
                    //     business_id: '',
                    //     is_business: false,
                    //     qr_code: '',
                    //     info: [{
                    //         title: '',
                    //         description: [''],
                    //         image: {
                    //             source: '',
                    //             source_type: 'url',
                    //             alt: '',
                    //         },
                    //         index: 0,
                    //         lang: 'se',
                    //         sub_title: '',
                    //     }],
                    //     geometry: {
                    //         type: 'Point',
                    //         coordinates: [event.longitude, event.latitude],
                    //     },
                    //     properties: {
                    //         title: payload.name,
                    //         description: payload.description,
                    //         geo_type: "pin",
                    //         country: 'sweden',
                    //         is_active: false,
                    //     }
                    // }

                    const commands: ICommand[] = [
                        new CreateBusinessCommand(event.business_mongo),
                        // // new CreatePinFirestoreCommand(pinEntity),
                        // // new CreatePinMongoCommand(pinEntity),
                        // new CreatePinFirestoreCommand(pin),
                        new CreatePinMongoCommand(pin)
                    ]

                    return commands
                }),
                mergeMap(f => f),
            )
    }

    @Saga()
    businessActivated = (events$: Observable<any>): Observable<ICommand> => {
        return events$
            .pipe(
                ofType(ActivateBusinessEvent),
                // delay(1000),
                map(event => {
                    this.logger.debug('Saga activate business event func')
                    // this.logger.debug(events$)
                    // this.logger.debug(event)

                    const commands: ICommand[] = [
                        new ActivateBusinessCommand(event.business_id),
                        // new ActivatePinFirestoreCommand(event.pin_fire_id),
                        // new ActivatePinMongoCommand(event.pin_mongo_id),
                        // new CreateBusinessTilesetSourceCommand(
                        //     'roamr-pins',
                        //     event.business_id,
                        //     // event.pin_fire_id,
                        //     // event.pin_mongo_id,
                        // )
                    ]

                    return commands
                }),
                mergeMap(f => f),
            )
    }
}