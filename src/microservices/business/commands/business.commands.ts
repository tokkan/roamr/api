import { CommandHandler, ICommandHandler, EventPublisher, EventBus } from '@nestjs/cqrs'
import { Logger } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { BusinessEventHandle } from '../events/business.aggregate'
import { BusinessService } from '../services/business.service'
import { IBusinessMongoEntity } from '../models/business'

export class CreateBusinessCommand {
    constructor(
        public readonly mongo_entity: IBusinessMongoEntity,
        // public readonly entity: IBusinessFireEntity,
    ) { }
}

export class ActivateBusinessCommand {
    constructor(
        public readonly business_id: string
    ) { }
}

@CommandHandler(CreateBusinessCommand)
export class CreateBusinessCommandHandler<T extends CreateBusinessCommand> implements ICommandHandler<T> {
    private readonly logger = new Logger(CreateBusinessCommandHandler.name)
    private readonly eventHandle: BusinessEventHandle

    constructor(
        private publisher: EventPublisher,
        private eventBus: EventBus,
        private readonly businessService: BusinessService,
    ) {
        this.eventHandle =
            publisher.mergeObjectContext(
                new BusinessEventHandle(new ObjectId().toHexString()))
        this.eventHandle.autoCommit = true
    }

    async execute(command: T): Promise<any> {
        this.logger.debug('Executing BusinessEventHandler')
        try {
            const entity = command.mongo_entity
            let res =
                await this.businessService.createBusinessMongo(entity)
            return res
        } catch (error) {
            this.logger.error(error)
        }
    }
}

@CommandHandler(ActivateBusinessCommand)
export class ActivateBusinessCommandHandler<T extends ActivateBusinessCommand> implements ICommandHandler<T> {
    private readonly logger = new Logger(ActivateBusinessCommandHandler.name)
    private readonly eventHandle: BusinessEventHandle

    constructor(
        private publisher: EventPublisher,
        private eventBus: EventBus,
        private businessService: BusinessService
    ) {
        this.eventHandle =
            publisher.mergeObjectContext(
                new BusinessEventHandle(new ObjectId().toHexString()))
        this.eventHandle.autoCommit = true
    }

    async execute(command: T): Promise<any> {
        this.logger.debug('Executing BusinessEventHandler')
        try {
            return await
                this.businessService
                    .activateBusinessInFirestore(command.business_id)

        } catch (error) {
            this.logger.error(error)
            return null
        }
    }
}