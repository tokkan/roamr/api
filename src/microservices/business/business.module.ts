import { Module } from '@nestjs/common'
import { CqrsModule } from '@nestjs/cqrs'
import { BusinessService } from './services/business.service'
import { BusinessEventHandle } from './events/business.aggregate'
import { BusinessSagas } from './sagas/business.saga'
import {
  CreateBusinessCommandHandler,
  ActivateBusinessCommandHandler
} from './commands/business.commands'

const commandHandlers = [
  CreateBusinessCommandHandler,
  ActivateBusinessCommandHandler,
  // AddSocialAccountToUserCommandHandler,
  // CreatePlayerCommandHandler,
]

const eventHandlers = [
  // AddPlayerToUserEventHandler,
]

@Module({
  imports: [
    CqrsModule,
  ],
  providers: [
    BusinessService,
    ...commandHandlers,
    ...eventHandlers,
    BusinessEventHandle,
    BusinessSagas,
  ],
  exports: [
    BusinessService,
  ]
})
export class BusinessModule { }
