import { Logger } from "@nestjs/common"
import { Firebase as db } from 'src/database/firebase/database'
import { DatabaseModels } from "src/database/firebase/database"
import { IBusinessFireEntity } from "../models/business"

const logger = new Logger('BusinessRepository')

// async function parse(data: any) {
//     return await JSON.parse(JSON.stringify(data))
// }

async function parse<T extends IBusinessFireEntity>(data: any): Promise<T> {
    return await JSON.parse(JSON.stringify(data))
}

export async function getBusinessById(business_id: string) {
    try {
        let c = db.collection(DatabaseModels.SE_BUSINESS)
        let response = await c.doc(business_id).get()
        if (!response.exists) {
            return null
        } else {
            return response.data()
        }
    } catch (error) {
        console.log(error)
        logger.error(error)
        return null
    }
}

export async function getBusinessForUserId(user_id: string) {
    try {
        let c = db.collection(DatabaseModels.SE_BUSINESS)
        let response = await c.where('users', 'array-contains', user_id).get()
        if (response.empty) {
            return []
        } else {
            return response.docs.map(m => m.data())
        }
    } catch (error) {
        logger.error(error)
        return []
    }
}

export async function getBusinessRequests(
    limit: number,
    offset: number,
) {
    try {
        let c = db.collection(DatabaseModels.SE_BUSINESS)
        let response = await c
            .where('is_active', '==', false)
            // .orderBy('name')
            // .offset(offset)
            // .limit(limit)
            .get()

        if (response.empty) {
            return []
        } else {
            return response.docs.map(m => m.data())
        }
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function createBusiness(entity: IBusinessFireEntity) {
    try {
        let c = db.collection(DatabaseModels.SE_BUSINESS)

        if (entity.fire_id === undefined ||
            entity.fire_id.length <= 0) {
            entity.fire_id = c.doc().id
        }

        let response = await c.doc(entity.fire_id).set(entity)

        return response

    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function updateBusiness(entity: IBusinessFireEntity) {
    try {
        let c = db.collection(DatabaseModels.SE_BUSINESS)
        let response = await c.doc(entity.fire_id).update(entity)
        return response
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function updateBusinessByValuePairs<
    T extends IBusinessFireEntity,
    K extends keyof IBusinessFireEntity
>(
    id: string,
    valuePairs: { prop: K, value: T[K] }[]
) {
    try {
        let c = db.collection(DatabaseModels.SE_BUSINESS)
        let query = await c.doc(id).get()
        let entity = await parse<IBusinessFireEntity>(query.data())

        valuePairs.forEach((f) => {
            entity[f.prop] = f.value
        })

        let response = await c.doc(id).update(entity)
        return response
    } catch (error) {
        logger.error(error)
        return null
    }
}

export async function deleteBusiness(entity_id: string) {
    try {
        let c = db.collection(DatabaseModels.SE_BUSINESS)
        let response = await c.doc(entity_id).delete()
        return response
    } catch (error) {
        logger.error(error)
        return null
    }
}