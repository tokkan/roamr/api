import { Logger } from "@nestjs/common"
import { ObjectId } from "mongodb"
import { DatabaseModels } from "src/database/mongodb/models"
import { IBusinessMongoEntity } from "../models/business"
import { MongoDatabaseHandle as db } from 'src/database/mongodb'

const logger = new Logger('BusinessMongoRepository')

// export async function getBusinessById(business_id: string) {
//     try {
//         let c = db.collection(DatabaseModels.BUSINESS)
//         let response = await c.doc(business_id).get()
//         if (!response.exists) {
//             return null
//         } else {
//             return response.data()
//         }
//     } catch (error) {
//         console.log(error)
//         logger.error(error)
//         return null
//     }
// }

export async function getBusinessForUserId(user_id: string) {
    try {
        // let c = await db.collection(DatabaseModels.BUSINESS)
        let c = await db.collectionSocial(DatabaseModels.BUSINESS)
        let response = 
            await c.find({
                users: {
                    $in: [user_id]
                }
            }).toArray()
        if (response) {
            return response
        } else {
            return []
        }
    } catch (error) {
        logger.error(error)
        return []
    }
}

// export async function getBusinessRequests(
//     limit: number,
//     offset: number,
// ) {
//     try {
//         let c = db.collection(DatabaseModels.BUSINESS)
//         let response = await c
//             .where('is_active', '==', false)
//             // .orderBy('name')
//             // .offset(offset)
//             // .limit(limit)
//             .get()

//         if (response.empty) {
//             return []
//         } else {
//             return response.docs.map(m => m.data())
//         }
//     } catch (error) {
//         logger.error(error)
//         return null
//     }
// }

export async function createBusinessMongo(entity: IBusinessMongoEntity) {
    try {
        // let c = await db.collection(DatabaseModels.BUSINESS)
        let c = await db.collectionSocial(DatabaseModels.BUSINESS)
        let response = 
            await c.insertOne(entity)
        return response
    } catch (error) {
        logger.error(error)
        return null
    }
}

// export async function updateBusiness(entity: IBusinessFireEntity) {
//     try {
//         let c = db.collection(DatabaseModels.BUSINESS)
//         let response = await c.doc(entity.fire_id).update(entity)
//         return response
//     } catch (error) {
//         logger.error(error)
//         return null
//     }
// }

export async function deleteBusinessById(business_id: ObjectId) {
    try {
        // let c = await db.collection(DatabaseModels.BUSINESS)
        let c = await db.collectionSocial(DatabaseModels.BUSINESS)
        let response = await c.deleteOne({
            _id: business_id,
        })
        return response
    } catch (error) {
        logger.error(error)
        return null
    }
}