import { Injectable, Logger } from '@nestjs/common'
import { BusinessEventHandle, ICreatePinFromBusinessPayload } from '../events/business.aggregate'
import { CommandBus, EventPublisher } from '@nestjs/cqrs'
import { ObjectId } from 'mongodb'
import { getBusinessRequests, getBusinessById, updateBusiness as updateBusinessFirestore, updateBusinessByValuePairs } from '../repositories/business.repository'
import { createBusinessMongo, getBusinessForUserId, deleteBusinessById } from '../repositories/business-mongo.repository'
import { IBusinessFireEntity, IBusinessMongoEntity, SocialMediaTypes, IBusinessEntityBase } from '../models/business'
import { buildQRCode } from 'src/common/utility/qr-code'
import { ICreateBusinessPayload } from '../models/payloads'

@Injectable()
export class BusinessService {
    private readonly logger = new Logger(BusinessService.name)
    private readonly eventHandle: BusinessEventHandle

    constructor(
        // private readonly commandBus: CommandBus,
        private readonly publisher: EventPublisher,
    ) {
        this.eventHandle =
            this.publisher.mergeObjectContext(
                new BusinessEventHandle(new ObjectId().toHexString()))
        this.eventHandle.autoCommit = true
    }

    async parse<T extends IBusinessFireEntity>(data: any): Promise<T | null> {
        try {
            return await JSON.parse(JSON.stringify(data))
        } catch (error) {
            this.logger.error(error)
            return null
        }
    }

    async checkBusinessesForUser() {
        // check if user has too many requests/businesses
    }

    async getBusinessById(business_id: string) {
        let response = await getBusinessById(business_id)
        let entity = await this.parse(response)
        return entity
    }

    async getBusinessForUser(user_id: string) {
        return await getBusinessForUserId(user_id)
    }

    async getBusinessRequests(limit: number = 10, offset: number = 0) {
        return await getBusinessRequests(limit, offset)
    }

    async createBusinessMongo(entity: IBusinessMongoEntity) {
        let res =
            await createBusinessMongo(entity)
        return res
    }

    async createBusinessFromPayload(
        user_id: string,
        payload: ICreateBusinessPayload
    ) {
        let social_media:
            { handle: string, type: SocialMediaTypes }[] = []

        const {
            businessName,
            sub_title,
            description,
            language,
            emailAddress,
            latitude,
            longitude,
            municipality,
            phoneNumber,
            pin_type,
            socialMediaHandle,
            socialMediaProvider,
            sub_pin_types,
            zone_id,
            country,
        } = payload

        if (socialMediaHandle !== undefined &&
            socialMediaHandle.length > 0
        ) {
            social_media.push({
                handle: socialMediaHandle,
                type: socialMediaProvider,
            })
        }

        let business_mongo_id = new ObjectId()
        // let business_fire_id = await generatePinId()
        // let pin_fire_id = await generatePinId()
        let pin_mongo_id = new ObjectId()
        let qr_code = buildQRCode(business_mongo_id.toHexString(), 'business')

        let entity: IBusinessEntityBase = {
            parent_id: '',
            is_parent: false,
            customer_id: '',
            customer_user_id: user_id,
            users: [user_id],
            name: businessName,
            sub_title: sub_title,
            description: description,
            qr_code: qr_code,
            business_tags: [],
            contact: {
                email: emailAddress,
                phone: phoneNumber,
                social_media: social_media,
            },
            is_verified: false,
            // merchandise: {
            //     content: '',
            //     currency_type: CurrencyTypes.SEK,
            //     items: [],
            //     timetable: null,
            //     title: '',
            // },
            custom_business_hours: {
                start: null,
                end: null,
                times: [],
            },
            default_business_hours: {
                times: []
            },
            is_active: false,
        }

        let business_mongo: IBusinessMongoEntity = {
            ...entity,
            _id: business_mongo_id,
            // fire_id: business_fire_id,
            pin_ids: [{
                _id: pin_mongo_id,
                // fire_id: pin_fire_id,
                index: 0,
            }],
            trail_ids: [],
            area_ids: [],
        }

        // let business_fire: IBusinessFireEntity = {
        //     ...entity,
        //     _id: business_mongo_id.toHexString(),
        //     fire_id: business_fire_id,
        //     pin_ids: [{
        //         _id: pin_mongo_id.toHexString(),
        //         fire_id: pin_fire_id,
        //         index: 0,
        //     }],
        //     trail_ids: [],
        //     area_ids: [],
        // }

        let pin: ICreatePinFromBusinessPayload = {
            // business_id: business_fire_id,
            business_id: business_mongo_id,
            // pin_fire_id,
            pin_mongo_id: pin_mongo_id.toHexString(),
            // municipality_fire_id: municipality.municipality_fire_id,
            municipality_mongo_id: municipality.municipality_mongo_id,
            zone_id,
            title: businessName,
            sub_title,
            description,
            language,
            latitude,
            longitude,
            pin_type,
            sub_pin_types,
            image: null,
            country,
        }

        this.eventHandle.createBusiness(
            user_id,
            // pin_fire_id,
            // pin_mongo_id.toHexString(),
            // municipality.municipality_fire_id,
            // municipality.municipality_mongo_id,
            // zone_id,
            // latitude,
            // longitude,
            // pin_type,
            business_mongo,
            // business_fire,
            pin,
        )
    }

    async updateBusinessFirestore(entity: IBusinessFireEntity) {
        try {
            let res = await updateBusinessFirestore(entity)
            return res
        } catch (error) {
            this.logger.error(error)
            return null
        }
    }

    async updateBusinessMongoEntityByProp<T extends IBusinessFireEntity, K extends keyof T>(
        entity: T,
        prop: K,
        value: T[K]
    ) {
        entity[prop] = value
        return entity
    }

    async updateBusinessMongoByProp<T extends IBusinessFireEntity, K extends keyof T>(
        entity: T,
        prop: K,
        value: T[K]
    ) {
        // TODO: Update business by directly in repository
    }

    async updateBusinessFirestoreByProp<
        T extends IBusinessFireEntity,
        K extends keyof IBusinessFireEntity
    >(
        id: string,
        prop: K,
        value: T[K]
    ) {
        let res = await updateBusinessByValuePairs(id, [{
            prop,
            value,
        }])

        return res
    }

    updateBusinessByProp<T extends IBusinessFireEntity, K extends keyof T>(
        entity: T,
        prop: K,
        value: T[K]
    ) {
        entity[prop] = value
        return entity
    }

    async deleteBusinessById(business_id: ObjectId) {
        let res = await deleteBusinessById(business_id)
        return res
    }

    async activateBusinessEvent(business_id: string) {
        // validation to check if all business requirements are met
        // if validation passed, set active state to true
        let document = await this.getBusinessById(business_id)
        let entity = await this.parse<IBusinessFireEntity>(document)

        if (entity !== null) {
            // entity = this.updateBusinessByProp(entity, 'is_active', true)
            // let r1 = await updateBusinessFirestore(entity)
            // return r1
            this.eventHandle.activateBusiness(
                business_id,
                // entity.pin_fire_id,
                // entity.pin_mongo_id
            )
        } else {
            this.logger.error(`Business entity couldn't be found`)
            return null
        }
    }

    async activateBusinessInFirestore(business_id: string) {
        return await updateBusinessByValuePairs(business_id, [{
            prop: 'is_active',
            value: true
        }])
    }
}
