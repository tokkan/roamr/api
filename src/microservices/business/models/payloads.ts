import { PinTypes } from "src/microservices/world/models";
import { LanguageTypes, CountryTypes } from "src/models/common/common";
import { SocialMediaTypes } from "./business";

export interface IMunicipalityPayload {
    municipality_fire_id: string
    municipality_mongo_id: string
    municipality_name: string
    municipality_code: string
}

export interface ICreateBusinessPayload {
    zone_id: string
    businessName: string
    sub_title: string
    description: string[]
    language: LanguageTypes
    latitude: number
    longitude: number
    emailAddress: string
    phoneNumber: string
    pin_type: PinTypes
    // sub_pin_types: (PinTypes & SubPinTypes)[]
    sub_pin_types: PinTypes[]
    socialMediaHandle: string
    socialMediaProvider: SocialMediaTypes
    municipality: IMunicipalityPayload
    country: CountryTypes
}