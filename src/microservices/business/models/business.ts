import { ObjectId } from "mongodb"
import { IFirestoreEntity, IMongoStringEntity, IMongoEntity } from "src/database/models";

export enum SocialMediaTypes {
    TWITTER = 'Twitter',
    INSTAGRAM = 'Instagram',
    FACEBOOK = 'Facebook',
}

export interface ISocialMedia {
    type: SocialMediaTypes
    handle: string
}

export enum DaysTypes {
    SUNDAY = 1,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
}

export interface IBusinessHoursDay {
    day: DaysTypes
    opens_at_h: number
    opens_at_m: number
    closes_at_h: number
    closes_at_m: number
}

export interface IBusinessHours {
    times: IBusinessHoursDay[]
}

export interface ICustomBusinessHours extends IBusinessHours {
    start: Date
    end: Date
}

export interface IContact {
    phone: string
    email: string
    social_media: ISocialMedia[]
}

export enum BusinessTags {
    INFORMATION = 'Information',
    SHOP = 'Shop',
    restaurant = 'Restaurant',
    cafe = 'Cafe',
    EVENT = 'Event',
}

export interface IConnectedMongoPin {
    _id: ObjectId
    // fire_id: string
    index: number
}

export interface IConnectedFirePin {
    _id: string
    fire_id: string
    index: number
}

export interface IBusinessEntityBase {
    parent_id: string
    is_parent: boolean
    customer_id: string
    customer_user_id: string
    users: string[]
    name: string
    sub_title: string
    description: string[]
    qr_code: string
    business_tags: BusinessTags[]
    // merchandise: IMerchandise
    default_business_hours?: IBusinessHours
    custom_business_hours?: ICustomBusinessHours
    contact: IContact
    is_verified: boolean
    is_active: boolean
}

export interface IBusinessFireEntity extends IBusinessEntityBase, IFirestoreEntity, IMongoStringEntity {
    pin_ids: IConnectedFirePin[]
    trail_ids: IConnectedFirePin[]
    area_ids: IConnectedFirePin[]
}

export interface IBusinessMongoEntity extends IBusinessEntityBase, IMongoEntity {
    pin_ids: IConnectedMongoPin[]
    trail_ids: IConnectedMongoPin[]
    area_ids: IConnectedMongoPin[]
}