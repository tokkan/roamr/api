import { Logger } from "@nestjs/common"
import { AggregateRoot } from "@nestjs/cqrs"
import { ObjectId } from "mongodb"
import * as QRCode from 'qrcode'
import * as jimp from 'jimp'
import { PinTypes, IPinImage } from "src/microservices/world/models"
import { LanguageTypes, CountryTypes } from "src/models/common/common"
import { IBusinessMongoEntity } from "../models/business"

export interface ICreatePinFromBusinessPayload {
    // pin_fire_id: string,
    pin_mongo_id: string,
    // municipality_fire_id: string,
    municipality_mongo_id: string,
    zone_id: string,
    longitude: number,
    latitude: number,
    pin_type: PinTypes,
    sub_pin_types: PinTypes[],
    title: string,
    sub_title: string,
    description: string[],
    language: LanguageTypes,
    image?: IPinImage,
    business_id: ObjectId,
    country: CountryTypes,
}

export class CreateBusinessEvent {
    constructor(
        public readonly user_id: string,
        // public readonly pin_fire_id: string,
        // public readonly pin_mongo_id: string,
        // public readonly municipality_fire_id: string,
        // public readonly municipality_mongo_id: string,
        // public readonly zone_id: string,
        // public readonly longitude: number,
        // public readonly latitude: number,
        // public readonly pin_type: PinTypes,
        public readonly business_mongo: IBusinessMongoEntity,
        // public readonly business_fire: IBusinessFireEntity,
        public readonly pin: ICreatePinFromBusinessPayload,
    ) { }
}

export class ActivateBusinessEvent {
    constructor(
        public readonly business_id: string,
        // public readonly pin_fire_id: string,
        // public readonly pin_mongo_id: string,
    ) { }
}

export class BusinessEventHandle extends AggregateRoot {
    private readonly logger = new Logger(BusinessEventHandle.name)

    constructor(private readonly id: string) {
        super()
    }

    async createBusiness(
        user_id: string,
        // pin_fire_id: string,
        // pin_mongo_id: string,
        // municipality_fire_id: string,
        // municipality_mongo_id: string,
        // zone_id: string,
        // latitude: number,
        // longitude: number,
        // pin_type: PinTypes,
        mongo_business: IBusinessMongoEntity,
        // fire_business: IBusinessFireEntity,
        pin: ICreatePinFromBusinessPayload,
    ) {
        this.apply(new CreateBusinessEvent(
            user_id,
            // pin_fire_id,
            // pin_mongo_id,
            // municipality_fire_id,
            // municipality_mongo_id,
            // zone_id,
            // latitude,
            // longitude,
            // pin_type,
            mongo_business,
            // fire_business,
            pin,
        ))
        return {
            business_mongo_id: mongo_business._id,
            // business_fire_id: mongo_business.fire_id,
        }
    }

    async activateBusiness(
        business_id: string,
        // pin_fire_id: string,
        // pin_mongo_id: string,
    ) {
        this.apply(new ActivateBusinessEvent(
            business_id,
            // pin_fire_id,
            // pin_mongo_id,
        ))
        // return { business_id, pin_fire_id }
        return business_id
    }
}