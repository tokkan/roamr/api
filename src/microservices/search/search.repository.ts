import { Logger } from "@nestjs/common"
import { MongoDatabaseHandle as db, DatabaseModels } from "src/database/mongodb"
import { IUserSimpleProjection } from "./models"

const logger = new Logger('FollowshipRepository')

export async function freeTextSearch(
    searchText: string,
) {
    // TODO: Search users
    // TODO: Search businesses
    // TODO: Search events
    // TODO: Search pins (just go with pins since all other geos should have a pin?)
    // TODO: Search zones
    // TODO: Search municipalities
}

// TODO: Use RxJS to stream back search results from multiple collections?
// TODO: Create text indexes on collections that should be able to search in
export async function searchUsers(
    searchText: string,
    limit: number,
    skip: number,
) {
    try {
        let c = await db.collectionSocial(DatabaseModels.USERS)
        let res = await c.find<IUserSimpleProjection>(
            {
                $text: {
                    $search: searchText
                },
                // score: { $meta: "textScore" }
            })
            .project({
                score: { $meta: "textScore" },
                _id: 1,
                username: 1,
                full_name: 1,
                qr_code: 1,
                avatar_url: 1,
                avatar: 1,
            })
            // .sort({ score: { $meta: "textScore" } })
            .sort({ score: { $meta: "textScore" } })
            .limit(limit)
            .skip(skip)
            .toArray()

        return res
    } catch (error) {
        console.debug('searchUsers error:')
        console.debug(error)
        logger.error(error)
        return null
    }
}