import { Injectable, Logger } from '@nestjs/common'
import { IUserSimple } from './models'
import { searchUsers } from './search.repository'

@Injectable()
export class SearchService {
    private readonly logger = new Logger(SearchService.name)

    async searchUsers(
        searchText: string,
        limit: number,
        skip: number,
    ) {
        let res = await searchUsers(searchText, limit, skip)
        let users: IUserSimple[] = res.map(m => {
            return {
                user_id: m._id,
                username: m.username,
                full_name: m.full_name,
                avatar: m.avatar,
                avatar_url: m.avatar_url,
                qr_code: m.qr_code,
                type: 'user',
            }
        })
        return users
    }
}
