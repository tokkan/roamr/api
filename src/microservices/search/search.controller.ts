import { Controller, Inject, Logger } from '@nestjs/common'
import { ClientProxy, MessagePattern } from '@nestjs/microservices'
import { SearchService } from './search.service'

// @Controller('search')
@Controller()
export class SearchController {
    // export class SearchController implements OnModuleInit {
    private readonly logger = new Logger(SearchController.name)

    constructor(
        @Inject('GATEWAY_SERVICE')
        private readonly client: ClientProxy,
        private readonly searchService: SearchService,
    ) { }

    @MessagePattern({
        cmd: 'search_free_text'
    })
    async searchFreeText(data: {
        text: string
        limit: number
        skip: number
    }) {
        const {
            text,
            limit,
            skip,
        } = data

        let res =
            await this.searchService
                .searchUsers(text, limit, skip)
        return res
    }
}
