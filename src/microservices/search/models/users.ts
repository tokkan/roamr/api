import { ObjectId } from "mongodb"

export interface IUserSimple {
    user_id: ObjectId
    username: string
    full_name: string
    qr_code: string
    avatar: string
    avatar_url: string
    type: 'user'
}

export interface IUserSimpleProjection {
    _id: ObjectId
    username: string
    full_name: string
    qr_code: string
    avatar: string
    avatar_url: string
    type: 'user'
}