import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'GATEWAY_SERVICE',
        transport: Transport.TCP,
        options: {
          host: process.env.GATEWAY_MICROSERVICE_HOST,
          port: +process.env.GATEWAY_MICROSERVICE_PORT,
        }
      }
    ]),
  ],
  controllers: [
    SearchController,
  ],
  providers: [SearchService],
  exports: [
    SearchService,
  ]
})
export class SearchModule { }
