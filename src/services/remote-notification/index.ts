import { JobOptions } from "bull"
// import { TokenMessage } from "firebase-admin/lib/messaging"
import { messaging } from 'firebase-admin'
import {
    BulkWriteUpdateOneOperation,
    BulkWriteInsertOneOperation,
    BulkWriteDeleteOneOperation,
    ObjectId
} from "mongodb"
import { IMongoEntity } from "src/database/models"
import { AllNotificationTypes } from "./models"

export enum RedisQueues {
    NOTIFICATIONS = 'notifications',
}

export interface IRemoteNotification extends IMongoEntity {
    type: AllNotificationTypes
    receiver: ObjectId
    // target: ObjectId
    // target_type: FollowerTypes
    data: {
        [key: string]: string
    }
    // notification: TokenMessage
    notification: messaging.Message
    created: Date
    read: boolean
    sent: boolean
}

export interface IRemoteNotificationJob {
    name?: string
    data: IRemoteNotification
    opts?: JobOptions
}

export interface IJobAction<T extends AllNotificationTypes, P> {
    type: T
    operation: P
}

export type NotificationJobActions =
    INotificationMessageAction |
    INotificationFollowAction
// INotificationReactionAction |
// INotificationCommentAction

// export type SubscriptionJobActions =
//     ISubscribeAction |
//     IUnsubscribeAction

// export type TopicJobActions =
//     IUserTopicAction

/**
 * Notification dispatch
 */
export interface INotificationMessageAction extends IJobAction<
    'message',
    {}
    > { }

// export interface INotificationReactionAction extends IJobAction<
//     'reaction',
//     {}
//     > { }

export interface INotificationFollowAction extends IJobAction<
    'follow',
    {}
    > { }

// export interface INotificationCommentAction extends IJobAction<
//     'comment',
//     {}
//     > { }

export interface INotificationJob {
    job: NotificationJobActions
}

// /**
//  *  Notification subscriptions
//  */
// export interface ISubscribeAction extends IJobAction<
//     'subscribe',
//     ISubscriptionPayload
//     > { }

// export interface IUnsubscribeAction extends IJobAction<
//     'unsubscribe',
//     ISubscriptionPayload
//     > { }

// export interface INotificationSubscriptionJob {
//     job: SubscriptionJobActions
// }

// export interface ISubscriptionPayload {
//     subscriber_token: string
//     action: 'post_notifications' | 'event_notifications' | 'story_notifications'
// }

// /**
//  * Topic 
//  */
// export interface IUserTopicAction extends IJobAction<
//     'user_topic',
//     ISubscriptionPayload
//     > { }

// export interface IEventTopic extends IJobAction<
//     'event_topic',
//     ISubscriptionPayload
//     > { }

// export interface IPinTopic extends IJobAction<
//     'pin_topic',
//     ISubscriptionPayload
//     > { }

// export interface IMunicipalityTopic extends IJobAction<
//     'municipality_topic',
//     ISubscriptionPayload
//     > { }

// export interface IZoneTopic extends IJobAction<
//     'zone_topic',
//     ISubscriptionPayload
//     > { }

// export interface INotificationTopicJob {
//     job: TopicJobActions
// }

