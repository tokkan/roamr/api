import { Module } from '@nestjs/common'
import { BullModule } from '@nestjs/bull'
import { CqrsModule } from '@nestjs/cqrs'
import { NotifyFollowTargetCommandHandler } from './commands/remote-notifications.commands'
import { RemoteNotificationJobService } from './services/remote-notification-job.service'
import { RemoteNotificationService } from './services/remote-notification.service'
import { RedisQueues } from '.'

const queryHandlers = [
]

const commandHandlers = [
  NotifyFollowTargetCommandHandler,
]

const eventHandlers = [
]

@Module({
  imports: [
    CqrsModule,
    BullModule.registerQueue(
      {
        name: RedisQueues.NOTIFICATIONS,
        redis: {
          host: process.env.REDIS_HOST,
          port: +process.env.PORT,
        }
      },
    ),
  ],
  providers: [
    ...queryHandlers,
    ...commandHandlers,
    ...eventHandlers,
    RemoteNotificationService,
    RemoteNotificationJobService,
  ],
  exports: [
    RemoteNotificationService,
  ],
})
export class RemoteNotificationModule { }
