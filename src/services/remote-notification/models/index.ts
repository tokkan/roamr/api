// export type NotificationTypes =
//     'follow' |
//     'reaction' |
//     'comment' 

export type NotificationTemplateTypes =
    'follow' |
    'like_post' |
    'like_event' |
    'like_pin' |
    'like_message' |
    'comment' |
    'message' |
    'post' |
    'event' |
    'interested_in_event' |
    'interested_in_pin'

export type DirectNotificationTypes =
    'follow' |
    'like_post' |
    'like_comment' |
    'like_message' |
    'like_event' |
    'like_pin' |
    'comment_post' |
    'comment_event' |
    'message'

// export type SubscriptionNotificationTypes =
//     'post_notification' |
//     'story_notification' |
//     'post_event'

export type UserNotificationTypes =
    'user_post_notification' |
    'user_story_notification'

export type EventNotificationTypes =
    'event_post_notification' |
    'event_story_notification'

export type TopicNotificationTypes =
    'pin_topic' |
    'municipality_topic'

export type AnalyticsNotificationTypes =
    'interested_in_post' |
    'interested_in_event' |
    'interested_in_pin' |
    'has_been_to_post' |
    'has_been_to_pin'

export type AllNotificationTypes =
    DirectNotificationTypes |
    UserNotificationTypes |
    EventNotificationTypes |
    TopicNotificationTypes |
    AnalyticsNotificationTypes

export enum NotificationTemplateColors {
    FOLLOW = '#FF5733',
    LIKE = '#F1C40F',
    COMMENT = '#7408C9',
    MESSAGE = '#EA0B7E',
    POST = '#339ADB',
    EVENT = '#27AE60',
}