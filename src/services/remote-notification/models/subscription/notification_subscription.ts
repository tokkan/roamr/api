import { ObjectId } from "mongodb"
import { IMongoEntity } from "src/database/models"

export type NotificationTopicTypes =
    'user_topic' |
    'event_topic' |
    'story_topic' |
    'pin_topic' |
    'municipality_topic' |
    'zone_topic'

export interface INotificationSubscriptionTopic {
    topic: string
    topic_type: NotificationTopicTypes
}

export interface INotificationSubscription extends IMongoEntity {
    user_id: ObjectId
    topics: INotificationSubscriptionTopic[]
    enabled: boolean
    created: Date
    updated: Date
}