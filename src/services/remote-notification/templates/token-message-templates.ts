import admin from 'firebase-admin'
import { AllNotificationTypes, NotificationTemplateColors } from '../models'

export const getNotificationTokenMessage = (
    template: AllNotificationTypes,
    from: string,
): admin.messaging.Notification => {
    let t = getNotificationTemplate(template, from)

    return {
        title: t.title,
        body: t.body,
        // imageUrl: '',
        // color: t.color,
        // clickAction: t.clickAction,
    }
}

const getNotificationTemplate = (
    template: AllNotificationTypes,
    from: string,
    clickAction?: string
) => {
    switch (template) {
        case 'follow':
            return getFollowNotificationTemplate(from)
        case 'like_post':
            // return getLikePostNotificationTemplate(from)
        // case 'comment':
        //     // return getCommentNotificationTemplate(from)
        // case 'message':
        //     // return getCommentNotificationTemplate(from)
        // case 'post':
        //     // return getPostNotificationTemplate(from)
        // case 'event':
        //     // return getEventNotificationTemplate(from)
        default:
            return {
                title: `Roamr`,
                body: ``,
                color: NotificationTemplateColors.FOLLOW,
                imageUrl: ``,
                clickAction: ``,
            }
    }
}

const getFollowNotificationTemplate = (
    from: string,
) => {
    return {
        // title: `${from}`,
        title: `Roamr`,
        body: `${from} followed you`,
        color: NotificationTemplateColors.FOLLOW,
        clickAction: '',
    }
}