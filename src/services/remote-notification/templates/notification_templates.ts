import admin from 'firebase-admin'
import { NotificationTemplateTypes, NotificationTemplateColors } from '../models'

export const getNotificationMessage = (
    condition: string,
    token: string,
    topic: string,
    data: {
        [key: string]: any
    },
    notification: admin.messaging.NotificationMessagePayload,
): admin.messaging.Message => {
    return {
        condition,
        token,
        topic,
        data,
        notification,
    }
}

export const getNotificationMessagePayload = (
    template: NotificationTemplateTypes,
    from: string,
): admin.messaging.NotificationMessagePayload => {
    let t = getNotificationTemplate(template, from)

    return {
        title: t.title,
        body: t.body,
        color: t.color,
        clickAction: t.clickAction,
    }
}

const getNotificationTemplate = (
    template: NotificationTemplateTypes,
    from: string,
    clickAction?: string
) => {
    switch (template) {
        case 'follow':
            return getFollowNotificationTemplate(from)
        case 'like_post':
            return getLikePostNotificationTemplate(from)
        case 'comment':
            return getCommentNotificationTemplate(from)
        case 'message':
            return getCommentNotificationTemplate(from)
        case 'post':
            return getPostNotificationTemplate(from)
        case 'event':
            return getEventNotificationTemplate(from)
        default:
            return {
                title: `Roamr`,
                body: ``,
                color: NotificationTemplateColors.FOLLOW,
                clickAction: '',
            }
    }
}

const getFollowNotificationTemplate = (
    from: string,
) => {
    return {
        title: `${from}`,
        body: `${from} followed you`,
        color: NotificationTemplateColors.FOLLOW,
        clickAction: '',
    }
}

const getLikePostNotificationTemplate = (
    from: string,
): admin.messaging.NotificationMessagePayload => {
    return {
        title: `${from}`,
        body: `${from} liked your post`,
        color: NotificationTemplateColors.LIKE,
        clickAction: '',
    }
}

const getLikeEventNotificationTemplate = (
    from: string,
) => {
    return {
        title: `${from}`,
        body: `${from} liked your event`,
        color: NotificationTemplateColors.LIKE,
        clickAction: '',
    }
}

const getLikePinNotificationTemplate = (
    from: string,
) => {
    return {
        title: `${from}`,
        body: `${from} liked a pin`,
        color: NotificationTemplateColors.LIKE,
        clickAction: '',
    }
}

const getLikeMessageNotificationTemplate = (
    from: string,
) => {
    return {
        title: `${from}`,
        body: `${from} liked your message`,
        color: NotificationTemplateColors.LIKE,
        clickAction: '',
    }
}

const getCommentNotificationTemplate = (
    from: string,
) => {
    return {
        title: `${from}`,
        body: `${from} commented on your post`,
        color: NotificationTemplateColors.LIKE,
        clickAction: '',
    }
}

const getMessageNotificationTemplate = (
    from: string,
) => {
    return {
        title: `${from}`,
        body: `${from} sent a message`,
        color: NotificationTemplateColors.MESSAGE,
        clickAction: '',
    }
}

const getPostNotificationTemplate = (
    from: string,
) => {
    return {
        title: `${from}`,
        body: `${from} has a new post`,
        color: NotificationTemplateColors.POST,
        clickAction: '',
    }
}

const getEventNotificationTemplate = (
    from: string,
) => {
    return {
        title: `${from}`,
        body: `${from} has a new event`,
        color: NotificationTemplateColors.EVENT,
        clickAction: '',
    }
}