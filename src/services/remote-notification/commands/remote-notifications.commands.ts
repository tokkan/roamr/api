import { CommandHandler, ICommandHandler, EventPublisher, EventBus } from '@nestjs/cqrs'
import { Logger } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { RemoteNotificationsEventHandle } from '../events/remote-notifications.aggregate'
import { FollowerTypes } from 'src/microservices/social/models/following/following'
import { RemoteNotificationService } from '../services/remote-notification.service'

export class NotifyFollowTargetCommand {
    constructor(
        public readonly source_id: ObjectId,
        public readonly target_id: ObjectId,
        public readonly target_type: FollowerTypes,
    ) { }
}

@CommandHandler(NotifyFollowTargetCommand)
export class NotifyFollowTargetCommandHandler implements ICommandHandler<NotifyFollowTargetCommand> {
    private readonly logger = new Logger(NotifyFollowTargetCommandHandler.name)
    private readonly eventHandle: RemoteNotificationsEventHandle

    constructor(
        private publisher: EventPublisher,
        private eventBus: EventBus,
        private remoteNotificationService: RemoteNotificationService
    ) {
        this.eventHandle =
            publisher.mergeObjectContext(
                new RemoteNotificationsEventHandle(new ObjectId().toHexString()))
        this.eventHandle.autoCommit = true
    }

    async execute(command: NotifyFollowTargetCommand): Promise<any> {
        this.logger.debug('Executing followshipEventHandler')
        try {
            const {
                source_id,
                target_id,
                target_type,
            } = command
            this.remoteNotificationService
                .addJobToQueue(
                    'follow',
                    source_id,
                    target_id,
                    target_type,
                )
        } catch (error) {
            this.logger.error(error)
        }
    }
}