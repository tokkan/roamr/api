import { InjectQueue } from '@nestjs/bull'
import { Injectable, Logger, OnModuleInit } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'
import { Queue } from 'bull'
import { IRemoteNotification, RedisQueues } from './..'
import { RemoteNotificationService } from './remote-notification.service'

@Injectable()
export class RemoteNotificationJobService implements OnModuleInit {
    private readonly logger = new Logger(RemoteNotificationJobService.name)

    constructor(
        @InjectQueue(RedisQueues.NOTIFICATIONS)
        private readonly notificationQueue: Queue<IRemoteNotification>,
        private readonly remoteNotificationService: RemoteNotificationService,
    ) { }

    async onModuleInit() {
        await this.pauseQueue()
    }

    async pauseQueue() {
        await this.notificationQueue.pause(false)
    }

    async resumeQueue() {
        await this.notificationQueue.resume(false)
    }

    @Cron(CronExpression.EVERY_30_SECONDS)
    async batchHandleQue() {
        const jobs = await this.notificationQueue.getJobs(
            ['active', 'paused', 'delayed', 'failed', 'waiting'],
            0, 10000, true)
        if (jobs.length > 0) {
            this.logger.debug(`Batch running notification jobs`)

            let res =
                await this.remoteNotificationService
                    .sendAll(
                        // [{
                        //     token: '',
                        //     data: {},
                        //     notification: {},
                        // }]
                        jobs.map(m => m.data.notification)
                    )
        } {
            this.logger.debug(`No jobs to execute`)
        }
    }
}
