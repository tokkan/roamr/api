import { InjectQueue } from '@nestjs/bull'
import { Injectable, Logger } from '@nestjs/common'
import { QueryBus } from '@nestjs/cqrs'
import { Queue } from 'bull'
import admin from 'firebase-admin'
import { ObjectId } from 'mongodb'
import { FollowerTypes } from 'src/microservices/social/models/following/following'
import { IUserMongoEntity } from 'src/microservices/user/models'
import { GetUsersByIdsQuery } from 'src/microservices/user/queries/user.queries'
import { IRemoteNotification, IRemoteNotificationJob, RedisQueues } from '..'
import { AllNotificationTypes } from '../models'
import { getNotificationTokenMessage } from '../templates/token-message-templates'

@Injectable()
export class RemoteNotificationService {
    private readonly logger = new Logger(RemoteNotificationService.name)

    constructor(
        private readonly queryBus: QueryBus,
        @InjectQueue(RedisQueues.NOTIFICATIONS)
        private readonly notificationQueue: Queue<IRemoteNotification>,
    ) { }

    async send(message: admin.messaging.Message) {
        let res = await admin.messaging()
            .send(message)
        return res
    }

    async sendAll(messages: admin.messaging.Message[]) {
        let res = await admin.messaging()
            .sendAll(messages)
        return res
    }

    async sendMulticast(
        tokens: string[],
        data: {
            [key: string]: any
        },
        notification: admin.messaging.Notification
    ) {
        this.logger.debug(this.sendMulticast.name)

        let res =
            await admin.messaging()
                .sendMulticast({
                    tokens: tokens,
                    // android: {
                    // },
                    data,
                    notification,
                })
        return res
    }

    async sendToTopic(topic: string, payload: {
        data: {
            [key: string]: string
        },
        notification: admin.messaging.NotificationMessagePayload,
    }) {
        let res =
            await admin.messaging()
                .sendToTopic(topic, payload)
        return res
    }

    async subscribeToTopic(tokens: string[], topic: string) {
        let res =
            await admin.messaging()
                .subscribeToTopic(tokens, topic)
        return res
    }

    async unsubscribeToTopic(tokens: string[], topic: string) {
        let res =
            await admin.messaging()
                .unsubscribeFromTopic(tokens, topic)
        return res
    }

    async sendToTopicWithCondition(
        condition: string,
        payload: {
            data: {
                [key: string]: any
            },
            notification: admin.messaging.NotificationMessagePayload,
        },
        options: admin.messaging.MessagingOptions
    ) {
        let res =
            await admin.messaging()
                .sendToCondition(condition, payload, options)
        return res
    }

    async addJobToQueue(
        notification_type: AllNotificationTypes,
        receiver_user_id: ObjectId,
        target_id: ObjectId,
        target_type: FollowerTypes,
    ) {
        // TODO: Get notification token from user to receive notification
        // TODO: Add queryhandlers and commandhandlers to their module
        // TODO: Get multiple users in one query instead of making multiple

        const users = await this.queryBus
            .execute<GetUsersByIdsQuery, IUserMongoEntity[]>(
                new GetUsersByIdsQuery(
                    [receiver_user_id, target_id]
                )
            )

        const notifee = users.find(f => f._id === receiver_user_id)
        const target = users.find(f => f._id === target_id)
        // const notification_tokens =
        //     to_user.notification_credentials.map(m => (
        //         m.notification_token))

        // TODO: Get notification title, body etc here
        let template = getNotificationTokenMessage(
            notification_type,
            target.username,
        )

        let notifications: IRemoteNotificationJob[] =
            notifee.notification_credentials
                .map(m => {
                    let notification: IRemoteNotification = {
                        _id: new ObjectId(),
                        type: notification_type,
                        receiver: notifee._id,
                        // target: target._id,
                        data: {
                        },
                        notification: {
                            token: m.notification_token,
                            notification: {
                                title: template.title,
                                body: template.body,
                                // imageUrl: '',
                            }
                        },
                        created: new Date(),
                        read: false,
                        sent: false,
                    }
                    return {
                        // name: '',
                        data: notification,
                        // opts: {
                        // }
                    }
                })

        const jobs = await this.notificationQueue.addBulk(notifications)
    }
}
