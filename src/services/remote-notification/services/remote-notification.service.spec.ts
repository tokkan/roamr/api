import { Test, TestingModule } from '@nestjs/testing';
import { RemoteNotificationService } from './remote-notification.service';

describe('RemoteNotificationService', () => {
  let service: RemoteNotificationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RemoteNotificationService],
    }).compile();

    service = module.get<RemoteNotificationService>(RemoteNotificationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
