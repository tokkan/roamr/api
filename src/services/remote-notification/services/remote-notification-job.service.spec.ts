import { Test, TestingModule } from '@nestjs/testing';
import { RemoteNotificationJobService } from './remote-notification-job.service';

describe('RemoteNotificationJobService', () => {
  let service: RemoteNotificationJobService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RemoteNotificationJobService],
    }).compile();

    service = module.get<RemoteNotificationJobService>(RemoteNotificationJobService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
