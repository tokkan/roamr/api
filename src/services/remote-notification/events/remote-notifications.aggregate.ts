import { Logger } from "@nestjs/common"
import { AggregateRoot } from "@nestjs/cqrs"
import { ObjectId } from "mongodb"
import { FollowerTypes } from "src/microservices/social/models/following/following"

export class NotifyFollowTargetEvent {
    constructor(
        public readonly source_id: ObjectId,
        public readonly target_id: ObjectId,
        public readonly target_type: FollowerTypes,
    ) { }
}

export class RemoteNotificationsEventHandle extends AggregateRoot {
    private readonly logger = new Logger(RemoteNotificationsEventHandle.name)

    constructor(private readonly id: string) {
        super()
    }

    async notifyOfFollow(
        source_id: ObjectId,
        target_id: ObjectId,
        target_type: FollowerTypes,
    ) {
        this.apply(new NotifyFollowTargetEvent(
            source_id,
            target_id,
            target_type
        ))
        return {
            source_id,
            target_id,
            target_type,
        }
    }
}