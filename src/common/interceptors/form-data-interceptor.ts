import {
    Injectable, NestInterceptor, ExecutionContext, CallHandler
} from "@nestjs/common"
import { Observable } from "rxjs"
import { map } from "rxjs/operators"

@Injectable()
export class FormDataInterceptor implements NestInterceptor {
    public intercept(_context: ExecutionContext, next: CallHandler): Observable<any> {

        // let request = _context.switchToHttp().getRequest()
        let request = _context.switchToRpc().getData()
        // let ctx = _context.switchToRpc()

        console.log('FormDataInterceptor')
        // console.log(ctx)
        // console.log(request)

        // let keys = Object.keys(request.body)
        let keys = Object.keys(request)

        console.log(keys)
        // console.log(request[keys[0]])
        // request = JSON.stringify(request)

        keys.forEach(f => {
            // // request.body[f] = JSON.parse(request.body[f])
            // // if (typeof request[f] === 'string') {
                request[f] = JSON.parse(request[f])
            // // }
            // // request[f] = JSON.parse(JSON.stringify(request[f]))
        })

        if (request.payload !== undefined &&
            request.payload.assets !== undefined) {
            if (typeof request.payload.assets === 'string') {
                request.payload.assets =
                    JSON.parse(request.payload.assets)
            }
        }

        // changing response
        return next.handle().pipe(
            map(value => {
                return value
            }),
        )
    }
}