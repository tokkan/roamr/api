import { ObjectId } from "mongodb"

export type GatewayEventTypes =
    'notification' |
    'notification_count' |
    'instant_message' |
    'instant_message_count' |
    'event_new' |
    'event_nearby' |
    'event_chat' |
    'nearby_pin' |
    'nearby_chat'

export type EventActions =
    INotificationCountAction

interface IEventAction<T extends GatewayEventTypes, P> {
    event: T,
    data: P
}

export interface INotificationCountAction extends IEventAction<'notification_count', {
    type: 'user',
    user_id: ObjectId,
    count: number,
}> { }