import { Inject, Injectable, Logger } from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { EventActions } from '.'

@Injectable()
export class MicroserviceService {
    private readonly logger = new Logger(MicroserviceService.name)

    constructor(
        @Inject('GATEWAY_SERVICE') private client: ClientProxy,
    ) { }

    test() {
        // this.emit({
        //     event: 'notification_count',
        //     data: {
        //         user_id: 
        //     }
        // })
    }

    async connect() {
        return await this.client.connect()
    }

    // async send(
    // ) {
    //     this.client.send('pattern', 'data')
    // }

    emit(
        payload: EventActions,
    ) {
        const {
            event,
            data,
        } = payload
        this.client.emit(event, data)
    }
}
