import { EventEmitter } from "events"

// https://github.com/stemmlerjs/white-label
// https://khalilstemmler.com/articles/typescript-domain-driven-design/chain-business-logic-domain-events/

function Saga() {
    return function(target, propertyKey, descriptor: PropertyDescriptor) {

        let originalMethod = descriptor.value
        console.log('Saga')
        console.log(descriptor)

        descriptor.value = function (...args: any[]) {
            console.log('Saga II')
            console.log(args)
            return originalMethod.apply(this, args)
        }
    }
}

export class TypedEventEmitter<T> {
    private readonly eventEmitter = new EventEmitter

    addListener<K extends keyof T>(event: K, listener: (payload: T[K], ...args: any[]) => void): void {
        this.eventEmitter.addListener(`${event}`, listener)
    }

    // @Saga()
    on<K extends keyof T>(event: K, listener: (payload: T[K], ...args: any[]) => void) {
        this.eventEmitter.on(`${event}`, listener)
    }

    onId<K extends keyof T>(event: K, id: string, listener: (payload: T[K], ...args: any[]) => void) {
        const eventName = `${event}_${id}`
        this.eventEmitter.on(eventName, listener)
    }

    once<K extends keyof T>(event: K, listener: (payload: T[K], ...args: any[]) => void) {
        this.eventEmitter.once(`${event}`, listener)
    }

    removeListener<K extends keyof T>(event: K, listener: (payload: T[K], ...args: any[]) => void) {
        this.eventEmitter.removeListener(`${event}`, listener)
    }

    off<K extends keyof T>(event: K, listener: (payload: T[K], ...args: any[]) => void) {
        this.eventEmitter.off(`${event}`, listener)
    }

    removeAllListeners<K extends keyof T>(event?: K) {
        event !== undefined ?
            this.eventEmitter.removeAllListeners(`${event}`) :
            this.eventEmitter.removeAllListeners()
    }

    setMaxListeners(n: number) {
        this.eventEmitter.setMaxListeners(n)
    }

    getMaxListeners(): number {
        return this.eventEmitter.getMaxListeners()
    }

    listeners<K extends keyof T>(event: K): Function[] {
        return this.eventEmitter.listeners(`${event}`)
    }

    rawListeners<K extends keyof T>(event: K): Function[] {
        return this.eventEmitter.rawListeners(`${event}`)
    }

    emit<K extends keyof T>(event: K, listener: T[K]): boolean {
        return this.eventEmitter.emit(`${event}`, listener)
    }

    emitToId<K extends keyof T>(event: K, id: string, listener: T[K]): boolean {
        const eventName = `${event}_${id}`
        return this.eventEmitter.emit(eventName, listener)
    }

    listenerCount<K extends keyof T>(type: K): number {
        return this.eventEmitter.listenerCount(`${type}`)
    }

    prependListener<K extends keyof T>(event: K, listener: (arg0: T[K], ...args: any[]) => void) {
        this.eventEmitter.prependListener(`${event}`, listener)
    }

    prependOnceListener<K extends keyof T>(event: K, listener: (arg0: T[K], ...args: any[]) => void) {
        this.eventEmitter.prependOnceListener(`${event}`, listener)
    }

    eventNames(): (string | symbol)[] {
        return this.eventEmitter.eventNames()
    }
}