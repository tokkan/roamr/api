// import { Entity } from "./Entity"
// import { IDomainEvent } from "./events/IDomainEvent"
// import { DomainEvents } from "./events/DomainEvents"
// import { UniqueEntityID } from "./UniqueEntityID"

// // Aggregate root is an `abstract` class because, well- there's 
// // no such thing as a aggregate in and of itself. It needs to _be_
// // something, like User, Vinyl, etc.

// export abstract class AggregateRoot<T> extends Entity<T> {

//     // A list of domain events that occurred on this aggregate
//     // so far.
//     private _domainEvents: IDomainEvent[] = [];

//     get id(): UniqueEntityID {
//         return this._id;
//     }

//     get domainEvents(): IDomainEvent[] {
//         return this._domainEvents;
//     }

//     protected addDomainEvent(domainEvent: IDomainEvent): void {
//         // Add the domain event to this aggregate's list of domain events
//         this._domainEvents.push(domainEvent);

//         // Add this aggregate instance to the DomainEventHandler's list of
//         // 'dirtied' aggregates 
//         DomainEvents.markAggregateForDispatch(this);
//     }

//     public clearEvents(): void {
//         this._domainEvents.splice(0, this._domainEvents.length);
//     }

//     private logDomainEventAdded(domainEvent: IDomainEvent): void {
//     ...
//     }
// }