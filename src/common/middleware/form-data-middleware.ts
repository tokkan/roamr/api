import { Injectable, NestMiddleware, Logger } from "@nestjs/common"
import { Request, Response } from 'express'

@Injectable()
export class FormDataMiddleware implements NestMiddleware {
    private readonly logger = new Logger(FormDataMiddleware.name)

    async use(request: Request, res: Response, next: Function) {
        let keys = Object.keys(request.body)

        this.logger.debug('use')
        console.log('Form data middleware')
        console.log('before')
        console.log(request.body)
        // console.log(request.body.payload)

        keys.forEach(f => {
            request.body[f] = JSON.parse(request.body[f])
        })

        console.log('after')
        // console.log(request.body.payload)
        next()
    }
}