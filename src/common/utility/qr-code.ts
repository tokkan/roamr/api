import { Logger } from '@nestjs/common'
import QRCode from 'qrcode'
import Jimp from 'jimp'

export type QRCodeTypes =
    'pin' |
    'trail' |
    'area' |
    'zone' |
    'municipality' |
    'user' |
    'business'

/**
 * Error correction capability allows to 
 * successfully scan a QR Code even if the 
 * symbol is dirty or damaged. 
 * Four levels are available to choose according 
 * to the operating environment.
 * Higher levels offer a better error resistance 
 * but reduce the symbol's capacity.
 * If the chances that the QR Code symbol 
 * may be corrupted are low 
 * (for example if it is showed through a monitor) 
 * is possible to safely use a low error level such as Low or Medium.
 *
 * Level	    Error resistance
 * L (Low)	    ~7%
 * M (Medium)	~15%
 * Q (Quartile)	~25%
 * H (High)	    ~30%
 */
export type QRCodeErrorCorrectionTypes =
    'L' |
    'M' |
    'Q' |
    'H'

const logger = new Logger(`QrCode`)

export function buildQRCode(
    id: string,
    type: QRCodeTypes,
) {
    let qr_code = `roamr/${type}/${id}`
    return qr_code
}

export async function generateQRCodeToDataURL(
    plain_qr_code: string,
    errorCorrectionLevel: QRCodeErrorCorrectionTypes = 'M',
) {
    try {
        let data_url =
            await QRCode.toDataURL(plain_qr_code, {
                errorCorrectionLevel: errorCorrectionLevel,
            })
        return data_url
    } catch (error) {
        this.logger.error(error)
        return null
    }
}

export async function generateQRCodeToBuffer(
    plain_qr_code: string,
    errorCorrectionLevel: QRCodeErrorCorrectionTypes = 'M',
) {
    try {
        let buffer =
            await QRCode.toBuffer(plain_qr_code, {
                errorCorrectionLevel: errorCorrectionLevel,
            })
        return buffer
    } catch (error) {
        this.logger.error(error)
        return null
    }
}

export async function generateCustomQRCode(
    plain_qr_code: string,
    errorCorrectionLevel: QRCodeErrorCorrectionTypes = 'M',
    width: number,
    height: number,
) {
    // TODO: Create custom QR codes with the help of e.g. JIMP
    // TODO: Perhaps try make a qrcode, but instead use buffer

    let qr_code_buffer = await this.generateQRCodeToBuffer(
        plain_qr_code,
        errorCorrectionLevel
    )
    let image = new Jimp({
        data: qr_code_buffer,
        width,
        height,
    })

    // TODO: Customize jimp image
    // TODO: use rxjs?

    let base64 = await image.getBase64Async('image/png')
    // let buffer = await image.getBufferAsync('image/png')

    return base64
}