/**
 * https://dev.to/ascorbic/creating-a-typed-compose-function-in-typescript-3-351i
 * @param fn1 
 * @param fns 
 */
export const pipe = <T extends any[], R>(
    fn1: (...args: T) => R,
    ...fns: Array<(a: R) => R>
) => {
    const piped = fns.reduce(
        (prevFn, nextFn) => (value: R) => nextFn(prevFn(value)),
        value => value
    )
    return (...args: T) => piped(fn1(...args))
}

export const valuePairsToObject = (obj: {
    [key: string]: any,
    prop: string,
    value: any,
}[]) => {
    return obj.reduce((acc, curr) => {
        return {
            ...acc,
            [curr.prop]: curr.value
        }
    }, {})
}

interface IValuePair {
    prop: string
    value: any
}

export const objectToValuePairs = (
    obj: any,
): IValuePair[] => {
    let entries = Object.entries(obj)
    let pair: IValuePair[] = entries.reduce((prev: any, curr: any) => {
        return [
            ...prev,
            {
                prop: curr[0],
                value: curr[1],
            }
        ]
    }, [])
    return pair
}

interface IPOJO {
    [key: string]: any
}

export const mergeObjects = (...obj: object[]) => {
    return obj.reduce((acc, curr) => {
        return {
            ...acc,
            ...curr
        }
    }, {})
}

export const removeUndefinedFromObject = <T>(obj: T): Partial<T> => {
    return Object.keys(obj).reduce((acc, key) => {
        const _acc = acc;
        if (obj[key] !== undefined) {
            _acc[key] = obj[key]
        }
        return _acc;
    }, {})
}

export const renameObject = (
    arr: object,
    newName: string,
) => {
    return Object.entries(arr).reduce((prev, curr) => {
        return {
            ...prev,
            [`${newName}${curr[0]}`]: curr[1]
        }
    }, {})
}