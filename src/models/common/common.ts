export type CountryTypes =
    'sweden' |
    'norway' |
    'denmark' |
    'finland' |
    'iceland' |
    'netherlands' |
    'germany' |
    'usa'

export type LanguageTypes =
    'se' |
    'en' |
    'dk' |
    'nl'

export type DevicePlatforms =
    'android' |
    'ios' |
    'windows' |
    'linux' |
    'macos' |
    'web'

export enum DevicePlatformsEnum {
    'android' = 'android',
    'ios' = 'ios',
    'windows' = 'windows',
    'linux' = 'linux',
    'macos' = 'macos',
    'web' = 'web',
}

export type ImageSourceTypes =
    'url' |
    'base64'