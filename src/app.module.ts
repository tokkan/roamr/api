import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { ScheduleModule } from '@nestjs/schedule'
import { SocialController } from './controllers/social/social.controller'
import { SearchController } from './microservices/search/search.controller'
import { SearchModule } from './microservices/search/search.module'
import { MessagesModule } from './microservices/social/messages/messages.module'
import { PostModule } from './microservices/social/post/post.module'
import { SocialModule } from './microservices/social/social.module'
import { StorageModule } from './microservices/storage/storage.module'
import { UsersModule } from './microservices/user/users.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    ScheduleModule.forRoot(),
    ClientsModule.register([
      {
        name: 'GATEWAY_SERVICE',
        transport: Transport.TCP,
        options: {
          host: process.env.GATEWAY_MICROSERVICE_HOST,
          port: +process.env.GATEWAY_MICROSERVICE_PORT,
        }
      }
    ]),
    MessagesModule,
    PostModule,
    SearchModule,
    SocialModule,
    StorageModule,
    UsersModule,
  ],
  controllers: [
    // SearchController,
    SocialController,
  ],
  providers: [
  ],
})
export class AppModule { }
