import { Logger } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { MicroserviceOptions, Transport } from '@nestjs/microservices'
// import { DocumentBuilder } from '@nestjs/swagger/dist/document-builder'
// import { SwaggerModule } from '@nestjs/swagger/dist/swagger-module'
import { AppModule } from './app.module'
import { Firebase } from './database/firebase/database'
import { MongoDatabaseHandle } from './database/mongodb'

const logger = new Logger('RoamrAPI')

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.TCP,
      options: {
        host: process.env.API_MICROSERVICE_HOST,
        port: +process.env.API_MICROSERVICE_PORT,
      }
    })

  MongoDatabaseHandle.setConnectionString(
    'roamr-social',
    process.env.DB_CONNECTION_STRING_SOCIAL
  )
  MongoDatabaseHandle.setConnectionString(
    'roamr-world',
    process.env.DB_CONNECTION_STRING_WORLD
  )

  await MongoDatabaseHandle.connect('roamr-social')
  await MongoDatabaseHandle.connect('roamr-world')
  Firebase.initialize()

  // const docOptions = new DocumentBuilder()
  //   .setTitle('Gateway Docs')
  //   .setDescription('The Gateway description')
  //   .setVersion('1.0')
  //   .addTag('gateway')
  //   .build()
  // const document = SwaggerModule.(app, docOptions)
  // SwaggerModule.setup('gateway', app, document)

  // app.enableShutdownHooks()
  // await app.listenAsync()
  await app.listen(() => {
    setTimeout(async () => {
      MongoDatabaseHandle.checkIndex()
      logger.log(`Microservice is listening on TCP socket`)
    }, 1000)
  })
}
bootstrap()
