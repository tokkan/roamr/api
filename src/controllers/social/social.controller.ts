import { Controller, Inject, Logger, OnModuleInit } from '@nestjs/common'
import { ClientProxy, MessagePattern } from '@nestjs/microservices'
import { Cron, CronExpression } from '@nestjs/schedule'

@Controller('social')
export class SocialController {
    // export class SocialController implements OnModuleInit {
    private readonly logger = new Logger(SocialController.name)

    constructor(
        @Inject('GATEWAY_SERVICE') private client: ClientProxy,
    ) { }

    // @Cron(CronExpression.EVERY_30_SECONDS)
    onTest() {
        this.logger.debug('on test')
        this.client.emit(
            'notification2',
            1
        )
    }

    // // TODO: Try use ClientProxy to send message to gateway here or use cron job
    // async onModuleInit() {
    //     // throw new Error('Method not implemented.')
    //     console.log(this.onModuleInit.name)
    //     let res = await this.client.connect()
    //     console.log(this.client)
    // }

    @MessagePattern({
        cmd: 'sum'
    })
    test(data: number[]) {
        this.logger.debug('Test controller executed')
        console.log(data)
        return this.client.send({
            cmd: 'notification',
        }, {
            data: 'lemon'
        })
        // return (data || []).reduce((a, b) => a + b)
    }
}
