import { ObjectId } from "mongodb"

export interface IFirestoreEntity {
    fire_id: string
}

export interface IMongoEntity {
    _id: ObjectId
}

export interface IMongoStringEntity {
    _id: string
}
