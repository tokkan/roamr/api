import { MongoClient, Db, MongoClientOptions, GridFSBucket, GridFSBucketOptions } from "mongodb"
import { Logger } from "@nestjs/common"
import { DatabaseModels, ITextIndexMap } from "."
import {
    ISocialDatabaseAction, ISocialDatabaseModels, IWorldDatabaseAction, IWorldDatabaseModels
} from "./models"

export type Buckets =
    'social_fs' |
    'world_fs'

export interface IDatabase {
    // connectionString: string
}

export class MongoDatabaseHandle implements IDatabase {
    private static readonly logger = new Logger('Mongo Database')
    // private static _connectionString: string = ''
    private static _connectionStringSocial: string = ''
    private static _connectionStringWorld: string = ''
    // private static _client: MongoClient
    private static _clientSocial: MongoClient
    private static _clientWorld: MongoClient
    // private static _store: Db
    private static _storeSocial: Db
    private static _storeWorld: Db
    private static _bucketSocial: GridFSBucket
    private static _bucketWorld: GridFSBucket
    private static _options: MongoClientOptions = {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        poolSize: 5
    }

    // static async store() {
    //     if (this._store !== undefined) {
    //         return this._store
    //     } else {
    //         await this.connect()
    //         return this._store
    //     }
    // }

    /**
     * Connects to a mongodb instance
     * @param dbName Name of the database to connect to
     */
    static async connect(dbName: 'roamr-social' | 'roamr-world') {
        try {
            switch (dbName) {
                case 'roamr-social':
                    if (this._clientSocial !== undefined &&
                        this._storeSocial !== undefined) {
                    } else {
                        this.logger.log('Connecting to MongoDB')
                        this._clientSocial =
                            await MongoClient.connect(
                                this._connectionStringSocial,
                                this._options)
                        this.setDatabase(dbName)
                        this.logger.log(`Connected to Mongo database: ${dbName}`)

                        this.logger.log('Connecting to MongoDB bucket')
                        this._bucketSocial = new GridFSBucket(this._storeSocial)
                        this.logger.log(`Connected to MongoDB bucket: roamr_social/fs`)
                    }
                    break
                case 'roamr-world':
                    if (this._clientWorld !== undefined &&
                        this._storeWorld !== undefined) {
                    } else {
                        this.logger.log('Connecting to MongoDB')
                        this._clientWorld =
                            await MongoClient.connect(
                                this._connectionStringWorld,
                                this._options)
                        this.setDatabase(dbName)
                        this.logger.log(`Connected to Mongo database: ${dbName}`)

                        this.logger.log('Connecting to MongoDB bucket')
                        this._bucketWorld = new GridFSBucket(this._storeWorld)
                        this.logger.log(`Connected to MongoDB bucket: roamr_world/fs`)
                    }
                    break
            }

            // if (this._client !== undefined &&
            //     this._store !== undefined) {
            // } else {
            //     this.logger.log('Connecting to MongoDB')
            //     this._client =
            //         await MongoClient.connect(
            //             this._connectionString,
            //             this._options)
            //     this.setDatabase(dbName)
            //     // return this._store
            //     this.logger.log(`Connected to Mongo database: ${dbName}`)
            // }
        } catch (error) {
            if (process.env.NODE_ENV !== 'production') {
                this.logger.error(error)
            } else {
                console.log('db error')
                console.debug(error)
                this.logger.error(error)
            }
        }
    }

    static bucket(bucketName: Buckets) {
        switch (bucketName) {
            case 'social_fs':
                return this._bucketSocial
            case 'world_fs':
                return this._bucketWorld
        }
    }

    static async collection(payload: ISocialDatabaseAction | IWorldDatabaseAction) {
        switch (payload.db) {
            case 'roamr-social':
                return await this.collectionSocial(payload.collection)
            case 'roamr-world':
                return await this.collectionWorld(payload.collection)
        }
    }

    static async collectionSocial<
        T extends ISocialDatabaseModels,
        K extends keyof ISocialDatabaseModels
    >(collection: K) {
        try {
            await this.connect('roamr-social')
            return this._storeSocial.collection<T[K]>(collection)
        } catch (error) {
            console.log('Failed getting collection')
            console.log(error)
            this.logger.error(error)
            return null
        }
    }

    static async collectionWorld<
        T extends IWorldDatabaseModels,
        K extends keyof IWorldDatabaseModels
    >(collection: K) {
        try {
            await this.connect('roamr-world')
            return this._storeWorld.collection<T[K]>(collection)
        } catch (error) {
            console.log('Failed getting collection')
            console.log(error)
            this.logger.error(error)
            return null
        }
    }

    // static async collection<
    //     T extends IDatabaseModels,
    //     K extends keyof IDatabaseModels
    // >(collection: K) {
    //     try {
    //         await this.connect()
    //         return this._store.collection<T[K]>(collection)
    //     } catch (error) {
    //         console.log('Failed getting collection')
    //         console.log(error)
    //         this.logger.error(error)
    //         return null
    //     }
    // }

    static async socialCollection<
        T extends ISocialDatabaseModels,
        K extends keyof ISocialDatabaseModels,
        >(collection: K) {
        try {
            await this.connect('roamr-social')
            return this._storeSocial.collection<T[K]>(collection)
        } catch (error) {
            console.log('Failed getting collection')
            console.log(error)
            this.logger.error(error)
            return null
        }
    }

    static async worldCollection<
        T extends IWorldDatabaseModels,
        K extends keyof IWorldDatabaseModels,
        >(collection: K) {
        try {
            await this.connect('roamr-world')
            return this._storeWorld.collection<T[K]>(collection)
        } catch (error) {
            console.log('Failed getting collection')
            console.log(error)
            this.logger.error(error)
            return null
        }
    }

    static async setDatabase(dbName: 'roamr-social' | 'roamr-world') {
        switch (dbName) {
            case 'roamr-social':
                if (this._storeSocial === undefined) {
                    this._storeSocial = this._clientSocial.db(dbName)
                }
                break
            case 'roamr-world':
                if (this._storeWorld === undefined) {
                    this._storeWorld = this._clientWorld.db(dbName)
                }
                break
        }

        // if (this._store === undefined) {
        //     this._store = this._client.db(dbName)
        // }
    }

    /**
     * Closes the database connection 
     */
    static close(dbName: 'roamr-social' | 'roamr-world') {
        switch (dbName) {
            case 'roamr-social':
                if (this._clientSocial) {
                    this._clientSocial.close()
                    this.logger.log('MongoDB connection closed')
                }
                break
            case 'roamr-world':
                if (this._clientWorld) {
                    this._clientWorld.close()
                    this.logger.log('MongoDB connection closed')
                }
                break
        }

        // if (this._client) {
        //     this._client.close()
        //     this.logger.log('MongoDB connection closed')
        // }
    }

    /**
     * Sets the connection string for the database
     * @param connectionString database connection string
     */
    static setConnectionString(
        dbName: 'roamr-social' | 'roamr-world',
        connectionString: string
    ) {
        switch (dbName) {
            case 'roamr-social':
                this._connectionStringSocial = connectionString
                break
            case 'roamr-world':
                this._connectionStringWorld = connectionString
                break
            default:
                break
        }
        // this._connectionString = connectionString
        this.logger.log('MongoDB connection has been set')
    }

    /**
     * TEXT_INDEXES: Text indexes get a name of fieldA_text_fieldB_text
     * where fieldA and fieldB are two different fields. 
     * If there are only fieldA, then the name would be fieldA_text
     * https://docs.mongodb.com/manual/indexes/
     * 
     * ASC_DESC_INDEXES: fieldA_1_fieldB_-1
     * Here fieldA has a ascending index and fieldB a descending index
     */
    static checkIndex() {
        this.logger.debug('Checking MongoDB indexes')

        const textIndexes: ITextIndexMap[] = [
            {
                collection: DatabaseModels.USERS,
                field: [
                    'username',
                    'full_name',
                ]
            }
        ]

        textIndexes.forEach(async f => {
            try {
                // let c = await this.collection(f.collection)
                let c = await this.collection({
                    db: 'roamr-social',
                    collection: DatabaseModels.USERS
                })
                let index_name =
                    f.field.reduce((acc, curr, idx) => {
                        if (idx === 0) {
                            return `${curr}_text`
                        } else {
                            return `${acc}_${curr}_text`
                        }
                    }, '')
                let index_exists = await c.indexExists(index_name)

                if (!index_exists) {
                    let field_spec = f.field.reduce((acc, curr) => {
                        return {
                            ...acc,
                            [curr]: 'text',
                        }
                    }, {})
                    let res = await c.createIndex(field_spec)
                    this.logger.debug(`Created index: ${res}`)
                }
            } catch (error) {
                this.logger.error(error)
            }
        })
    }
}