import { initializeApp, credential, app } from 'firebase-admin'
import { Logger } from '@nestjs/common'
// import serviceAccount from './../secrets/roamr-b1ce7-firebase-adminsdk-p0yxy-430d58f351.json'
const serviceAccount =
    require('./../../../secrets/roamr-b1ce7-firebase-adminsdk-p0yxy-430d58f351.json')
// // import { Bucket } from 'firebase-admin/node_modules/@google-cloud/storage'
// import { Bucket } from '@google-cloud/storage'

export enum DatabaseModels {
    USERS = 'users',
    NOTIFICATIONS = 'notifications',
    FOLLOWSHIP = 'follow_relationships',
    JOBS = 'jobs',
    SE_BUSINESS = 'se_business',
    SE_PINS = 'se_pins',
    SE_PINS_LIKES = 'se_pins_likes',
    SE_PINS_COMMENTS = 'se_pins_comments',
    SE_POSTS = 'se_posts',
    SE_TRAILS = 'se_trails',
    SE_AREAS = 'se_areas',
    SE_COUNTIES = 'se_counties',
    SE_COUNTY_CODES = 'se_county_codes',
    SE_MUNICIPALITY_CODES = 'se_municipality_codes',
    SE_MUNICIPALITIES = 'se_municipalities',
    SE_ZONES = 'se_zones',
    SE_EVENTS = 'se_events',
}

export class Firebase {
    private static readonly logger = new Logger(Firebase.name)
    private static app: app.App

    static initialize() {
        this.logger.log('Connected to Firebase Firestore')
        this.connect()
    }

    private static connect() {
        if (this.app === undefined) {
            this.app = initializeApp({
                credential: credential.cert(serviceAccount),
                // databaseURL: "https://roamr-b1ce7.firebaseio.com",
                // storageBucket: 'gs://roamr-b1ce7.appspot.com'
                databaseURL: process.env.FIREBASE_DATABASE_URL,
                storageBucket: process.env.FIREBASE_GS_STORAGE_BUCKET,
            })
        }

        return this.app.firestore()
    }

    static collection(collection: DatabaseModels) {
        let db = this.connect()
        return db.collection(collection)
    }

    // static bucket(): Bucket {
    //     if (this.app !== undefined) {
    //         this.connect()
    //     }

    //     return this.app.storage().bucket()
    // }
}